import psycopg2

from config import config_bi, config_prod

     
def insert_customer(id_customer,id_contract,date_of_birth,nb_intervention,address):
    sql = """INSERT INTO customer(
	id_customer, id_contract, date_of_birth, nb_intervention, address)
	VALUES (%s, %s, %s,%s, %s)"""
    conn = None
    vendor_id = None
    try:
        params = config_bi()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql, (id_customer,id_contract,date_of_birth,nb_intervention,address))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return vendor_id

def insert_all_customers():
    conn = None
    try:
        params = config_prod()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""
        SELECT user_table.*,count(*)
        FROM user_table 
        inner JOIN contract ON user_table.id=contract.customer_id
        LEFT JOIN intervention ON contract.id=intervention.contract_id
        group by user_table.dtype,user_table.id,email,password,vehicle_bonus_malus,address,date_of_birth,firstname,lastname,phone_number;
        """)
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()
            insert_customer(row[1],1,row[6],row[10],row[5])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

