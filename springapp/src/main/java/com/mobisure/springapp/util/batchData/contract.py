import psycopg2

from config import config_bi, config_prod

     
def insert_contract(id_customer,id_country,start_date,end_date,id_insurance,id_option,nb_right_holder,daily_price,status,type):
    sql = """INSERT INTO public.contract(
	id_customer, id_country, start_time,end_time, id_insurance, id_option, nb_right_holder, daily_price, status, type)
	VALUES (%s, %s, %s,%s, %s, %s, %s, %s, %s, %s)"""
    conn = None
    vendor_id = None
    try:
        params = config_bi()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql, (id_customer,id_country,start_date,end_date,id_insurance,id_option,nb_right_holder,daily_price,status,type))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return vendor_id

def status(id):
    status=["VALID","PENDING","ENDED"]
    return status[id]

def get_insurance_id(transport,travel):
    if transport is None:
        return travel
    else:
        return transport

def insert_all_contract():
    conn = None
    try:
        params = config_prod()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("""SELECT count(*),contract.* 
from contract,contract_right_holders
where contract.id=contract_right_holders.contract_id
group by dtype, id, daily_price, end_of_subscription_date, status, subscription_date, type, country, customer_id, transport_id, transport_insurance_id, travel_insurance_id""")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()
            insert_contract(row[9],row[8],row[6],row[4],get_insurance_id(row[11],row[12]),0,row[0],row[3],status(row[5]),row[7])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

