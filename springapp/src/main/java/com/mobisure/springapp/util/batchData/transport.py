import psycopg2

from config import config_bi, config_prod

     
def insert_transport(id_transport, brand, model, year, value, type, isElectric):
    sql = """INSERT INTO public.transport(
	id_transport, brand, model, year, value, type, "isElectric")
	VALUES (%s, %s, %s, %s, %s, %s, %s)"""
    conn = None
    vendor_id = None
    try:
        params = config_bi()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql,(id_transport, brand, model, year, value, type, isElectric))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return vendor_id

def insert_all_transport():
    conn = None
    try:
        params = config_prod()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * from transport")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()
            insert_transport(row[1],row[2],row[3],row[5],row[4],row[0],row[6])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

