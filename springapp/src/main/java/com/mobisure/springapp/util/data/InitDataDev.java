package com.mobisure.springapp.util.data;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.InterventionRequest;
import com.mobisure.springapp.model.transport.*;
import com.mobisure.springapp.model.travel.ContractTravel;
import com.mobisure.springapp.model.travel.Country;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.model.user.MedicalEmployee;
import com.mobisure.springapp.model.user.Partner;
import com.mobisure.springapp.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Slf4j
@Component
public class InitDataDev {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PartnerRepository partnerRepository;

    @Autowired
    private MedicalEmployeeRepository medicalEmployeeRepository;

    @Autowired
    private TravelInsuranceRepository travelInsuranceRepository;

    @Autowired
    private ContractTravelRepository contractTravelRepository;

    @Autowired
    private VehicleInsuranceRepository vehicleInsuranceRepository;

    @Autowired
    private NewMobilityInsuranceRepository newMobilityInsuranceRepository;

    @Autowired
    private ContractTransportRepository contractTransportRepository;

    @Autowired
    private VehicleModelRepository vehicleModelRepository;

    public void init() {
        log.info("Development data initialization started");
        Random random = new Random();
        Customer c1 = null;
        Customer c2 = null;
        Customer c3 = null;

        try {
            c1 = new Customer("joffreyd59@gmail.com", encoder.encode("password"), "Joffrey", "Delva", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");
            c2 = new Customer("lilian.delouvy@gmail.com", encoder.encode("password"), "Lilian", "Delouvy", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");
            c3 = new Customer("conrad.baudelet@gmail.com", encoder.encode("password"), "Conrad", "Baudelet", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.customerRepository.saveAll(Arrays.asList(c1,c2,c3));

        Partner partner = new Partner("partner@partner.com", encoder.encode("password"));
        this.partnerRepository.save(partner);

        MedicalEmployee medicalEmployee = new MedicalEmployee("medical@medical.com", encoder.encode("password"));
        this.medicalEmployeeRepository.save(medicalEmployee);

        TravelInsurance travelInsurancePremium = travelInsuranceRepository.findTravelInsuranceByTravelCoverType(TravelCoverType.PREMIUM);
        TravelInsurance travelInsuranceLong = travelInsuranceRepository.findTravelInsuranceByTravelCoverType(TravelCoverType.LONG);
        TravelInsurance travelInsuranceShort = travelInsuranceRepository.findTravelInsuranceByTravelCoverType(TravelCoverType.SHORT);

        Date date = new Date();
        ContractTravel cT1  = new ContractTravel(c1,
                Collections.singletonList(c1),
                date,
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).plusDays(100).atZone(ZoneId.of("UTC")).toInstant()),
                travelInsuranceLong.getBaseDailyPrice()*0.90,
                travelInsuranceLong,
                Country.SWITZERLAND,
                new ArrayList<>()
        );
        cT1.setStatus(Contract.Status.VALID);
        ContractTravel cT2  = new ContractTravel(c1,
                Arrays.asList(c1,c2),
                date,
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).plusDays(10).atZone(ZoneId.of("UTC")).toInstant()),
                travelInsuranceShort.getBaseDailyPrice()*1.2,
                travelInsuranceShort,
                Country.BELGIUM,
                new ArrayList<>()
        );
        cT2.setStatus(Contract.Status.PENDING);
        ContractTravel cT3  = new ContractTravel(c3,
                Collections.singletonList(c3),
                date,
                travelInsurancePremium.getBaseDailyPrice(),
                travelInsurancePremium,
                new ArrayList<>()
        );
        cT3.setStatus(Contract.Status.VALID);
        this.contractTravelRepository.saveAll(Arrays.asList(cT1, cT2, cT3));

        VehicleInsurance vehicleInsurance = vehicleInsuranceRepository.findVehicleInsuranceByVehicleCoverType(VehicleCoverType.INTEGRAL);

        List<VehicleModel> vehicleModels = (List<VehicleModel>) vehicleModelRepository.findAll();
        VehicleModel vehicleModel = vehicleModels.get(random.nextInt(vehicleModels.size()) - 1);


        Vehicle vehicle = new Vehicle(
                vehicleModel.getStart_year(),
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).minusDays(10).atZone(ZoneId.of("UTC")).toInstant()),
                "AM-695-FS",
                vehicleModel
        );
        ContractTransport cV1 = new ContractTransport(
                c1,
                Collections.singletonList(c1),
                date,
                vehicleInsurance.getBaseDailyPrice(),
                vehicleInsurance,
                new ArrayList<>(),
                vehicle
        );
        this.contractTransportRepository.save(cV1);

        NewMobilityInsurance newMobilityInsurance = newMobilityInsuranceRepository.findNewMobilityInsuranceByNewMobilityCoverType(NewMobilityCoverType.THEFT);
        NewMobility newMobility = new NewMobility("Xiaomi", "Pro 2", 2018, 300, NewMobilityType.SCOOTER, true);
        ContractTransport cNM2 = new ContractTransport(c2,
                Collections.singletonList(c2),
                date,
                newMobilityInsurance.getBaseDailyPrice(),
                newMobilityInsurance,
                new ArrayList<>(),
                newMobility
        );
        this.contractTransportRepository.save(cNM2);

        InterventionRequest interventionRequest = new InterventionRequest(cV1.getId(), date, "Lille", Country.FRANCE, Collections.singletonList(Intervention.Type.MATERIAL), "Oups, je suis rentré dans un arbre");
        Intervention intervention = new Intervention(cV1, interventionRequest.getDate(), interventionRequest.getCity(), interventionRequest.getCountry(), interventionRequest.getTypes(), interventionRequest.getDescription());
        cV1.addIntervention(intervention);
        log.info("Development data initialization over");
    }
}
