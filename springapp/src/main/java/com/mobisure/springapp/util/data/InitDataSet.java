package com.mobisure.springapp.util.data;

import com.github.javafaker.Faker;
import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.Option;
import com.mobisure.springapp.model.transport.*;
import com.mobisure.springapp.model.travel.ContractTravel;
import com.mobisure.springapp.model.travel.Country;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.model.user.MedicalEmployee;
import com.mobisure.springapp.model.user.Partner;
import com.mobisure.springapp.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@Transactional
public class InitDataSet {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TravelInsuranceRepository travelInsuranceRepository;

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractTravelRepository contractTravelRepository;

    @Autowired
    private VehicleInsuranceRepository vehicleInsuranceRepository;

    @Autowired
    private NewMobilityInsuranceRepository newMobilityInsuranceRepository;

    @Autowired
    private ContractTransportRepository contractTransportRepository;

    @Autowired
    private VehicleModelRepository vehicleModelRepository;

    @Autowired
    private PartnerRepository partnerRepository;

    @Autowired
    private MedicalEmployeeRepository medicalEmployeeRepository;

    private final Random r = new Random();

    private final Faker faker = new Faker(new Locale("fr"));

    private final Date instantDate = new Date();

    public void init() {
        log.info("Dataset: Customer dataset initialisation started");
        initCustomers();
        log.info("Dataset: Customer dataset initialisation over");
        log.info("Dataset: Contract dataset initialisation started");
        initContracts();
        log.info("Dataset: Contract dataset initialisation over");
        log.info("Dataset: Intervention dataset initialisation started");
        initInterventions();
        log.info("Dataset: Intervention dataset initialisation over");
    }

    private void initContracts() {
        this.contractTransportRepository.deleteAll();
        this.contractTravelRepository.deleteAll();
        List<Customer> customers = (List<Customer>) this.customerRepository.findAll();
        for (Customer customer : customers) {
            int n = 3;
            while (n > 0) {
                n--;
                int i = r.nextInt(3);
                switch (i) {
                    case 1:
                        ContractTravel contractTravel = newFakeContractTravel(customer);
                        if (contractTravel.getEndOfSubscriptionDate().before(instantDate))
                            contractTravel.setStatus(Contract.Status.ENDED);
                        else if (contractTravel.getSubscriptionDate().after(instantDate)) {
                            if (r.nextInt() == 0) contractTravel.setStatus(Contract.Status.PENDING);
                            else contractTravel.setStatus(Contract.Status.VALID);
                        } else contractTravel.setStatus(Contract.Status.VALID);
                        this.contractTravelRepository.save(contractTravel);
                        break;
                    case 2:
                        ContractTransport contractTransport = newFakeContractTransport(customer);
                        if (contractTransport.getEndOfSubscriptionDate().before(instantDate))
                            contractTransport.setStatus(Contract.Status.ENDED);
                        else if (contractTransport.getSubscriptionDate().after(instantDate)) {
                            if (r.nextInt() == 0) contractTransport.setStatus(Contract.Status.PENDING);
                            else contractTransport.setStatus(Contract.Status.VALID);
                        } else contractTransport.setStatus(Contract.Status.VALID);
                        this.contractTransportRepository.save(contractTransport);
                        break;
                    default:
                        break;
                }
            }
        }
    }


    private List<Option> pickRandom(List<Option> availableOptions) {
        List<Option> options = new ArrayList<>();
        if (availableOptions.size() == 0) return options;
        int n = r.nextInt(availableOptions.size());

        for (int i = 0; i < n; i++)
            options.add(availableOptions.get(i));
        return options;
    }

    private ContractTransport newFakeContractTransport(Customer customer) {
        TransportInsurance insurance;
        List<Option> availableOptions;
        List<Option> options;
        int i = r.nextInt(2);
        int j;
        double price;
        Date dateStart = faker.date().past(800, TimeUnit.DAYS);
        Date dateEnd = null;
        ContractTransport contractTransport;
        Transport transport;

        int random = r.nextInt(3);
        if (random == 0)
            dateEnd = faker.date().between(dateStart, instantDate);
        else if (random == 1)
            dateEnd = faker.date().future(200, TimeUnit.DAYS, instantDate);

        if (i == 0) {
            VehicleHistoric vehicleHistoric = newFakeVehicleHistoric(customer);
            customer.setVehicleHistoric(vehicleHistoric);
            customer.setVehicleBonusMalus(vehicleHistoric.getBonusMalus());
            List<VehicleInsurance> vehicleInsurances = (List<VehicleInsurance>) vehicleInsuranceRepository.findAll();
            j = r.nextInt(vehicleInsurances.size());
            insurance = vehicleInsurances.get(j);
            availableOptions = insurance.getAvailableOptions();
            options = pickRandom(availableOptions);
            price = 2 + (5 - 2) * r.nextDouble();
            transport = newFakeVehicle(vehicleHistoric, customer);
        } else {
            List<NewMobilityInsurance> newMobilityInsurances = (List<NewMobilityInsurance>) newMobilityInsuranceRepository.findAll();
            j = r.nextInt(newMobilityInsurances.size());
            insurance = newMobilityInsurances.get(j);
            availableOptions = insurance.getAvailableOptions();
            options = pickRandom(availableOptions);
            price = 0.4 + (2 - 0.4) * r.nextDouble();
            transport = newFakeNewMobility();
        }

        contractTransport = new ContractTransport(customer,
                Collections.singletonList(customer),
                dateStart,
                price,
                insurance,
                options,
                transport);
        if (dateEnd != null) contractTransport.setEndOfSubscriptionDate(dateEnd);
        return contractTransport;
    }

    private VehicleHistoric newFakeVehicleHistoric(Customer customer) {
        Calendar cInstantDate = Calendar.getInstance();
        cInstantDate.setTime(instantDate);
        Calendar drivingLicenceCalendar = Calendar.getInstance();
        drivingLicenceCalendar.setTime(customer.getDateOfBirth());
        drivingLicenceCalendar.add(Calendar.YEAR, r.nextInt(30 - 18) + 18);
        Date drivingLicenceDate = drivingLicenceCalendar.getTime();
        if (drivingLicenceDate.after(instantDate)) {
            drivingLicenceCalendar.setTime(faker.date().past(30, TimeUnit.DAYS));
            drivingLicenceDate = drivingLicenceCalendar.getTime();
        }

        int nbMonth = (cInstantDate.get(Calendar.YEAR) - drivingLicenceCalendar.get(Calendar.YEAR)) * 12 + cInstantDate.get(Calendar.MONTH) - drivingLicenceCalendar.get(Calendar.MONTH);
        int nbResponsibleSinister = r.nextInt(3);
        double bonusMalus = 0.5 + (1.0 - 0.50) * r.nextDouble();
        for (int i = 0; i < nbResponsibleSinister; i++)
            bonusMalus *= 1.25;
        return new VehicleHistoric(drivingLicenceDate, nbResponsibleSinister, r.nextInt(4), nbMonth, bonusMalus);
    }

    private Vehicle newFakeVehicle(VehicleHistoric vehicleHistoric, Customer customer) {
        List<VehicleModel> vehicleModels = (List<VehicleModel>) this.vehicleModelRepository.findAll();
        VehicleModel vehicleModel = vehicleModels.get(r.nextInt(vehicleModels.size()));

        int prodYear = r.nextInt(vehicleModel.getEnd_year() - vehicleModel.getStart_year()) + vehicleModel.getStart_year();
        Calendar calendar = Calendar.getInstance();
        calendar.set(prodYear, Calendar.JANUARY, 1);
        Date prodDate = calendar.getTime();
        Date registrationDate = faker.date().between(
                prodDate.before(vehicleHistoric.getDrivingLicenceDate()) ? vehicleHistoric.getDrivingLicenceDate() : prodDate,
                instantDate
        );

        String registrationNumber = "" +
                (char) (r.nextInt(26) + 'a') +
                (char) (r.nextInt(26) + 'a') +
                (char) (r.nextInt(10) + '0') +
                (char) (r.nextInt(10) + '0') +
                (char) (r.nextInt(10) + '0') +
                (char) (r.nextInt(26) + 'a') +
                (char) (r.nextInt(26) + 'a');

        return new Vehicle(prodYear, registrationDate, registrationNumber.toUpperCase(Locale.ROOT), vehicleModel);
    }

    private NewMobility newFakeNewMobility() {
        List<String> brands = Arrays.asList("Xiaomi", "Decathlon", "Orbea", "NC");
        int i = r.nextInt(brands.size());
        int j;
        int value;
        int year = r.nextInt(2022 - 2010) + 2010;
        boolean isElectric;
        String brand = brands.get(i);
        String model;
        List<String> models;
        NewMobilityType newMobilityType;

        switch (brand) {
            case "Xiaomi":
                models = Arrays.asList("Pro", "Pro 2", "Essential", "NC");
                j = r.nextInt(models.size());
                model = models.get(j);
                value = r.nextInt(400) + 200;
                isElectric = r.nextBoolean();
                newMobilityType = NewMobilityType.SCOOTER;
                break;
            case "Decathlon":
                models = Arrays.asList("Rockrider", "Triban", "NC");
                j = r.nextInt(models.size());
                model = models.get(j);
                value = r.nextInt(800) + 200;
                isElectric = r.nextBoolean();
                newMobilityType = NewMobilityType.BIKE;
                break;
            case "Orbea":
                models = Arrays.asList("Orca", "Terra", "Oiz", "NC");
                j = r.nextInt(models.size());
                model = models.get(j);
                value = r.nextInt(5000) + 1700;
                isElectric = r.nextBoolean();
                newMobilityType = NewMobilityType.BIKE;
                break;
            default:
                model = "NC";
                value = r.nextInt(1000) + 100;
                isElectric = r.nextBoolean();
                int k = r.nextInt();
                if (k == 0)
                    newMobilityType = NewMobilityType.SCOOTER;
                else
                    newMobilityType = NewMobilityType.BIKE;
                break;
        }
        return new NewMobility(brand, model, year, value, newMobilityType, isElectric);
    }

    private ContractTravel newFakeContractTravel(Customer customer) {
        List<TravelInsurance> travelInsurances = (List<TravelInsurance>) travelInsuranceRepository.findAll();
        TravelInsurance travelInsurance = travelInsurances.get(r.nextInt(travelInsurances.size()));
        List<Option> availableOptions = travelInsurance.getAvailableOptions();
        List<Option> options = pickRandom(availableOptions);

        if (travelInsurance.getTravelCoverType() == TravelCoverType.PREMIUM)
            return new ContractTravel(customer, Collections.singletonList(customer), faker.date().past(1000, TimeUnit.DAYS), 1.0, travelInsurance, options);
        else {
            int length;
            List<Country> countries = Arrays.asList(Country.values());
            Country country = countries.get(r.nextInt(countries.size()));
            int isPast = r.nextInt(4);
            Date dateReturn;
            Date dateDepart;
            Calendar c = Calendar.getInstance();

            if (isPast != 0) {
                dateReturn = faker.date().past(300, TimeUnit.DAYS);
                if (travelInsurance.getTravelCoverType() == TravelCoverType.SHORT)
                    length = r.nextInt(travelInsurance.getMaxDaysLength());
                else
                    length = r.nextInt(600 - travelInsurance.getMinDaysLength()) + travelInsurance.getMinDaysLength();
                c.setTime(dateReturn);
                c.add(Calendar.DATE, -length);
                dateDepart = c.getTime();
            } else {
                dateDepart = faker.date().future(300, TimeUnit.DAYS);
                if (travelInsurance.getTravelCoverType() == TravelCoverType.SHORT)
                    length = r.nextInt(travelInsurance.getMaxDaysLength());
                else
                    length = r.nextInt(600 - travelInsurance.getMinDaysLength()) + travelInsurance.getMinDaysLength();
                c.setTime(dateDepart);
                c.add(Calendar.DATE, length);
                dateReturn = c.getTime();
            }

            return new ContractTravel(customer, Collections.singletonList(customer), dateDepart, dateReturn, 1.0, travelInsurance, country, options);
        }
    }

    public void initCustomers() {
        int i = 0;
        while (i < 200) {
            Customer customer = newFakeCustomer();
            this.customerRepository.save(customer);
            i++;
        }
    }

    public Customer newFakeCustomer() {
        Calendar c = Calendar.getInstance();
        String firstname = faker.name().firstName();
        String lastname = faker.name().lastName();
        Date date = faker.date().birthday(18, 65);
        c.setTime(date);
        String streetName = faker.address().streetName();
        String number = faker.address().buildingNumber();
        String city = faker.address().city();
        String address = number + " " + streetName + ", " + city;
        String phoneNumber = faker.phoneNumber().cellPhone();
        String email = firstname + "." + lastname + c.get(Calendar.YEAR) + "@gmail.com";

        return new Customer(email, encoder.encode("1234"), firstname, lastname, date, address, phoneNumber);
    }

    public void initInterventions() {
        Partner partner = new Partner("partnerDataSet@partner.com", encoder.encode("password"));
        this.partnerRepository.save(partner);

        MedicalEmployee medicalEmployee = new MedicalEmployee("medicalDataSet@medical.com", encoder.encode("password"));
        this.medicalEmployeeRepository.save(medicalEmployee);

        for (Contract contract : this.contractRepository.findAll()) {
            if (contract.getSubscriptionDate().before(instantDate)) {
                if (contract instanceof ContractTransport) {
                    ContractTransport contractTransport = (ContractTransport) contract;
                    if (r.nextFloat() <= 0.70 && contractTransport.getStatus().equals(Contract.Status.VALID))
                        contractTransport.addIntervention(newFakeInterventionTransport(contractTransport));
                    if (r.nextFloat() <= 0.20 && contractTransport.getStatus().equals(Contract.Status.VALID))
                        contractTransport.addIntervention(newFakeInterventionTransport(contractTransport));
                }
                if (contract instanceof ContractTravel) {
                    ContractTravel contractTravel = (ContractTravel) contract;
                    if (r.nextFloat() <= 0.20 && contractTravel.getStatus().equals(Contract.Status.VALID))
                        contractTravel.addIntervention(newFakeInterventionTravel(contractTravel));
                }
            }
        }
    }

    private List<Intervention.Type> randomTypes() {
        List<Intervention.Type> randomTypes = new ArrayList<>();
        if (r.nextFloat() < 0.40) randomTypes.add(Intervention.Type.MATERIAL);
        if (r.nextFloat() < 0.40) randomTypes.add(Intervention.Type.INJURY);
        if (r.nextFloat() < 0.20 | randomTypes.isEmpty()) randomTypes.add(Intervention.Type.UNDEFINED);

        return randomTypes;
    }

    private Intervention newFakeInterventionTravel(ContractTravel contract) {
        Date date = faker.date().between(contract.getSubscriptionDate(), contract.getEndOfSubscriptionDate().before(instantDate) ? contract.getEndOfSubscriptionDate() : instantDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(instantDate);
        cal.add(Calendar.DATE, -60);
        int cost = r.nextInt(1500) + 1500;
        Intervention intervention = new Intervention(contract, date, "", contract.getCountry(), randomTypes(), "");

        return getIntervention(cal, cost, intervention);
    }

    private Intervention newFakeInterventionTransport(ContractTransport contract) {
        Date date = faker.date().between(contract.getSubscriptionDate(), contract.getEndOfSubscriptionDate().before(instantDate) ? contract.getEndOfSubscriptionDate() : instantDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(instantDate);
        cal.add(Calendar.DATE, -60);
        int cost = r.nextInt(13000) + 7000;
        Intervention intervention = new Intervention(contract, date, faker.address().city(), Country.FRANCE, randomTypes(), "");

        return getIntervention(cal, cost, intervention);
    }

    private Intervention getIntervention(Calendar cal, int cost, Intervention intervention) {
        if (intervention.getDate().after(cal.getTime()) && r.nextFloat() < 0.70)
            intervention.setStatus(Intervention.Status.PENDING);
        else if (intervention.getDate().before(cal.getTime())) intervention.setStatus(Intervention.Status.ACCEPTED);
        else intervention.setStatus(Intervention.Status.ENDED);

        intervention.setCost(cost);

        if (intervention.getTypes().contains(Intervention.Type.INJURY))
            intervention.setInterventionEmployee(medicalEmployeeRepository.findUserByEmail("medicalDataSet@medical.com"));
        else intervention.setInterventionEmployee(partnerRepository.findUserByEmail("partnerDataSet@partner.com"));

        return intervention;
    }
}
