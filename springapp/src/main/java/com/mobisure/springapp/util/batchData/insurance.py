import psycopg2

from config import config_bi, config_prod

     
def insert_insurance(id_insurance, base_daily_price, price, cover_type, type):
    sql = """INSERT INTO public.insurance(id_insurance, base_daily_price, price, cover_type, type)
	VALUES (%s, %s,%s, %s, %s)"""
    conn = None
    vendor_id = None
    try:
        params = config_bi()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql, (id_insurance, base_daily_price, price, cover_type, type))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return vendor_id

def insert_all_insurance():
    conn = None
    try:
        params = config_prod()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * from insurance")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()
            insert_insurance(row[1],row[2],row[2],"test",row[3])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

