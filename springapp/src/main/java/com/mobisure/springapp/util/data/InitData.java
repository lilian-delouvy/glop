package com.mobisure.springapp.util.data;

import com.mobisure.springapp.model.transport.*;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import com.mobisure.springapp.model.user.InsuranceEmployee;
import com.mobisure.springapp.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class InitData {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private InsuranceEmployeeRepository insuranceEmployeeRepository;

    /* Travel insurance */
    public static final double travelPremiumPrice = 1.33;
    public static final double travelShortPrice = 2;
    public static final double travelLongPrice = 1.75;

    @Autowired
    private TravelInsuranceRepository travelInsuranceRepository;

    /* Transport insurance */
    public static final double newMobilityTheftPrice = 1;
    public static final double newMobilityIntegralPrice = 1.5;
    public static final double vehicleThirdPrice = 3;
    public static final double vehicleThirdTheftPrice = 4;
    public static final double vehicleIntegralPrice = 4.5;

    @Autowired
    private VehicleInsuranceRepository vehicleInsuranceRepository;

    @Autowired
    private NewMobilityInsuranceRepository newMobilityInsuranceRepository;

    /* Vehicles */
    @Autowired
    private VehicleModelRepository vehicleModelRepository;

    public void init() {
        log.info("Common data initialization started");
        InsuranceEmployee insuranceEmployee = new InsuranceEmployee("admin@assurmob.com", encoder.encode("password"));
        this.insuranceEmployeeRepository.save(insuranceEmployee);

        TravelInsurance travelInsurancePremium = new TravelInsurance(travelPremiumPrice, TravelCoverType.PREMIUM, -1, -1);
        TravelInsurance travelInsuranceLong = new TravelInsurance(travelLongPrice, TravelCoverType.LONG, 30, -1);
        TravelInsurance travelInsuranceShort = new TravelInsurance(travelShortPrice, TravelCoverType.SHORT, 0, 30);
        this.travelInsuranceRepository.saveAll(Arrays.asList(travelInsuranceLong, travelInsuranceShort, travelInsurancePremium));

        VehicleInsurance vehicleInsuranceThird = new VehicleInsurance(vehicleThirdPrice, VehicleCoverType.THIRD_PARTY);
        VehicleInsurance vehicleInsuranceThirdTheft = new VehicleInsurance(vehicleThirdTheftPrice, VehicleCoverType.THIRD_PARTY_THEFT);
        VehicleInsurance vehicleInsuranceIntegral = new VehicleInsurance(vehicleIntegralPrice, VehicleCoverType.INTEGRAL);
        this.vehicleInsuranceRepository.saveAll(Arrays.asList(vehicleInsuranceIntegral, vehicleInsuranceThird, vehicleInsuranceThirdTheft));

        NewMobilityInsurance newMobilityInsuranceTheft = new NewMobilityInsurance(newMobilityTheftPrice, NewMobilityCoverType.THEFT);
        NewMobilityInsurance newMobilityInsuranceIntegral = new NewMobilityInsurance(newMobilityIntegralPrice, NewMobilityCoverType.INTEGRAL);
        this.newMobilityInsuranceRepository.saveAll(Arrays.asList(newMobilityInsuranceIntegral, newMobilityInsuranceTheft));

        initVehicles();

        log.info("Common data initialization over");
    }

    public void initVehicles() {
        List<VehicleModel> vehicleModels = new ArrayList<>();

        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1700, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1750, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1800, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1900, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 2000, 150, VehicleEnergy.GASOLINE));

        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2000, 70, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2200, 90, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2700, 95, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2800, 100, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 3000, 110, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 3200, 120, VehicleEnergy.DIESEL));

        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2200, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2400, 90, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 2900, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 3100, 100, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 3300, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 2", 2002, 2009, 3500, 120, VehicleEnergy.GASOLINE));

        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 4000, 85, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 5000, 95, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 5500, 115, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 6000, 140, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 6500, 240, VehicleEnergy.DIESEL));

        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 4000, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 5000, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 5500, 115, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 6000, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 3", 2008, 2016, 6500, 240, VehicleEnergy.GASOLINE));

        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 1700, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 1750, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 1800, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 1900, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 1", 1995, 2001, 2000, 150, VehicleEnergy.GASOLINE));

        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2000, 70, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2200, 90, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2700, 95, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2800, 100, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 3000, 110, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 3200, 120, VehicleEnergy.DIESEL));

        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2200, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2400, 90, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 2900, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 3100, 100, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 3300, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 2", 2002, 2009, 3500, 120, VehicleEnergy.GASOLINE));

        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 4000, 85, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 5000, 95, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 5500, 115, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 6000, 140, VehicleEnergy.DIESEL));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 6500, 240, VehicleEnergy.DIESEL));

        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 4000, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 5000, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 5500, 115, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 6000, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Volkswagen", "Golf 3", 2008, 2016, 6500, 240, VehicleEnergy.GASOLINE));

        this.vehicleModelRepository.saveAll(vehicleModels);
    }

}
