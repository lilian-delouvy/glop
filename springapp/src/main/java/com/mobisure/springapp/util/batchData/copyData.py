
from config import config_bi
from contract import insert_all_contract
from customer import insert_all_customers
from insurance import insert_all_insurance
from intervention import insert_all_intervention
from transport import insert_all_transport

import psycopg2

from config import config_bi, config_prod

if __name__ == '__main__':
    params = config_bi()
    conn_bi = psycopg2.connect(**params)
    cur = conn_bi.cursor()
    cur.execute("truncate contract,customer,insurance,intervention,transport")
    conn_bi.commit()

    insert_all_customers()
    insert_all_contract()
    insert_all_insurance()
    insert_all_intervention()
    insert_all_transport()