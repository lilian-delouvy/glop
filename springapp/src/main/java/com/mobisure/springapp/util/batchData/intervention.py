import psycopg2

from config import config_bi, config_prod

     
def insert_intervention(id_customer, id_country, time, id_insurance, id_transport, date, cost, status, type, city, country):
    sql = """INSERT INTO public.intervention(
	id_customer, id_country, time, id_insurance, id_transport, date, cost, status, type, city, country)
	VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    conn = None
    vendor_id = None
    try:
        params = config_bi()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(sql,(id_customer, id_country, time, id_insurance, id_transport, date, cost, status, type, city, country))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return vendor_id

def insert_all_intervention():
    conn = None
    try:
        params = config_prod()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * from intervention")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()
            insert_intervention(row[7],row[3],row[4],0,0,row[4],row[2],row[6],'type',row[1],row[3])
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

