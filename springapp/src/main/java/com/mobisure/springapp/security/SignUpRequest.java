package com.mobisure.springapp.security;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class SignUpRequest extends JwtRequest {

    String firstname;

    String lastname;

    Date dateOfBirth;

    String address;

    String phoneNumber;
}