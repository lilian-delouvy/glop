package com.mobisure.springapp;

import com.mobisure.springapp.util.data.InitData;
import com.mobisure.springapp.util.data.InitDataDev;
import com.mobisure.springapp.util.data.InitDataSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;


@Slf4j
@SpringBootApplication
public class SpringappApplication {

	@Value("${spring.environment}")
	private String env;

	@Autowired
	InitData initData;

	@Autowired
	InitDataDev initDataDev;

	@Autowired
	InitDataSet initDataSet;

	public static void main(String[] args) {
		SpringApplication.run(SpringappApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		initData.init();
		if (env.equals("development")) {
			initDataSet.init();
			initDataDev.init();
		}
		if (env.equals("production")) {
			initDataSet.init();
		}
		log.info("Data initializations over");
	}
}
