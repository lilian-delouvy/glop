package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface UserBaseRepository<U extends User> extends CrudRepository<U,Long> {

    U findUserByEmail(@Param("email") String email);
}
