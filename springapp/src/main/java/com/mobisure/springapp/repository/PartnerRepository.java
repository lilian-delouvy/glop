package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.Partner;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface PartnerRepository extends UserBaseRepository<Partner>{

    @Query("SELECT e FROM Partner e where e.email = :email")
    @Override
    Partner findUserByEmail(@Param("email") String email);

}
