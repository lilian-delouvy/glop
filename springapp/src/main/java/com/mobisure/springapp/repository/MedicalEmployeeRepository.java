package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.MedicalEmployee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface MedicalEmployeeRepository extends UserBaseRepository<MedicalEmployee>{

    @Query("SELECT e FROM MedicalEmployee e where e.email = :email")
    @Override
    MedicalEmployee findUserByEmail(@Param("email") String email);

}
