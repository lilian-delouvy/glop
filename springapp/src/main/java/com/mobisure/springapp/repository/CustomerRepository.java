package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface CustomerRepository extends UserBaseRepository<Customer> {

    @Query("SELECT c FROM Customer c where c.email = :email")
    @Override
    Customer findUserByEmail(@Param("email") String email);

}
