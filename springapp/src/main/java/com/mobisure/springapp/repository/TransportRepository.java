package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.Transport;

import javax.transaction.Transactional;

@Transactional
public interface TransportRepository extends TransportBaseRepository<Transport> {
}
