package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.Vehicle;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends TransportBaseRepository<Vehicle> {
}
