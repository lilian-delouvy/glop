package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.travel.ContractTravel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractTravelRepository extends ContractBaseRepository<ContractTravel> {

    @Override
    @Query("SELECT c FROM ContractTravel c where c.subscriber.email = :email")
    List<ContractTravel> findAllBySubscriber_Email(@Param("email") String email);
}
