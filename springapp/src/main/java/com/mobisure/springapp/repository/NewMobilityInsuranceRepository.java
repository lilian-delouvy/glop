package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.NewMobilityCoverType;
import com.mobisure.springapp.model.transport.NewMobilityInsurance;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface NewMobilityInsuranceRepository extends InsuranceBaseRepository<NewMobilityInsurance> {
    @Override
    NewMobilityInsurance findInsuranceById(Long id);

    NewMobilityInsurance findNewMobilityInsuranceByNewMobilityCoverType(@Param("newMobilityCoverType") NewMobilityCoverType newMobilityCoverType);
}
