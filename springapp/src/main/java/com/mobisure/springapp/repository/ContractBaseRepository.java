package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.Contract;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

@NoRepositoryBean
public interface ContractBaseRepository<C extends Contract> extends CrudRepository<C,Long> {

    List<C> findAllBySubscriber_Email(@Param("email") String email);

    List<C> findAllByStatus(@Param("status") Contract.Status status);
}
