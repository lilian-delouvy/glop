package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.TransportInsurance;
import com.mobisure.springapp.model.transport.VehicleCoverType;
import com.mobisure.springapp.model.transport.VehicleInsurance;
import com.mobisure.springapp.model.travel.TravelCoverType;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface VehicleInsuranceRepository extends InsuranceBaseRepository<VehicleInsurance> {
    @Override
    VehicleInsurance findInsuranceById(Long id);

    VehicleInsurance findVehicleInsuranceByVehicleCoverType(@Param("vehicleCoverType") VehicleCoverType vehicleCoverType);
}
