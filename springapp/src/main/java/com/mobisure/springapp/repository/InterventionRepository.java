package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.Intervention;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterventionRepository extends CrudRepository<Intervention,Long> {
    List<Intervention> findAllByStatus(@Param("status") Intervention.Status status);
}
