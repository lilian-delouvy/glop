package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.Insurance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface InsuranceBaseRepository<I extends Insurance> extends CrudRepository<I,Long> {

    I findInsuranceById(@Param("id") Long id);
}