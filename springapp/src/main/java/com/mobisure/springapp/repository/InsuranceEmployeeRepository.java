package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.InsuranceEmployee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface InsuranceEmployeeRepository extends UserBaseRepository<InsuranceEmployee> {

    @Query("SELECT e FROM InsuranceEmployee e where e.email = :email")
    @Override
    InsuranceEmployee findUserByEmail(@Param("email") String email);

}
