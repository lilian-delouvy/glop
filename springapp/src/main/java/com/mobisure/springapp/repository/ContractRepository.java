package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.Contract;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ContractRepository extends ContractBaseRepository<Contract> {

    @Override
    List<Contract> findAllBySubscriber_Email(@Param("email") String email);

    @Override
    List<Contract> findAllByStatus(@Param("status") Contract.Status status);
}
