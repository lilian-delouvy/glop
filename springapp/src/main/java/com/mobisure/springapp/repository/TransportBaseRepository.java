package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.Transport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface TransportBaseRepository<T extends Transport> extends CrudRepository<T,Long> {
}
