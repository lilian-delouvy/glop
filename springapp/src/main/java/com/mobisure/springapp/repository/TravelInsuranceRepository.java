package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface TravelInsuranceRepository extends InsuranceBaseRepository<TravelInsurance> {
    @Override
    TravelInsurance findInsuranceById(Long id);

    TravelInsurance findTravelInsuranceByTravelCoverType(@Param("coverType") TravelCoverType travelCoverType);
}
