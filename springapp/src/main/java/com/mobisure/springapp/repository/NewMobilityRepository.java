package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.NewMobility;
import org.springframework.stereotype.Repository;

@Repository
public interface NewMobilityRepository extends TransportBaseRepository<NewMobility> {
}
