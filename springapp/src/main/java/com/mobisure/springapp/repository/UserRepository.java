package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.user.User;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends UserBaseRepository<User> {
    @Override
    User findUserByEmail(String email);
}
