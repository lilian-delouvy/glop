package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.ContractTransport;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractTransportRepository extends ContractBaseRepository<ContractTransport> {

    @Override
    @Query("SELECT c FROM ContractTransport c where c.subscriber.email = :email")
    List<ContractTransport> findAllBySubscriber_Email(@Param("email") String email);
}
