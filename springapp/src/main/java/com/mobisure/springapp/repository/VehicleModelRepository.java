package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.transport.VehicleModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleModelRepository extends CrudRepository<VehicleModel, Long> {
    VehicleModel findVehicleModelById(Long id);
}
