package com.mobisure.springapp.repository;

import com.mobisure.springapp.model.Insurance;

import javax.transaction.Transactional;

@Transactional
public interface InsuranceRepository extends InsuranceBaseRepository<Insurance> {
    @Override
    Insurance findInsuranceById(Long id);
}
