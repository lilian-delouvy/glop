package com.mobisure.springapp.controller;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.ContractRequest;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.InterventionRequest;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.service.ContractService;
import com.mobisure.springapp.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.InvalidClassException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class ContractController {

    @Autowired
    ContractService contractService;

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/employees/contracts", produces = "application/json")
    public List<Contract> getAllContracts(){
        return contractService.getContracts();
    }

    @GetMapping(value = "/customer/contracts", produces = "application/json")
    public List<Contract> getContracts(){
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Customer customer = customerService.getCustomerByEmail(username);
        return customer.getContractList();
    }

    @GetMapping(value = "/customer/contracts/{status}", produces = "application/json")
    public List<Contract> getCustomerContractsByStatus(@PathVariable String status){
        List<Contract> contractsList = getContracts();
        return contractsList.stream().filter(contract -> contract.getStatus().toString().equals(status)).collect(Collectors.toList());
    }

    @PostMapping("/customer/contract/new")
    public ResponseEntity<?> requestContract(@RequestBody ContractRequest request) throws InvalidClassException {
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Customer customer = customerService.getCustomerByEmail(username);
        Long id = contractService.requestContract(request, customer);
        return ResponseEntity.ok(contractService.getContract(id));
    }

    @GetMapping(value = "/insurance_employee/contracts/{status}", produces = "application/json")
    public List<Contract> getContracts(@PathVariable String status){
        return this.contractService.getContractsByStatus(status);
    }

    @GetMapping(value = "/contract-details/{id}", produces = "application/json")
    public Contract getContract(@PathVariable long id){
        return this.contractService.getContract(id);
    }

    @PostMapping("/insurance_employee/contract/validate")
    public ResponseEntity<?> validateContract(@RequestBody Long id) {
        contractService.validate(id);
        return ResponseEntity.ok(contractService.getContract(id));
    }

    @PostMapping("/insurance_employee/contract/invalidate")
    public ResponseEntity<?> invalidateContract(@RequestBody Long id) {
        contractService.invalidate(id);
        return ResponseEntity.ok(contractService.getContract(id));
    }

    @GetMapping(value = "/customer/contract/interventions", produces = "application/json")
    public List<Intervention> getInterventions(@RequestBody Long contractId) throws IllegalArgumentException{
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Contract contract = contractService.getContract(contractId);
        if(contract.getSubscriber().getEmail().equals(username))
            return contract.getInterventions();
        else throw new IllegalArgumentException("This contract doesn't belong to current customer");
    }

    @PostMapping("/customer/contract/intervention/new")
    public ResponseEntity<?> requestIntervention(@RequestBody InterventionRequest request) {
        Intervention intervention = contractService.requestIntervention(request);
        return ResponseEntity.ok(intervention);
    }
}
