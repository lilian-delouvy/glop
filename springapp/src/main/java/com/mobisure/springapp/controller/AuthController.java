package com.mobisure.springapp.controller;

import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.security.*;
import com.mobisure.springapp.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    CustomerServiceImpl customerService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserDetailsService userDetailsService;

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @PostMapping("/customer/signup")
    public ResponseEntity<?> registerCustomer(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (customerService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Email is already in use!");
        }

        // Create new Customer's account
        Customer customer = new Customer(signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getFirstname(),
                signUpRequest.getLastname(),
                signUpRequest.getDateOfBirth(),
                signUpRequest.getAddress(),
                signUpRequest.getPhoneNumber()
                );

        customerService.save(customer);
        return ResponseEntity.ok("Customer registered successfully!");
    }

    private ResponseEntity<?> authenticateUser(JwtRequest jwtRequest, String employeeType) throws Exception {
        authenticate(jwtRequest.getEmail(), jwtRequest.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getEmail());
        if(userDetails.getAuthorities().contains(new SimpleGrantedAuthority(employeeType))) {

            final String token = jwtUtils.generateToken(userDetails);

            return ResponseEntity.ok(new JwtResponse(token));
        }
        return ResponseEntity.badRequest().body("Error: Wrong account type, required: " + employeeType + ", provided: "+userDetails.getAuthorities().toString());
    }

    @PostMapping("/customer/signin")
    public ResponseEntity<?> authenticateCustomer(@RequestBody @Valid JwtRequest jwtRequest) throws Exception {
        return authenticateUser(jwtRequest, "CUSTOMER");
    }

    @PostMapping("/insurance_employee/signin")
    public ResponseEntity<?> authenticateInsuranceEmployee(@RequestBody @Valid JwtRequest jwtRequest) throws Exception {
        return authenticateUser(jwtRequest, "INSURANCE_EMPLOYEE");
    }

    @PostMapping("/partner/signin")
    public ResponseEntity<?> authenticatePartner(@RequestBody @Valid JwtRequest jwtRequest) throws Exception {
        return authenticateUser(jwtRequest, "PARTNER");
    }

    @PostMapping("/medical_employee/signin")
    public ResponseEntity<?> authenticateMedicalEmployee(@RequestBody @Valid JwtRequest jwtRequest) throws Exception {
        return authenticateUser(jwtRequest, "MEDICAL_EMPLOYEE");
    }
}
