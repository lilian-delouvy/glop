package com.mobisure.springapp.controller;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/customer/infos", produces = "application/json")
    public Customer getInfos(){
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();

        return this.customerService.getCustomerByEmail(username);
    }

    @GetMapping(value = "/insurance_employee/customer/all", produces = "application/json")
    public List<Customer> getCustomers(){
        return this.customerService.getCustomers();
    }

    @GetMapping(value = "/customer/interventions", produces = "application/json")
    public List<Intervention> getInterventions(){
        ArrayList<Intervention> interventions = new ArrayList<>();
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Customer customer = customerService.getCustomerByEmail(username);
        for (Contract contract: customer.getContractList())
            interventions.addAll(contract.getInterventions());
        return interventions;
    }
}