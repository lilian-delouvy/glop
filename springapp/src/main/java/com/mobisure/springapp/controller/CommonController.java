package com.mobisure.springapp.controller;

import com.mobisure.springapp.model.Insurance;
import com.mobisure.springapp.model.transport.VehicleModel;
import com.mobisure.springapp.service.InsuranceService;
import com.mobisure.springapp.service.VehicleModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class CommonController {

    @Autowired
    InsuranceService insuranceService;

    @Autowired
    VehicleModelService vehicleModelService;

    @GetMapping("/")
    public String index() {
        return "Hello World !";
    }

    /* Insurances */
    @GetMapping(value = "common/insurances/all", produces = "application/json")
    public List<Insurance> getInsurances() {
        return insuranceService.getInsurances();
    }

    @GetMapping(value = "common/insurances/{id}", produces = "application/json")
    public Insurance getInsurance(@PathVariable Long id) {
        return insuranceService.getInsuranceByID(id);
    }

    @GetMapping(value = "common/vehicle_model/all", produces = "application/json")
    public List<VehicleModel> getVehicleModels() {
        return vehicleModelService.getVehicleModels();
    }

}
