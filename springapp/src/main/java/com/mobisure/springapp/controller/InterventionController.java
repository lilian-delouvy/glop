package com.mobisure.springapp.controller;

import com.mobisure.springapp.model.CloseInterventionRequest;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.user.MedicalEmployee;
import com.mobisure.springapp.model.user.Partner;
import com.mobisure.springapp.security.UserDetailsImpl;
import com.mobisure.springapp.service.InterventionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/intervention")
public class InterventionController {

    public static class RequestStatus {
        private final Long id;
        private final String status;

        public RequestStatus(Long id, String status){
            this.id = id;
            this.status = status;
        }
    }

    @Autowired
    InterventionService interventionService;

    @GetMapping(value = "/all", produces = "application/json")
    public List<Intervention> getInterventions(){
        return interventionService.getInterventions();
    }

    @GetMapping(value = "/{status}", produces = "application/json")
    public List<Intervention> getInterventionsByStatus(@PathVariable String status){
        return interventionService.getInterventionsByStatus(Intervention.Status.valueOf(status));
    }

    @GetMapping(value = "/related", produces = "application/json")
    public List<Intervention> getUserRelatedInterventions(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl currentUser = (UserDetailsImpl) authentication.getPrincipal();
        return interventionService.getUserRelatedInterventions(currentUser.getId());
    }

    @PostMapping(value = "/update", produces = "application/json")
    public void setInterventionStatus(@Valid @RequestBody RequestStatus requestStatus){
        Intervention.Status interventionStatus = Intervention.Status.valueOf(requestStatus.status);
        if(interventionStatus == Intervention.Status.ACCEPTED){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserDetailsImpl currentUser = (UserDetailsImpl) authentication.getPrincipal();
            if(currentUser.getUser() instanceof MedicalEmployee){
                interventionService.setInterventionAssignedEmployee(requestStatus.id, (MedicalEmployee) currentUser.getUser());
            }
            else{
                interventionService.setInterventionAssignedEmployee(requestStatus.id, (Partner) currentUser.getUser());
            }
        }
        interventionService.setInterventionStatus(requestStatus.id, interventionStatus);
    }

    @PutMapping(value = "/close", produces = "application/json")
    public ResponseEntity<?> closeRequest(@Valid @RequestBody CloseInterventionRequest closeInterventionRequest){
        Intervention intervention = interventionService.getIntervention(closeInterventionRequest.getInterventionId());
        if(intervention == null){
            return ResponseEntity.badRequest().body("Invalid id");
        }
        intervention.setCost(closeInterventionRequest.getCost());
        intervention.setDescription(closeInterventionRequest.getDescription());
        intervention.setStatus(Intervention.Status.ENDED);
        interventionService.save(intervention);
        return ResponseEntity.noContent().build();
    }
}
