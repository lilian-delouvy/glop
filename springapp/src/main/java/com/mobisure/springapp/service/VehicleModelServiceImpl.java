package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.VehicleModel;
import com.mobisure.springapp.repository.VehicleModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleModelServiceImpl implements VehicleModelService {

    @Autowired
    VehicleModelRepository vehicleModelRepository;

    @Override
    public List<VehicleModel> getVehicleModels() {
        return (List<VehicleModel>) vehicleModelRepository.findAll();
    }

    @Override
    public VehicleModel getVehicleModel(Long id) {
        return vehicleModelRepository.findVehicleModelById(id);
    }

    @Override
    public void save(VehicleModel vehicleModel) {
        vehicleModelRepository.save(vehicleModel);
    }
}
