package com.mobisure.springapp.service;

import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.user.InterventionEmployee;

import java.util.List;

public interface InterventionService {

    Intervention getIntervention(Long id);

    List<Intervention> getInterventions();

    List<Intervention> getInterventionsByStatus(Intervention.Status status);

    List<Intervention> getUserRelatedInterventions(Long userId);

    void setInterventionStatus(Long id, Intervention.Status status);

    void setInterventionAssignedEmployee(Long id, InterventionEmployee interventionEmployee);

    void save(Intervention intervention);
}
