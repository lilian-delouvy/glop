package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.NewMobilityCoverType;
import com.mobisure.springapp.model.transport.VehicleCoverType;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.Insurance;
import com.mobisure.springapp.repository.InsuranceRepository;
import com.mobisure.springapp.repository.NewMobilityInsuranceRepository;
import com.mobisure.springapp.repository.TravelInsuranceRepository;
import com.mobisure.springapp.repository.VehicleInsuranceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsuranceServiceImpl implements InsuranceService {

    @Autowired
    InsuranceRepository insuranceRepository;

    @Autowired
    TravelInsuranceRepository travelInsuranceRepository;

    @Autowired
    NewMobilityInsuranceRepository newMobilityInsuranceRepository;

    @Autowired
    VehicleInsuranceRepository vehicleInsuranceRepository;


    @Override
    public List<Insurance> getInsurances() {
        return (List<Insurance>) insuranceRepository.findAll();
    }

    @Override
    public Insurance getInsuranceByID(Long id) {
        return insuranceRepository.findInsuranceById(id);
    }

    @Override
    public Insurance getInsuranceByTravelCoverType(TravelCoverType travelCoverType) {
        return travelInsuranceRepository.findTravelInsuranceByTravelCoverType(travelCoverType);
    }

    @Override
    public Insurance getInsuranceByNewMobilityCoverType(NewMobilityCoverType newMobilityCoverType) {
        return newMobilityInsuranceRepository.findNewMobilityInsuranceByNewMobilityCoverType(newMobilityCoverType);
    }

    @Override
    public Insurance getInsuranceByVehicleCoverType(VehicleCoverType vehicleCoverType) {
        return vehicleInsuranceRepository.findVehicleInsuranceByVehicleCoverType(vehicleCoverType);
    }

    @Override
    public void save(Insurance insurance) {
        insuranceRepository.save(insurance);
    }
}
