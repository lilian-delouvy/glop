package com.mobisure.springapp.service;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.ContractRequest;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.InterventionRequest;
import com.mobisure.springapp.model.user.Customer;

import java.io.InvalidClassException;
import java.util.List;

public interface ContractService {

    List<Contract> getContracts();

    Intervention requestIntervention(InterventionRequest request);

    List<Contract> getContractsByCustomer(String email);

    void save(Contract contract);

    Long requestContract(ContractRequest request, Customer customer) throws InvalidClassException;

    Contract getContract(Long id);

    List<Contract> getContractsByStatus(String status);

    void validate(Long id);

    void invalidate(Long id);
}