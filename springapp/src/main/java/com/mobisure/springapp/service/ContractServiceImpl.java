package com.mobisure.springapp.service;

import com.mobisure.springapp.model.*;
import com.mobisure.springapp.model.transport.*;
import com.mobisure.springapp.model.travel.ContractTravel;
import com.mobisure.springapp.model.travel.ContractTravelRequest;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    CustomerService customerService;

    @Autowired
    InsuranceService insuranceService;

    @Autowired
    VehicleModelService vehicleModelService;

    @Override
    public List<Contract> getContracts() {
        return (List<Contract>) contractRepository.findAll();
    }

    @Override
    public Contract getContract(Long id) {
        return contractRepository.findById(id).orElse(null);
    }

    @Override
    public List<Contract> getContractsByStatus(String param) {
        Contract.Status status = Contract.Status.valueOf(param);
        return this.contractRepository.findAllByStatus(status);
    }

    @Override
    public Intervention requestIntervention(InterventionRequest request) {
        Contract contract = this.getContract(request.getContractId());
        Intervention intervention = new Intervention(contract, request.getDate(), request.getCity(), request.getCountry(), request.getTypes(), request.getDescription());
        contract.addIntervention(intervention);
        this.save(contract);
        return intervention;
    }

    @Override
    public void validate(Long id) {
        Contract contract = this.contractRepository.findById(id).orElseThrow(()->new IllegalArgumentException("This contract doesn't exist"));
        contract.setStatus(Contract.Status.VALID);
        this.save(contract);
    }

    @Override
    public void invalidate(Long id) {
        Contract contract = this.contractRepository.findById(id).orElseThrow(()->new IllegalArgumentException("This contract doesn't exist"));
        contract.setStatus(Contract.Status.ENDED);
        this.save(contract);
    }

    @Override
    public List<Contract> getContractsByCustomer(String email) {
        return contractRepository.findAllBySubscriber_Email(email);
    }

    @Override
    public void save(Contract contract) {
        contractRepository.save(contract);
    }

    @Override
    public Long requestContract(ContractRequest request, Customer customer) throws InvalidClassException {
        Contract contract;
        if(request instanceof ContractTravelRequest)
            contract = generateContractTravel((ContractTravelRequest) request, customer);
        else if(request instanceof ContractTransportRequest)
            contract = generateContractTransport((ContractTransportRequest) request, customer);
        else throw new InvalidClassException("Invalid Request Class");
        this.contractRepository.save(contract);
        return contract.getId();
    }

    public ContractTravel generateContractTravel(ContractTravelRequest contractTravelRequest, Customer customer){
        double price;
        double optionsPrice = 0;
        List<Customer> rightHolders = new ArrayList<>();
        TravelInsurance travelInsurance = (TravelInsurance) insuranceService.getInsuranceByTravelCoverType(contractTravelRequest.getTravelCoverType());


        if (contractTravelRequest.getOptions() != null)
            for (Option option:contractTravelRequest.getOptions())
                optionsPrice+=option.getPrice();
        price = travelInsurance.getBaseDailyPrice()+optionsPrice;

        for (String email:contractTravelRequest.getRightHoldersEmails())
            rightHolders.add(this.customerService.getCustomerByEmail(email));

        if(travelInsurance.getTravelCoverType().equals(TravelCoverType.PREMIUM))
            return new ContractTravel(
                    customer,
                    rightHolders,
                    contractTravelRequest.getSubscriptionDate(),
                    price,
                    travelInsurance,
                    contractTravelRequest.getOptions());
        else
            return new ContractTravel(
                    customer,
                    rightHolders,
                    contractTravelRequest.getSubscriptionDate(),
                    contractTravelRequest.getEndOfSubscriptionDate(),
                    price,
                    travelInsurance,
                    contractTravelRequest.getCountry(),
                    contractTravelRequest.getOptions());
    }

    public ContractTransport generateContractTransport(ContractTransportRequest contractTransportRequest, Customer customer) throws InvalidClassException {
        double price;
        double optionsPrice = 0;
        double coefficient = 1;
        Transport transport;

        List<Customer> rightHolders = new ArrayList<>();
        TransportInsurance transportInsurance;

        if (contractTransportRequest instanceof ContractVehicleRequest) {
            transportInsurance = (VehicleInsurance) insuranceService.getInsuranceByVehicleCoverType(((ContractVehicleRequest) contractTransportRequest).getVehicleCoverType());
            customer.setVehicleBonusMalus(((ContractVehicleRequest) contractTransportRequest).getVehicleHistoric().getBonusMalus());
            customer.setVehicleHistoric(((ContractVehicleRequest) contractTransportRequest).getVehicleHistoric());
            customerService.save(customer);
            coefficient = customer.getVehicleBonusMalus();
            VehicleModel vehicleModel = this.vehicleModelService.getVehicleModel(((ContractVehicleRequest) contractTransportRequest).getVehicleModelId());
            transport = new Vehicle(
                    ((ContractVehicleRequest) contractTransportRequest).getYear(),
                    ((ContractVehicleRequest) contractTransportRequest).getRegistrationDate(),
                    ((ContractVehicleRequest) contractTransportRequest).getRegistrationNumber(),
                    vehicleModel
            );
        } else if (contractTransportRequest instanceof ContractNewMobilityRequest) {
            transportInsurance = (NewMobilityInsurance) insuranceService.getInsuranceByNewMobilityCoverType(((ContractNewMobilityRequest) contractTransportRequest).getNewMobilityCoverType());
            transport = ((ContractNewMobilityRequest) contractTransportRequest).getTransport();
        } else throw new InvalidClassException("Invalid Request Class");

        if (contractTransportRequest.getOptions() != null)
            for (Option option : contractTransportRequest.getOptions())
                optionsPrice += option.getPrice();
        price = coefficient * transportInsurance.getBaseDailyPrice() + optionsPrice;

        for (String email : contractTransportRequest.getRightHoldersEmails())
            rightHolders.add(this.customerService.getCustomerByEmail(email));

        return new ContractTransport(
                    customer,
                    rightHolders,
                    contractTransportRequest.getSubscriptionDate(),
                    price,
                    transportInsurance,
                    contractTransportRequest.getOptions(),
                transport
                );
    }


}
