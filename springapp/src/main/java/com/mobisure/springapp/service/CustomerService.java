package com.mobisure.springapp.service;

import com.mobisure.springapp.model.user.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    Boolean existsByEmail(String email);

    Customer getCustomerByEmail(String email);

    void save(Customer customer);
}
