package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.NewMobilityCoverType;
import com.mobisure.springapp.model.transport.VehicleCoverType;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.Insurance;

import java.util.List;

public interface InsuranceService {

    List<Insurance> getInsurances();

    Insurance getInsuranceByID(Long id);

    Insurance getInsuranceByTravelCoverType(TravelCoverType travelCoverType);

    Insurance getInsuranceByNewMobilityCoverType(NewMobilityCoverType newMobilityCoverType);

    Insurance getInsuranceByVehicleCoverType(VehicleCoverType vehicleCoverType);


    void save(Insurance insurance);
}
