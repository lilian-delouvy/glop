package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.Transport;

public interface TransportService {

    void save(Transport transport);
}