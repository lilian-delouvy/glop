package com.mobisure.springapp.service;

import com.mobisure.springapp.model.user.InsuranceEmployee;
import com.mobisure.springapp.repository.InsuranceEmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InsuranceEmployeeServiceImpl implements InsuranceEmployeeService {

    @Autowired
    InsuranceEmployeeRepository insuranceEmployeeRepository;

    @Autowired
    ContractService contractService;


    @Override
    public List<InsuranceEmployee> getInsuranceEmployees() {
        return (ArrayList<InsuranceEmployee>) this.insuranceEmployeeRepository.findAll();
    }

    @Override
    public Boolean existsByEmail(String email) {
        return insuranceEmployeeRepository.findUserByEmail(email)!=null;
    }

    @Override
    public InsuranceEmployee getInsuranceEmployeeByEmail(String email) {
        return insuranceEmployeeRepository.findUserByEmail(email);
    }

    @Override
    public void save(InsuranceEmployee insuranceEmployee) {
        insuranceEmployeeRepository.save(insuranceEmployee);
    }
}
