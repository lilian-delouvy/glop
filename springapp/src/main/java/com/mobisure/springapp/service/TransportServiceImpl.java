package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.Transport;
import com.mobisure.springapp.repository.TransportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransportServiceImpl implements TransportService {

    @Autowired
    TransportRepository transportRepository;

    @Override
    public void save(Transport transport) {
        transportRepository.save(transport);
    }
}
