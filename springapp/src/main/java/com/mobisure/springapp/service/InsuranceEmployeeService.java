package com.mobisure.springapp.service;

import com.mobisure.springapp.model.user.InsuranceEmployee;

import java.util.List;

public interface InsuranceEmployeeService {

    List<InsuranceEmployee> getInsuranceEmployees();

    Boolean existsByEmail(String email);

    InsuranceEmployee getInsuranceEmployeeByEmail(String email);

    void save(InsuranceEmployee insuranceEmployee);
}
