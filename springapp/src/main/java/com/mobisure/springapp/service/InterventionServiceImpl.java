package com.mobisure.springapp.service;

import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.user.InterventionEmployee;
import com.mobisure.springapp.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterventionServiceImpl implements InterventionService {

    @Autowired
    InterventionRepository interventionRepository;

    public void save(Intervention intervention) {
        interventionRepository.save(intervention);
    }

    @Override
    public Intervention getIntervention(Long id) {
        return this.interventionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("This intervention doesn't exist"));
    }

    @Override
    public List<Intervention> getInterventions() {
        return (List<Intervention>) this.interventionRepository.findAll();
    }

    @Override
    public List<Intervention> getInterventionsByStatus(Intervention.Status status) {
        return this.interventionRepository.findAllByStatus(status);
    }

    @Override
    public void setInterventionStatus(Long id, Intervention.Status status) {
        Intervention intervention = getIntervention(id);
        intervention.setStatus(status);
        save(intervention);
    }

    @Override
    public void setInterventionAssignedEmployee(Long id, InterventionEmployee interventionEmployee){
        Intervention intervention = getIntervention(id);
        intervention.setInterventionEmployee(interventionEmployee);
        save(intervention);
    }

    @Override
    public List<Intervention> getUserRelatedInterventions(Long userId){
        return ((List<Intervention>) this.interventionRepository.findAll())
                .stream().filter(
                        intervention -> intervention.getInterventionEmployee() != null && intervention.getInterventionEmployee().getId() == userId
                )
                .collect(Collectors.toList());
    }
}
