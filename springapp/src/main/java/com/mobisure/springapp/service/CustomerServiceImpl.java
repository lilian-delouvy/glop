package com.mobisure.springapp.service;

import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ContractService contractService;

    @Override
    public List<Customer> getCustomers(){
        return (ArrayList<Customer>) this.customerRepository.findAll();
    }

    @Override
    public Boolean existsByEmail(String email){
        return customerRepository.findUserByEmail(email) != null;
    }

    @Override
    public Customer getCustomerByEmail(String email) {
        return customerRepository.findUserByEmail(email);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }
}
