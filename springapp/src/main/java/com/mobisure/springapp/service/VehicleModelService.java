package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.VehicleModel;

import java.util.List;

public interface VehicleModelService {

    List<VehicleModel> getVehicleModels();

    VehicleModel getVehicleModel(Long id);

    void save(VehicleModel vehicleModel);
}