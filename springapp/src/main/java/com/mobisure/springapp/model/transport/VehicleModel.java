package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class VehicleModel {
    @Id
    @GeneratedValue
    private Long id;

    String brand;
    String model;
    int start_year;
    int end_year;
    int value;
    int powerDIN;
    VehicleEnergy vehicle_energy;

    public VehicleModel(String brand, String model, int start_year, int end_year, int value, int powerDIN, VehicleEnergy vehicle_energy) {
        this.brand = brand;
        this.model = model;
        this.start_year = start_year;
        this.end_year = end_year;
        this.value = value;
        this.powerDIN = powerDIN;
        this.vehicle_energy = vehicle_energy;
    }
}
