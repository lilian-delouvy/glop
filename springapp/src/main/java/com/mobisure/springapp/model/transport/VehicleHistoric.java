package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class VehicleHistoric {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    Date drivingLicenceDate;
    int nbResponsibleSinister;
    int nbNonResponsibleSinister;
    int nbInsuredMonth;
    double bonusMalus;

    public VehicleHistoric(Date drivingLicenceDate, int nbResponsibleSinister, int nbNonResponsibleSinister, int nbInsuredMonth, double bonusMalus) {
        this.drivingLicenceDate = drivingLicenceDate;
        this.nbResponsibleSinister = nbResponsibleSinister;
        this.nbNonResponsibleSinister = nbNonResponsibleSinister;
        this.nbInsuredMonth = nbInsuredMonth;
        this.bonusMalus = bonusMalus;
    }
}
