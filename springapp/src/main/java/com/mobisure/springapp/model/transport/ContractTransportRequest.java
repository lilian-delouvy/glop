package com.mobisure.springapp.model.transport;

import com.mobisure.springapp.model.ContractRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ContractTransportRequest extends ContractRequest {

}
