package com.mobisure.springapp.model.transport;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Inheritance
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Vehicle.class, name = "VEHICLE"),
        @JsonSubTypes.Type(value = NewMobility.class, name = "NEW_MOBILITY")
})
@Entity
public abstract class Transport {
    String brand;
    String model;
    int year;
    int value;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    public Transport(String brand, String model, int year, int value) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.value = value;
    }
}
