package com.mobisure.springapp.model.transport;

public enum NewMobilityType {
    SCOOTER, BIKE
}
