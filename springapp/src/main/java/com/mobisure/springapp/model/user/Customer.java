package com.mobisure.springapp.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.transport.VehicleHistoric;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public class Customer extends User{

    @Column(name = "firstname")
    String firstname;

    @Column(name = "lastname")
    String lastname;

    @Column(name="date_of_birth")
    Date dateOfBirth;

    @Column(name="address")
    String address;

    @Column(name = "phone_number")
    String phoneNumber;

    @Column(name = "list_of_contract")
    @JsonIgnoreProperties("subscriber")
    @OneToMany(mappedBy = "subscriber", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    List<Contract> contractList = new ArrayList<>();

    @Column(name = "vehicle_bonus_malus")
    double VehicleBonusMalus = 1.0;

    @OneToOne(cascade = CascadeType.ALL)
    VehicleHistoric vehicleHistoric;

    public Customer(String email, String password) {
        super(email, password);
    }

    public Customer(String email, String password, String firstname, String lastname, Date dateOfBirth, String address, String phoneNumber) {
        super(email, password);
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getRole() {
        return "CUSTOMER";
    }

    @Override
    public String toString() {
        return "Customer{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", contractList=" + contractList +
                '}';
    }
}
