package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Vehicle extends Transport {
    Date registrationDate;
    String registrationNumber;
    @OneToOne
    VehicleModel vehicleModel;

    public Vehicle(int year, Date registrationDate, String registrationNumber, VehicleModel vehicleModel) {
        super(vehicleModel.brand, vehicleModel.getModel(), year, vehicleModel.getValue());
        this.registrationDate = registrationDate;
        this.registrationNumber = registrationNumber;
        this.vehicleModel = vehicleModel;
    }
}
