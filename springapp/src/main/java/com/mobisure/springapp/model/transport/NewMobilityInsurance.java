package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class NewMobilityInsurance extends TransportInsurance {
    @Column(unique=true)
    NewMobilityCoverType newMobilityCoverType;

    public NewMobilityInsurance(double baseDailyPrice, NewMobilityCoverType newMobilityCoverType) {
        super(baseDailyPrice,"NEW_MOBILITY");
        this.newMobilityCoverType = newMobilityCoverType;
    }
}