package com.mobisure.springapp.model.travel;

import com.mobisure.springapp.model.Insurance;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public class TravelInsurance extends Insurance {
    @Column(unique=true)
    TravelCoverType travelCoverType;

    int minDaysLength;

    int maxDaysLength;

    public TravelInsurance(double baseDailyPrice, TravelCoverType travelCoverType, int minDaysLength, int maxDaysLength) {
        super(baseDailyPrice,"TRAVEL");
        this.travelCoverType = travelCoverType;
        this.minDaysLength = minDaysLength;
        this.maxDaysLength = maxDaysLength;
    }
}
