package com.mobisure.springapp.model.transport;

public enum VehicleCoverType {
    THIRD_PARTY,THIRD_PARTY_THEFT,INTEGRAL
}
