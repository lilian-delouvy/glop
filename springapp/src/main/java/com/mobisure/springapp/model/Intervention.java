package com.mobisure.springapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobisure.springapp.model.travel.Country;
import com.mobisure.springapp.model.user.InterventionEmployee;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public class Intervention {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties("interventionList")
    @JoinColumn(name = "contract_id", nullable = false)
    Contract contract;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("interventionList")
    @JoinColumn(name = "employee_id")
    InterventionEmployee interventionEmployee;

    Date date;

    double cost;

    String city;

    Country country;

    public enum Status {
        ACCEPTED,PENDING,ENDED
    }

    Status status;

    public enum Type {
        INJURY,MATERIAL,UNDEFINED
    }

    @ElementCollection(targetClass = Type.class)
    @CollectionTable(name = "intervention_types", joinColumns = @JoinColumn(name = "id"))
    @Enumerated(EnumType.STRING)
    List<Type> types;

    String description;

    public Intervention(Contract contract, Date date, String city, Country country, List<Type> types, String description) {
        this.contract = contract;
        this.date = date;
        this.city = city;
        this.country = country;
        this.status = Status.PENDING;
        this.types = types;
        this.description = description;
    }
}
