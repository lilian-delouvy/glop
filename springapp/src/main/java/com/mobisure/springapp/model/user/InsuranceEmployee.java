package com.mobisure.springapp.model.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class InsuranceEmployee extends User{

    public InsuranceEmployee(String email, String password) {
        super(email, password);
    }

    @Override
    public String getRole() {
        return "INSURANCE_EMPLOYEE";
    }

}
