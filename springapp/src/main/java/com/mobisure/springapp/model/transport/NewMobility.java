package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class NewMobility extends Transport {
    NewMobilityType newMobilityType;
    Boolean isElectric;

    public NewMobility(String brand, String model, int year, int value, NewMobilityType newMobilityType, Boolean isElectric) {
        super(brand, model, year, value);
        this.newMobilityType = newMobilityType;
        this.isElectric = isElectric;
    }
}
