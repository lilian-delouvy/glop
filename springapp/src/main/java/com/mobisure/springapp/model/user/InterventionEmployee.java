package com.mobisure.springapp.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobisure.springapp.model.Intervention;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Inheritance
@Entity
public abstract class InterventionEmployee extends User{

    public InterventionEmployee(String email, String password){
        super(email, password);
        this.interventions = new ArrayList<>();
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("interventionEmployee")
    @JoinColumn(name = "intervention_id", nullable = false)
    List<Intervention> interventions;
}
