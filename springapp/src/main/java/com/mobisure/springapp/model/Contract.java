package com.mobisure.springapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobisure.springapp.model.user.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public abstract class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties("contractList")
    @JoinColumn(name = "customer_id", nullable = false)
    Customer subscriber;

    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("contractList")
    @JoinColumn(name = "rightHolder_id", nullable = false)
    List<Customer> rightHolders;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnoreProperties("contract")
    @JoinColumn(name = "intervention_id", nullable = false)
    List<Intervention> interventions = new ArrayList<>();

    Date subscriptionDate;

    Date endOfSubscriptionDate;

    double dailyPrice;

    String type;

    @ManyToMany
    List<Option> options = new ArrayList<>();

    public enum Status {
        VALID,PENDING,ENDED
    }

    Status status;

    public Contract(Customer subscriber, List<Customer> rightHolders, Date subscriptionDate, Date endOfSubscriptionDate, double dailyPrice, List<Option> options, String type) {
        this.subscriber = subscriber;
        this.rightHolders = rightHolders;
        this.subscriptionDate = subscriptionDate;
        this.endOfSubscriptionDate = endOfSubscriptionDate;
        this.dailyPrice = dailyPrice;
        this.options = options;
        this.status = Status.PENDING;
        this.type = type;
    }

    public Contract(Customer subscriber, List<Customer> rightHolders, Date subscriptionDate, double dailyPrice, List<Option> options, String type) {
        this.subscriber = subscriber;
        this.rightHolders = rightHolders;
        this.subscriptionDate = subscriptionDate;
        this.dailyPrice = dailyPrice;
        this.options = options;
        this.status = Status.PENDING;
        this.type = type;
    }

    public void addIntervention(Intervention intervention){
        this.interventions.add(intervention);
    }
}
