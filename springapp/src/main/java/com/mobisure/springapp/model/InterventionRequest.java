package com.mobisure.springapp.model;

import com.mobisure.springapp.model.travel.Country;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class InterventionRequest implements Serializable {
    Long contractId;
    Date date;
    String city;
    Country country;
    List<Intervention.Type> types;
    String description;
}
