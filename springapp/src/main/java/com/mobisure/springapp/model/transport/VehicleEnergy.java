package com.mobisure.springapp.model.transport;

public enum VehicleEnergy {
    DIESEL, GASOLINE, ELECTRIC, HYBRID
}
