package com.mobisure.springapp.model.travel;

import com.mobisure.springapp.model.ContractRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class ContractTravelRequest extends ContractRequest {
    TravelCoverType travelCoverType;
    Country country;
    Date endOfSubscriptionDate;
}
