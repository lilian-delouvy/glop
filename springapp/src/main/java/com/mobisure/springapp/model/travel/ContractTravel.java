package com.mobisure.springapp.model.travel;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Option;
import com.mobisure.springapp.model.user.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public class ContractTravel extends Contract {
    @OneToOne
    TravelInsurance travelInsurance;

    Country country;

    public ContractTravel(Customer subscriber, List<Customer> rightHolders, Date subscriptionDate, Date endOfSubscriptionDate, double dailyPrice, TravelInsurance travelInsurance, Country country, List<Option> options) {
        super(subscriber, rightHolders, subscriptionDate, endOfSubscriptionDate, dailyPrice, options, "TRAVEL");
        this.travelInsurance = travelInsurance;
        this.country = country;
    }

    public ContractTravel(Customer subscriber, List<Customer> rightHolders, Date subscriptionDate, double dailyPrice, TravelInsurance travelInsurance, List<Option> options) {
        super(subscriber, rightHolders, subscriptionDate, dailyPrice, options,"TRAVEL");
        Date endOfSubscriptionDate = Date.from(LocalDateTime.from(subscriptionDate.toInstant().atZone(ZoneId.of("UTC"))).plusDays(365).atZone(ZoneId.of("UTC")).toInstant());
        this.setEndOfSubscriptionDate(endOfSubscriptionDate);
        this.travelInsurance = travelInsurance;
    }
}
