package com.mobisure.springapp.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mobisure.springapp.model.transport.ContractNewMobilityRequest;
import com.mobisure.springapp.model.transport.ContractVehicleRequest;
import com.mobisure.springapp.model.travel.ContractTravelRequest;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ContractTravelRequest.class, name = "contractTravelRequest"),
        @JsonSubTypes.Type(value = ContractVehicleRequest.class, name = "contractVehicleRequest"),
        @JsonSubTypes.Type(value = ContractNewMobilityRequest.class, name = "contractNewMobilityRequest")
})
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public abstract class ContractRequest implements Serializable {
    List<Option> options;
    List<String> rightHoldersEmails;
    Date subscriptionDate;
}
