package com.mobisure.springapp.model.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Partner extends InterventionEmployee{

    public Partner(String email, String password){
        super(email, password);
    }

    @Override
    public String getRole() {
        return "PARTNER";
    }
}
