package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class VehicleInsurance extends TransportInsurance {
    @Column(unique=true)
    VehicleCoverType vehicleCoverType;

    public VehicleInsurance(double baseDailyPrice, VehicleCoverType vehicleCoverType) {
        super(baseDailyPrice,"VEHICLE");
        this.vehicleCoverType = vehicleCoverType;
    }
}