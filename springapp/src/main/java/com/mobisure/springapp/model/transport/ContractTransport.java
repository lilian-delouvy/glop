package com.mobisure.springapp.model.transport;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Option;
import com.mobisure.springapp.model.user.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
@Entity
public class ContractTransport extends Contract {
    @OneToOne
    TransportInsurance transportInsurance;

    @OneToOne(cascade = {CascadeType.ALL})
    Transport transport;

    public ContractTransport(Customer subscriber, List<Customer> rightHolders, Date subscriptionDate, double dailyPrice, TransportInsurance transportInsurance, List<Option> options, Transport transport) {
        super(subscriber, rightHolders, subscriptionDate, dailyPrice, options, "TRANSPORT");
        Date endOfSubscriptionDate = Date.from(LocalDateTime.from(subscriptionDate.toInstant().atZone(ZoneId.of("UTC"))).plusDays(365).atZone(ZoneId.of("UTC")).toInstant());
        this.setEndOfSubscriptionDate(endOfSubscriptionDate);
        this.transportInsurance = transportInsurance;
        this.transport = transport;
    }
}
