package com.mobisure.springapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Inheritance
@Entity
public abstract class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    long id;

    double baseDailyPrice;

    String type;

    @OneToMany(fetch = FetchType.EAGER)
    List<Option> availableOptions;

    public Insurance(double baseDailyPrice, String type) {
        this.baseDailyPrice = baseDailyPrice;
        this.type = type;
    }
}