package com.mobisure.springapp.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContractVehicleRequest extends ContractTransportRequest {
    VehicleCoverType vehicleCoverType;

    VehicleHistoric vehicleHistoric;

    Long vehicleModelId;

    int year;

    Date registrationDate;

    String registrationNumber;
}
