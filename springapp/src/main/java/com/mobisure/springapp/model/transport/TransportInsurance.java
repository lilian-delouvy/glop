package com.mobisure.springapp.model.transport;

import com.mobisure.springapp.model.Insurance;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Inheritance;

@AllArgsConstructor
@Inheritance
@Entity
public abstract class TransportInsurance extends Insurance {
    public TransportInsurance(double baseDailyPrice, String type) {
        super(baseDailyPrice, type);
    }
}