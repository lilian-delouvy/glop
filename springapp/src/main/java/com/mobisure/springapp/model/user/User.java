package com.mobisure.springapp.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Inheritance
@Entity
@Table(name = "user_table")
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "email", unique = true)
    String email;

    @JsonIgnore
    @Column(name = "password")
    String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public abstract String getRole();
}
