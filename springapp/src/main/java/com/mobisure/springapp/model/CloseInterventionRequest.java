package com.mobisure.springapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
public class CloseInterventionRequest {
    Long interventionId;
    Long cost;
    String description;
}
