package com.mobisure.springapp.service;

import com.mobisure.springapp.model.transport.VehicleEnergy;
import com.mobisure.springapp.model.transport.VehicleModel;
import com.mobisure.springapp.repository.VehicleModelRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VehicleModelServiceImplTest {

    @InjectMocks
    VehicleModelServiceImpl vehicleModelService;

    @Mock
    VehicleModelRepository vehicleModelRepository;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testGetVehicleModels() {
        List<VehicleModel> vehicleModels = new ArrayList<>();

        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1700, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1750, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1800, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1900, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 2000, 150, VehicleEnergy.GASOLINE));
        when(vehicleModelRepository.findAll()).thenReturn(vehicleModels);
        assertEquals(vehicleModels.size(), vehicleModelService.getVehicleModels().size());
    }

    @Test
    void testGetVehicleModel() {
        VehicleModel vehicleModel = new VehicleModel(34L, "Test", "Test 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE);
        when(vehicleModelRepository.findVehicleModelById(34L)).thenReturn(vehicleModel);
        assertNotNull(vehicleModelService.getVehicleModel(34L));
        assertEquals(34L, vehicleModelService.getVehicleModel(34L).getId());
    }

    @Test
    void testSave() {
        List<VehicleModel> vehicleModels = new ArrayList<>();

        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1700, 85, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1750, 95, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1800, 110, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1900, 140, VehicleEnergy.GASOLINE));
        vehicleModels.add(new VehicleModel("Renault", "Mégane 1", 1995, 2001, 2000, 150, VehicleEnergy.GASOLINE));

        VehicleModel vehicleModel = new VehicleModel(56L, "Test", "Test 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE);

        doAnswer(invocation -> {
            VehicleModel toSave = invocation.getArgument(0);
            vehicleModels.add(toSave);
            return null;
        }).when(vehicleModelRepository).save(any(VehicleModel.class));

        doAnswer(invocation -> {
            Long id = invocation.getArgument(0);
            for (VehicleModel v : vehicleModels)
                if (Objects.equals(v.getId(), id)) return v;
            return null;
        }).when(vehicleModelRepository).findVehicleModelById(anyLong());

        assertNull(vehicleModelRepository.findVehicleModelById(vehicleModel.getId()));
        vehicleModelService.save(vehicleModel);
        assertNotNull(vehicleModelRepository.findVehicleModelById(vehicleModel.getId()));
    }
}