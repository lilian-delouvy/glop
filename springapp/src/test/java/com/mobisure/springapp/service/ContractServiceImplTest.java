package com.mobisure.springapp.service;

import com.mobisure.springapp.model.Contract;
import com.mobisure.springapp.model.Intervention;
import com.mobisure.springapp.model.InterventionRequest;
import com.mobisure.springapp.model.transport.*;
import com.mobisure.springapp.model.travel.ContractTravel;
import com.mobisure.springapp.model.travel.Country;
import com.mobisure.springapp.model.travel.TravelCoverType;
import com.mobisure.springapp.model.travel.TravelInsurance;
import com.mobisure.springapp.model.user.Customer;
import com.mobisure.springapp.repository.ContractRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.mobisure.springapp.util.data.InitData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContractServiceImplTest {

    @InjectMocks
    ContractServiceImpl contractService;

    @Mock
    ContractRepository contractRepository;

    @Mock
    CustomerService customerService;

    @Mock
    InsuranceService insuranceService;

    TravelInsurance travelInsurancePremium = new TravelInsurance(travelPremiumPrice, TravelCoverType.PREMIUM, -1, -1);
    TravelInsurance travelInsuranceLong = new TravelInsurance(travelLongPrice, TravelCoverType.LONG, 30, -1);
    TravelInsurance travelInsuranceShort = new TravelInsurance(travelShortPrice, TravelCoverType.SHORT, 0, 30);

    VehicleInsurance vehicleInsuranceThird = new VehicleInsurance(vehicleThirdPrice, VehicleCoverType.THIRD_PARTY);
    VehicleInsurance vehicleInsuranceThirdTheft = new VehicleInsurance(vehicleThirdTheftPrice, VehicleCoverType.THIRD_PARTY_THEFT);
    VehicleInsurance vehicleInsuranceIntegral = new VehicleInsurance(vehicleIntegralPrice, VehicleCoverType.INTEGRAL);

    NewMobilityInsurance newMobilityInsuranceTheft = new NewMobilityInsurance(newMobilityTheftPrice, NewMobilityCoverType.THEFT);
    NewMobilityInsurance newMobilityInsuranceIntegral = new NewMobilityInsurance(newMobilityIntegralPrice, NewMobilityCoverType.INTEGRAL);

    Customer c1 = new Customer("joffreyd59@gmail.com", "password", "Joffrey", "Delva", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");
    Customer c2 = new Customer("lilian.delouvy@gmail.com", "password", "Lilian", "Delouvy", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");
    Customer c3 = new Customer("conrad.baudelet@gmail.com", "password", "Conrad", "Baudelet", new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"), "1 rue du test, 00000, Test", "0600000000");

    List<Contract> contracts;

    ContractServiceImplTest() throws ParseException {
    }

    @BeforeEach
    void setUp() {
        List<Contract> contracts = new ArrayList<>();

        Date date = new Date();
        ContractTravel cT1 = new ContractTravel(c1,
                Collections.singletonList(c1),
                date,
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).plusDays(100).atZone(ZoneId.of("UTC")).toInstant()),
                travelInsuranceLong.getBaseDailyPrice() * 0.90,
                travelInsuranceLong,
                Country.SWITZERLAND,
                new ArrayList<>()
        );
        cT1.setStatus(Contract.Status.VALID);
        contracts.add(cT1);
        ContractTravel cT2 = new ContractTravel(c1,
                Arrays.asList(c1, c2),
                date,
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).plusDays(10).atZone(ZoneId.of("UTC")).toInstant()),
                travelInsuranceShort.getBaseDailyPrice() * 1.2,
                travelInsuranceShort,
                Country.BELGIUM,
                new ArrayList<>()
        );
        cT2.setStatus(Contract.Status.PENDING);
        contracts.add(cT2);
        ContractTravel cT3 = new ContractTravel(c3,
                Collections.singletonList(c3),
                date,
                travelInsurancePremium.getBaseDailyPrice(),
                travelInsurancePremium,
                new ArrayList<>()
        );
        cT3.setStatus(Contract.Status.VALID);
        contracts.add(cT3);

        VehicleModel vehicleModel = new VehicleModel("Renault", "Mégane 1", 1995, 2001, 1500, 70, VehicleEnergy.GASOLINE);
        Vehicle vehicle = new Vehicle(
                vehicleModel.getStart_year(),
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).minusDays(10).atZone(ZoneId.of("UTC")).toInstant()),
                "AM-695-FS",
                vehicleModel
        );
        ContractTransport cV1 = new ContractTransport(
                c1,
                Collections.singletonList(c1),
                date,
                vehicleInsuranceIntegral.getBaseDailyPrice(),
                vehicleInsuranceIntegral,
                new ArrayList<>(),
                vehicle
        );
        contracts.add(cV1);

        NewMobility newMobility = new NewMobility("Xiaomi", "Pro 2", 2018, 300, NewMobilityType.SCOOTER, true);
        ContractTransport cNM2 = new ContractTransport(c2,
                Collections.singletonList(c2),
                date,
                newMobilityInsuranceTheft.getBaseDailyPrice(),
                newMobilityInsuranceTheft,
                new ArrayList<>(),
                newMobility
        );
        contracts.add(cNM2);

        this.contracts = contracts;
    }

    @Test
    void getContracts() {
        when(contractRepository.findAll()).thenReturn(this.contracts);

        assertEquals(this.contracts.size(), contractService.getContracts().size());
    }

    @Test
    void getContract() {
        Contract contract = this.contracts.get(0);
        contract.setId(34L);
        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));
        assertNotNull(contractService.getContract(34L));
        assertEquals(34L, contractService.getContract(34L).getId());
    }

    @Test
    void getContractsByStatus() {
        when(contractRepository.findAllByStatus(Contract.Status.VALID)).thenReturn(Arrays.asList(this.contracts.get(0), this.contracts.get(3)));
        assertEquals(2, contractService.getContractsByStatus(String.valueOf(Contract.Status.VALID)).size());
    }

    @Test
    void requestIntervention() {
        when(contractRepository.findById(this.contracts.get(0).getId())).thenReturn(Optional.ofNullable(this.contracts.get(0)));
        InterventionRequest interventionRequest = new InterventionRequest(this.contracts.get(0).getId(), new Date(), "Paris", Country.FRANCE, Collections.singletonList(Intervention.Type.INJURY), "ouille");
        Intervention intervention = contractService.requestIntervention(interventionRequest);
        assertEquals(this.contracts.get(0).getId(), intervention.getContract().getId());
        assertEquals(interventionRequest.getDate(), intervention.getDate());
        assertEquals(interventionRequest.getDescription(), intervention.getDescription());
    }

    @Test
    void validate() {
        Contract contract = this.contracts.get(0);
        contract.setId(34L);
        contract.setStatus(Contract.Status.PENDING);

        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));
        when(contractRepository.save(any())).thenReturn(any());
        contractService.validate(34L);
        assertTrue(contractRepository.findById(34L).isPresent());
        assertEquals(Contract.Status.VALID, contractRepository.findById(34L).get().getStatus());
    }

    @Test
    void invalidate() {
        Contract contract = this.contracts.get(0);
        contract.setId(34L);
        contract.setStatus(Contract.Status.VALID);

        when(contractRepository.findById(contract.getId())).thenReturn(Optional.of(contract));
        when(contractRepository.save(any())).thenReturn(any());
        contractService.invalidate(34L);
        assertTrue(contractRepository.findById(34L).isPresent());
        assertEquals(Contract.Status.ENDED, contractRepository.findById(34L).get().getStatus());
    }

    @Test
    void getContractsByCustomer() {
        when(contractRepository.findAllBySubscriber_Email(c1.getEmail())).thenReturn(Arrays.asList(this.contracts.get(0), this.contracts.get(1), this.contracts.get(3)));
        for (Contract contract : contractService.getContractsByCustomer(c1.getEmail()))
            assertEquals(c1, contract.getSubscriber());
    }

    @Test
    void save() {
        Date date = new Date();
        ContractTravel contract = new ContractTravel(c3,
                Collections.singletonList(c3),
                date,
                Date.from(LocalDateTime.from(date.toInstant().atZone(ZoneId.of("UTC"))).plusDays(100).atZone(ZoneId.of("UTC")).toInstant()),
                travelInsuranceLong.getBaseDailyPrice() * 0.90,
                travelInsuranceLong,
                Country.FRANCE,
                new ArrayList<>()
        );
        contract.setId(560L);
        doAnswer(invocation -> {
            Contract toSave = invocation.getArgument(0);
            this.contracts.add(toSave);
            return null;
        }).when(contractRepository).save(any(Contract.class));

        doAnswer(invocation -> {
            Long id = invocation.getArgument(0);
            for (Contract c : this.contracts)
                if (Objects.equals(c.getId(), id)) return Optional.of(c);
            return Optional.empty();
        }).when(contractRepository).findById(anyLong());
        assertNull(contractService.getContract(560L));
        contractService.save(contract);
        assertNotNull(contractService.getContract(560L));
    }

    @Test
    void requestContract() {
    }

    @Test
    void generateContractTravel() {
    }

    @Test
    void generateContractTransport() {
    }
}