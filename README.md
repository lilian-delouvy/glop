# MobiSure

 - [About](#about)
 - [Installation](#installation)
 - [Testing](#testing)

## About

This project is the Fuse-IT answer to the MobiSure tender, the project will provide a solution that will facilitate the work of employees and partners of assurmob as well as the management by clients of their insurance.

It is made from three parts: the spring app (back), the react app (web front) and the react native app (mobile front).

### Warning

The current project is in a "Work In Progress" state. This readme file will be updated throughout the development of the project.

## Installation

As said before, the projects will be divided in several parts which will themselves use several librairies. We recommend to every member of the development team the use of docker for the installation and development of the projetct, but there is a way to install each part of the projects locally.

### 1. Get the project from gitlab

To get the project you only need to creeate a directory and to clone the project from gitlab inside it (ensure that you are part of the project on gitlab and that you have setup your SSH keys on your setup and your gitlab account, see https://docs.gitlab.com/ee/ssh/).

```shell
$ mkdir mobisure
$ cd mobisure 
$ git clone git@gitlab.com:lilian-delouvy/glop.git
```
 
### 2.1 Docker
To install the project in a container via Docker, ensure that Docker is installed in your machine (see https://docs.docker.com/get-docker/), and use the following command in the mobisure directory:

```sh
$ docker-compose up
```

If everything works fine the application should be accessible, pass to part 3.

### 2.2 Spring Boot
To setup the Spring Boot application locally you'll need an IDE to setup the environment variables, we recommand the use of Intellij. First you need to build the project, use the following commands in the mobisure directory:
```sh
$ cd springapp
$ ./mvnw clean install -DskipTests -Pdev
```
For development without docker, we use an H2 Database.
If everything works fine the Spring Boot application should be working fine.

### 2.3 React.js
To setup the React application locally you'll need to install node.js, or at least npm (see https://nodejs.org/en/ and https://docs.npmjs.com/downloading-and-installing-node-js-and-npm). Now you need to install the dependancies, use the following commands in the mobisure directory:
```sh
$ cd reactapp
$ npm install
```


### 2.4 React Native
To setup the React Native application locally you'll need an Android/iOS emulator:
```sh
$ cd reactnativeapp
$ npm install
$ npx react-native run-android
$ npx react-native run-ios
```

### 2.5 Electron
To generate Electron release build, please run the following command at the root of the reactapp folder:
```
electron-packager . --platform=<your_platform> --overwrite --out=release-builds
```
For MacOS/Linux, <your_platform> must be replaced by "darwin".
For Windows, it must be replaced by "win32".

## Testing
These are the URL for the web application:
- Development: http://localhost:3000
- Production: https://mobisure-react.herokuapp.com/#/
- Metabase: https://mobisure-metabase.herokuapp.com/auth/login

These are the route to home page for the web application:
- Customer: {{URL}}/ 
- Insurance employee: {{URL}}/insurance-employee/login
- Partner employee: {{URL}}/partner/login
- Medical employee: {{URL}}/medical-employee/login

These are the accounts available:
- Insurance employee: 
    - Development & Production: admin@assurmob.com
- Partner employee:
    - Development:partner@partner.com
    - Production:partnerDataSet@partner.com
- Medical employee:
    - Development:medical@medical.com
    - Production:medicalDataSet@medical.com

We nedd to perform sql requests to add new accounts to the application.
- Development: 
    - H2: 
        - console H2: http://localhost:8080/h2
        - username: sa
        - password: password
    - Docker Postgres:  
        - URL: jdbc:postgresql://postgres_sql:5432/postgres
        - username: admin
        - password: admin
- Production: 
    - credentials & URL: secret
