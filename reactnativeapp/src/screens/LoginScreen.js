import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import CustomTextInput from '../components/CustomTextInput';
import CustomButton from '../components/CustomButton';
import axios from 'axios';
import { getSpringUrl } from 'reactapp/src/urls';
import { RetrieveToken, StoreToken } from '../utils/token';
import i18n from '../i18n';


export default function LoginScreen({ navigation }) {
  const [state, setState] = useState({ email: "", password: "" })
  const signin = () => {
    axios.post(getSpringUrl() + "/auth/customer/signin", { email: state.email, password: state.password }).then(function (response) {
      StoreToken(response.data.token)
      RetrieveToken()
      navigation.navigate('HomeScreen')
    })
      .catch(function (error) {
        console.log(error);


      })
  }
  return (
    <View style={styles.container}>


      <Image source={require('../assets/logo.png')} style={{ width: 340, height: 150 }} />
      <View style={{ marginTop: 40 }}>
        <CustomTextInput label={i18n.t('emailInput')} isConfidential={false} onChange={(email) => { setState({ ...state, email: email }) }}></CustomTextInput>
        <CustomTextInput label={i18n.t('passwordInput')} isConfidential={true} onChange={(password) => { setState({ ...state, password: password }) }}></CustomTextInput>

      </View>

      <View style={{ marginTop: 40 }}>
        <CustomButton label={i18n.t('loginBtn')} style={{}} mode="contained" onPress={signin}>Login</CustomButton>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: 20,
  },
});

