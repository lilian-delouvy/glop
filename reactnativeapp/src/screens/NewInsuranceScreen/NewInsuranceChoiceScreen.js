import { StyleSheet, View, Text, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import InsurancePreview from '../../components/InsurancePreview';
import NewsPreview from '../../components/NewsPreview';
import { Appbar } from 'react-native-paper';
import CustomAppbar from '../../components/CustomAppbar';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import ConsumerAPI from '../../api/ConsumerAPI';
import I18n from '../../i18n';
import { RetrieveToken } from '../../utils/token';
import NewTravelInsuranceForm from './NewTravelInsuranceForm';
import NewVehicleInsuranceForm from './NewVehicleInsuranceForm';

const NewInsuranceChoiceScreenFactory=(insurance)=>{
    console.log(insurance);
    switch(insurance.type){
        case "TRAVEL":
            return <NewTravelInsuranceForm insurance={insurance}></NewTravelInsuranceForm>
        case "TRANSPORT":
            return <NewVehicleInsuranceForm insurance={insurance}></NewVehicleInsuranceForm>
        default:
            return <NewVehicleInsuranceForm insurance={insurance}></NewVehicleInsuranceForm>
    }
}

export default NewInsuranceChoiceScreen = ({navigation,route }) => {


    useEffect(() => {
        
    }, [])


    const styles = StyleSheet.create({
        container: {
            paddingLeft: 20,
            paddingTop: 20
        },
        headers3: {
            fontSize: 30
        }
    });

    

    return (

        <SafeAreaView>
            <CustomAppbar navigation={navigation}></CustomAppbar>
            {
               NewInsuranceChoiceScreenFactory(route.params.insurance)
            }
        </SafeAreaView>
    )
}