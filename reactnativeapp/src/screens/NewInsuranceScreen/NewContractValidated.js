import { StyleSheet, View, Text, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import I18n from '../../i18n';
import CustomAppbar from '../../components/CustomAppbar';



export default NewContractValidated = ({ navigation }) => {

  

    const styles = StyleSheet.create({
        container: {
            paddingLeft: 20,
            paddingTop: 20
        },
        headers3: {
            fontSize: 30
        }
    });


    return (
        <SafeAreaView>
            <CustomAppbar navigation={navigation}></CustomAppbar>
            <Text>
                Votre assurance est en cours de validation par un de nos employé. ⌛️
            </Text>
            <Button style={{ marginTop: 20 }} mode="contained" onPress={() => navigation.navigate('MyInsuranceScreen')} >Retour à l'accueil </Button>
        </SafeAreaView>

    )

}



