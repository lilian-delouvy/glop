import { StyleSheet, View, Text, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import InsuranceAPI from '../../api/InsuranceAPI';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import InsuranceUtils from '../../models/InsuranceUtils';
import I18n from '../../i18n';
import CustomAppbar from '../../components/CustomAppbar';



export default NewInsuranceScreen = ({ navigation }) => {

    const [state, setState] = useState({
        insuranceList: []
    })

    useEffect(() => {
        InsuranceAPI.getAllInsuranceAvailable().then((value) => {
            setState({ ...state, insuranceList: value.data })
        }).catch((error) => {
            console.log(value.error);
        });

    }, [])

    onInsuranceDetailPressed = (insurance) => {
        navigation.navigate('NewInsuranceChoiceScreen',{insurance:insurance});
    }


    const styles = StyleSheet.create({
        container: {
            paddingLeft: 20,
            paddingTop: 20
        },
        headers3: {
            fontSize: 30
        }
    });

    const InsurancePreviewFactory=({value,navigation})=>{
        switch(value.type){
            case "TRAVEL":
                return (<View style={{ marginTop: 20 }}>
                    <Card>
                        <Card.Title title={InsuranceUtils.getNameFromInsuranceType(value.type)} subtitle={InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(value.type, value.travelCoverType)} />
                        <Card.Cover source={{ uri: I18n.t(['contractType',value.type,'imageUrl']) }} />
                        <Card.Actions>
                            <Button mode="contained" onPress={() => onInsuranceDetailPressed(value)}>{I18n.t('choose')} </Button>
                            <Button style={{ marginLeft: 20 }} mode="contained">{I18n.t('details')}</Button>
                        </Card.Actions>
                    </Card>
                </View>
                )
            case "VEHICLE":
                return (<View style={{ marginTop: 20 }}>
                    <Card>
                        <Card.Title title={InsuranceUtils.getNameFromInsuranceType(value.type)} subtitle={InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(value.type, value.vehicleCoverType)} />
                        <Card.Cover source={{ uri: I18n.t(['contractType',value.type,'imageUrl']) }} />
                        <Card.Actions>
                            <Button mode="contained" onPress={() => onInsuranceDetailPressed(value)}>{I18n.t('choose')} </Button>
                            <Button style={{ marginLeft: 20 }} mode="contained">{I18n.t('details')}</Button>
                        </Card.Actions>
                    </Card>
                </View>
                ) 
            case "NEW_MOBILITY":
                return (<View style={{ marginTop: 20 }}>
                    <Card>
                        <Card.Title title={InsuranceUtils.getNameFromInsuranceType(value.type)} subtitle={InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(value.type, value.newMobilityCoverType)} />
                        <Card.Cover source={{ uri: I18n.t(['contractType',value.type,'imageUrl']) }} />
                        <Card.Actions>
                            <Button mode="contained" onPress={() => onInsuranceDetailPressed(value)}>{I18n.t('choose')} </Button>
                            <Button style={{ marginLeft: 20 }} mode="contained">{I18n.t('details')}</Button>
                        </Card.Actions>
                    </Card>
                </View>
                )
        }
        return (<Text>Default card</Text>)
    }

    return (
        <SafeAreaView>
            <CustomAppbar navigation={navigation}></CustomAppbar>
            <ScrollView>
                {
                    state.insuranceList.map((value) => {
                        return (
                            <InsurancePreviewFactory value={value} navigation={navigation}/>
                        )
                    })
                }
            </ScrollView>
        </SafeAreaView>

    )

}



