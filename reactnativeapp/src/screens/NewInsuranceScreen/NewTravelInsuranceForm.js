import { StyleSheet, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomAppbar from '../../components/CustomAppbar';
import { Text } from 'react-native-paper';
import DropDownList from '../../components/DropDownList';
import InsuranceAPI from '../../api/InsuranceAPI';
import DatePicker from '../../components/DatePicker';
import I18n from '../../i18n';
import { Button } from 'react-native-paper';
import InsuranceUtils from '../../models/InsuranceUtils';
import CustomListPicker from '../../components/CustomListPicker';
import { format } from 'date-fns';

export default NewInsuranceChoiceScreen = ({insurance }) => {

    const navigation = useNavigation(); 
    const [state, setState] = useState({

        subscriptionDate: format(new Date, 'yyyy-MM-dd'),
        endOfSubscriptionDate: format(new Date, 'yyyy-MM-dd'),
        country: null,
        travelCoverType: insurance.travelCoverType,
        rightHoldersEmails: []
    })




    useEffect(() => {

    }, [])


    const styles = StyleSheet.create({
        container: {
            padding: 20,
        },
        headers3: {
            fontSize: 30
        }
    });
    onStartDateConfirmed = (date) => {
        setState({ ...state, subscriptionDate: format(date, 'yyyy-MM-dd') })
    }
    onEndDateConfirmed = (date) => {
        setState({ ...state, endOfSubscriptionDate: format(date, 'yyyy-MM-dd') })
    }

    onCountrySelected = (country) => {
        setState({ ...state, country: country })
    }

    confirmContract = () => {
        InsuranceAPI.addNewTravelContract(state).then(() => { navigation.navigate('HomeScreen') }).catch((error) => { console.log(error) });
    }

    return (
        <View style={styles.container}>
            <Text style={{ fontSize: 40 }}>{InsuranceUtils.getNameFromInsuranceType(insurance.type)}</Text>
            <Text style={{ fontSize: 30, color: 'rgb(120,120,120)' }}>{InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(insurance.type, insurance.travelCoverType)}</Text>
            <View style={{ paddingTop: 20, paddingBottom: 20 }}>
                <CustomListPicker label={I18n.t('rightHolder')} onListChanged={(values) => { setState({ ...state, rightHoldersEmails: values }) }} />
            </View>
            <CustomDatePicker label={I18n.t('startDate')} onDateSelected={onStartDateConfirmed}></CustomDatePicker>
            <CustomDatePicker label={I18n.t('endDate')} onDateSelected={onEndDateConfirmed} ></CustomDatePicker>
            <DropDownList label={I18n.t('wantedCountry')} onItemSelected={onCountrySelected} items={InsuranceAPI.getAllCountry()}></DropDownList>
            <Button style={{ marginTop: 20 }} mode="contained" onPress={confirmContract} >{I18n.t('choose')} </Button>
        </View>
    )
}



