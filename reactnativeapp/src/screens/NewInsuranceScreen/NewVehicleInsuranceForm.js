import { StyleSheet, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomAppbar from '../../components/CustomAppbar';
import { Text } from 'react-native-paper';
import DropDownList from '../../components/DropDownList';
import InsuranceAPI from '../../api/InsuranceAPI';
import DatePicker from '../../components/DatePicker';
import I18n from '../../i18n';
import { Button } from 'react-native-paper';
import InsuranceUtils from '../../models/InsuranceUtils';
import CustomListPicker from '../../components/CustomListPicker';
import { format } from 'date-fns';
import { ScrollView, FlatList } from 'react-native';
import VehicleAPI from '../../api/VehicleAPI';
import { useNavigation } from '@react-navigation/native';

import { LogBox } from 'react-native';
LogBox.ignoreLogs(['VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.']);


export default NewVehicleInsuranceForm = ({ insurance }) => {

    const navigation = useNavigation();

    const [state, setState] = useState({
        allVehicles: [],
        subscriptionDate: format(new Date, 'yyyy-MM-dd'),
        vehicleCoverType: insurance.vehicleCoverType,
        rightHoldersEmails: [],
        brand: "",
        filteredVehicle: [],
        startYear: null,
        endYear: null,
        powerDIN: null,
        vehicleEnergy: null,
        productionDate: format(new Date, 'yyyy-MM-dd'),
        registrationNumber: "",
        registrationDate: format(new Date, 'yyyy-MM-dd'),
        drivingLicenceDate: format(new Date, 'yyyy-MM-dd'),
        nbResponsibleSinister:0,
        nbNonResponsibleSinister:0,
        nbInsuredMonth:36,
        bonusMalus:0.7,
        selectedVehicle:null
    })

    const styles = StyleSheet.create({
        container: {
            color: "red",
            padding: 20,
        },
        headers3: {
            fontSize: 30
        }
    });

    useEffect(() => {
        VehicleAPI.getAllVehicle().then((value) => {
            setState({ ...state, allVehicles: value.data })
        })
    }, [])

    onBrandSelected = (selectedBrand) => {
        if (selectedBrand != null) {
            setState({ ...state, brand: selectedBrand, filteredVehicle: state.allVehicles.filter((vehicle) => vehicle.brand == selectedBrand) }, () => { console.log(state.filteredVehicle) })
        }
    }

    onModelSelected = (selectedModel) => {
        if (selectedModel != null) {
            setState({ ...state, model: selectedModel[0], startYear: selectedModel[1], endYear: selectedModel[2], filteredVehicle: state.filteredVehicle.filter((vehicle) => vehicle.end_year == selectedModel[2] && vehicle.model === selectedModel[0] && vehicle.start_year == selectedModel[1]) })

        }
    }

    onMotorSelected = (selectedMotor) => {
        if (selectedMotor != null) {
            setState({ ...state,
                 powerDIN: selectedMotor[0] ,
                 vehicleEnergy: selectedMotor[1],
                 filteredVehicle: state.filteredVehicle.filter((vehicle) => vehicle.vehicle_energy == selectedMotor[1] && vehicle.powerDIN === selectedMotor[0]) ,
                 selectedVehicle: state.filteredVehicle.filter((vehicle) => vehicle.vehicle_energy == selectedMotor[1] && vehicle.powerDIN === selectedMotor[0])[0]
                 })

        }
    }

    onStartDateConfirmed = (date) => {
        setState({ ...state, subscriptionDate: format(date, 'yyyy-MM-dd') })
    }

    onProductionDateConfirmed = (date) => {
        setState({ ...state, productionDate: format(date, 'yyyy-MM-dd') })
    }

    onImatConfirmed = (date) => {
        setState({ ...state, registrationDate: format(date, 'yyyy-MM-dd') })
    }
    confirmContract = () => {
        InsuranceAPI.addNewVehicleContract(state).then(() => { navigation.navigate('HomeScreen') }).catch((error) => { console.log(error) });
    }


    return (
        state.allVehicles != [] ?
            <FlatList
                data={[1]}
                renderItem={(item) => {
                    return (
                        <><Text></Text></>
                    )
                }}
                ListHeaderComponent={
                    <View style={styles.container}>
                        <Text style={{ fontSize: 40 }}>{InsuranceUtils.getNameFromInsuranceType(insurance.type)}</Text>
                        <Text style={{ fontSize: 30, color: 'rgb(120,120,120)' }}>{InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(insurance.type, insurance.vehicleCoverType)}</Text>
                        <View style={{ gap: 20, display: 'flex', flexDirection: 'column' }}>
                        <Text style={{fontSize:25,marginTop:30}}>Vehicule</Text>

                            <DropDownList key={state.allVehicles.length} zindex={1000} dropDownContainerStyle={{ backgroundColor: 'white', zIndex: 1000, elevation: 1000 }} label={"Marque"} onItemSelected={onBrandSelected} items={state.allVehicles.map((vehicle) => {
                                return { label: vehicle.brand, value: vehicle.brand }
                            }).filter((v, i, a) => a.findIndex(t => (JSON.stringify(t) === JSON.stringify(v))) === i)}></DropDownList>


                            <DropDownList zindex={100} key={state.brand} label={"Modèle"} onItemSelected={onModelSelected} items={
                                state.filteredVehicle.map((vehicle2) => ({ label: `${vehicle2.model} (${vehicle2.start_year}-${vehicle2.end_year})`, value: [vehicle2.model, vehicle2.start_year, vehicle2.end_year] })).filter((v, i, a) => a.findIndex(t => (JSON.stringify(t) === JSON.stringify(v))) === i)
                            }></DropDownList>

                            <DropDownList zindex={10} key={state.brand + "-" + state.model} label={"Motorisation"} onItemSelected={onMotorSelected} items={
                                state.filteredVehicle.map((vehicle2) => ({ label: `${vehicle2.powerDIN} ch - ${vehicle2.vehicle_energy}`, value: [vehicle2.powerDIN, vehicle2.vehicle_energy] })).filter((v, i, a) => a.findIndex(t => (JSON.stringify(t) === JSON.stringify(v))) === i)
                            }></DropDownList>



                            <CustomTextInput label={"Immatriculation"} isConfidential={false} onChange={(value) => { setState({ ...state, registrationNumber: value }) }} />
                            <CustomDatePicker label={"Date de production"} onDateSelected={onProductionDateConfirmed}></CustomDatePicker>
                            <CustomDatePicker label={"Date d'immatriculation"} onDateSelected={onImatConfirmed}></CustomDatePicker>
                            <CustomDatePicker label={I18n.t('startDate')} onDateSelected={onStartDateConfirmed}></CustomDatePicker>
                            <Text style={{fontSize:25,marginTop:30}}>Historique</Text>
                            <CustomDatePicker label={"Date d'obtention du permis"} onDateSelected={(date)=>setState({ ...state, drivingLicenceDate: format(date, 'yyyy-MM-dd') })}></CustomDatePicker>
                            <CustomTextInput label={"Nombre de sinistre responsable"} isConfidential={false} onChange={(value) => { setState({ ...state, nbResponsibleSinister: value }) }} />
                            <CustomTextInput label={"Nombre de sinistre non responsable"} isConfidential={false} onChange={(value) => { setState({ ...state, nbNonResponsibleSinister: value }) }} />
                            <CustomTextInput label={"Nombre de mois assuré"} isConfidential={false} onChange={(value) => { setState({ ...state, nbInsuredMonth: value }) }} />
                            <CustomTextInput label={"Bonus malus"} isConfidential={false} onChange={(value) => { setState({ ...state, bonusMalus: value }) }} />

                            <CustomListPicker label={I18n.t('rightHolder')} onListChanged={(values) => { setState({ ...state, rightHoldersEmails: values }) }} />
                            <Button style={{ marginTop: 20 }} mode="contained" onPress={confirmContract} >{I18n.t('choose')} </Button>
                        </View>
                    </View>
                    // Header Content to should list here
                }

                ListFooterComponent={
                    <Text></Text>
                } /> : <>Chargement</>





    )
}



