import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import React, { useState, useEffect } from 'react'
import { ScrollView } from 'react-native';
import ConsumerAPI from '../api/ConsumerAPI';
import InsurancePreview from '../components/InsurancePreview';
import CustomAppbar from '../components/CustomAppbar';
import i18n from '../i18n';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Divider } from 'react-native-paper';
import { TouchableHighlight } from 'react-native';

export default HomeScreen = ({ navigation }) => {

    const [state, setState] = useState({
        contractList: [],
        messageList: [{
            titre: "Votre confirmation assurance",
            contenu: "When the top app bar scrolls, its elevation above other elements becomes apparent. The top app bar disappears upon scrolling up, and appears upon scrolling down."
        }, {
            titre: "Votre confirmation assurance",
            contenu: "When the top app bar scrolls, its elevation above other elements becomes apparent. The top app bar disappears upon scrolling up, and appears upon scrolling down."
        },
        {
            titre: "Votre confirmation assurance",
            contenu: "When the top app bar scrolls, its elevation above other elements becomes apparent. The top app bar disappears upon scrolling up, and appears upon scrolling down."
        },


        ]
    })

    useEffect(() => {
        ConsumerAPI.getContractFromConsumer().then((value) => {
            setState({ ...state, contractList: value.data })
        }).catch((error) => {
            console.log(value.error);
        });

    }, [])



    const styles = StyleSheet.create({
        container: {
            paddingLeft: 20,
            paddingTop: 20
        },
        headers3: {
            fontSize: 30
        }
    });

    navigateToDetails=(contract)=>{
        console.log(contract);
        if(contract.type==="TRAVEL"){
            navigation.navigate('TravelContractDetails', { insurance: contract })
        }
        else if (contract.type==="TRANSPORT" ){
            navigation.navigate('TransportContractDetails', { insurance: contract })

        }

    }

    return (
        <SafeAreaView>
            <View>
                <CustomAppbar navigation={navigation}></CustomAppbar>
                <View style={{ marginLeft: 20, marginTop: 20 }}>
                    <Text style={styles.headers3}>{i18n.t('myContracts')}</Text>
                    <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 40 }}>
                        <ScrollView
                            horizontal={true}>
                            {
                                state.contractList.map((value) => {
                                    return (<>
                                        <TouchableOpacity onPress={() => navigateToDetails(value)}>

                                            <InsurancePreview key={value.id} contract={value}></InsurancePreview>

                                        </TouchableOpacity>
                                    </>)
                                })
                            }
                        </ScrollView>


                    </View>
                </View>
                <View style={{ padding: 15 }}>
                    <Text style={styles.headers3}>{i18n.t('lastDocument')}</Text>
                    <ScrollView>

                        {
                            state.messageList.map((value) => {
                                return (
                                    <View style={{ paddingTop: 30 }}>
                                        <Text style={{ color: 'rgb(0,0,255)', fontWeight: 'bold', fontSize: 15 }}>8 weeks ago</Text>
                                        <Text style={{ paddingTop: 10, fontWeight: 'bold', fontSize: 20 }}>{value.titre}</Text>
                                        <Text style={{ paddingTop: 15 }}>{value.contenu}</Text>
                                    </View>)
                            })
                        }
                    </ScrollView>
                </View>

            </View>

        </SafeAreaView>

    )
}
