import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text } from 'react-native';
import { RetrieveToken } from '../utils/token';
import axios from 'axios';
import { getSpringUrl } from 'reactapp/src/urls';
import { ActivityIndicator, Colors } from 'react-native-paper';
import ConsumerAPI from '../api/ConsumerAPI';

export default SplashScreen = ({ navigation }) => {
    const [state, setState] = useState({ isLoading: true });

    useEffect(() => {
        ConsumerAPI.verifyToken().then((value) => {
            navigation.navigate('HomeScreen')
        }).catch((error) => {
            navigation.navigate('LoginScreen')
        });

    }, [])
    
    if (state.isLoading) {
        return (
            <View style={styles.container}>
                <ActivityIndicator animating={true} color={Colors.red800} size={'large'} />
            </View>
        )
    }
    else {
        return (
            <View>
                <Text>Splash screen</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
});
