import { StyleSheet, View, Text, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomAppbar from '../../components/CustomAppbar';
import InsuranceUtils from '../../models/InsuranceUtils';
import { Table, Row, Rows } from 'react-native-table-component';
import I18n from '../../i18n';

export default TravelContractDetails = ({ navigation, route }) => {



    const [state, setState] = useState({

        subscriberTab: [],

    })




    useEffect(() => {
        setState({ ...state, subscriberTab: [] })

        route.params.insurance.rightHolders.map((value) => {
            setState({ ...state, subscriberTab: [...state.subscriberTab, [value.firstname, value.lastname, value.phoneNumber]] })
        });
    }, [])



    const styles = StyleSheet.create({
        container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
        head: { height: 40, backgroundColor: '#f1f8ff' },
        textTable: { margin: 6 },
        text: {
            fontSize: 20,
        }
    });

    return (

        <>
            <SafeAreaView>
                <CustomAppbar navigation={navigation}></CustomAppbar>
                <View style={{ flexDirection: "column", alignItems: "center", justifyContent: 'center', marginTop: 40 }}>
                    <Text style={{ fontSize: 40, fontWeight: 'bold' }}>{InsuranceUtils.getNameFromInsuranceType(route.params.insurance.type)}</Text>
                    <Text style={{ fontSize: 19, color: 'rgb(115,115,115)' }}>{InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(route.params.insurance.type, route.params.insurance.travelInsurance.travelCoverType)}</Text>
                    <Text style={{ fontSize: 19, color: 'rgb(115,115,115)', fontStyle: 'italic', paddingTop: 10 }}>{route.params.insurance.country}</Text>
                    <View style={{ ...styles.text, alignItems: "center", paddingTop: 30, marginTop: 40 }}>
                        <Text style={styles.text}>{I18n.t('date')} : {route.params.insurance.subscriptionDate.substring(0, 10) + " au " + route.params.insurance.endOfSubscriptionDate.substring(0, 10)}</Text>
                        <Text style={styles.text}>{I18n.t('status')} : {InsuranceUtils.getStepOfContract(route.params.insurance.status)}</Text>
                        <Text style={styles.text}>{I18n.t('price')} : {parseFloat(route.params.insurance.dailyPrice).toFixed( 2 ) } € </Text>
                    </View>


                    <View style={{ width: 380, marginTop: 70 }}>
                        <Text style={{marginBottom:10,fontSize:20,fontWeight: 'bold'}}>{I18n.t('rightHolder')}</Text>
                        <Table borderStyle={{ borderWidth: 2, borderColor: '#c8e1ff', marginTop: 10 }}>
                            <Row data={['Nom', 'Prénom', 'Numéro de téléphone']} style={styles.head} textStyle={styles.textTable} />
                            <Rows data={state.subscriberTab} textStyle={styles.textTable} />
                        </Table>
                    </View>


                </View>

            </SafeAreaView>
        </>
    )
}

