import { StyleSheet, View, Text, ScrollView } from 'react-native';
import ConsumerAPI from '../api/ConsumerAPI';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import InsurancePreview from '../components/InsurancePreview';
import NewsPreview from '../components/NewsPreview';
import { Appbar, Button } from 'react-native-paper';
import CustomAppbar from '../components/CustomAppbar';
import InsuranceUtils from '../models/InsuranceUtils';
import I18n from '../i18n';

export default MyInsuranceScreen = ({ navigation }) => {

    const [state, setState] = useState({
        contractList: [],

    })

    useEffect(() => {
        ConsumerAPI.getContractFromConsumer().then((value) => {
            setState({ ...state, contractList: value.data })
        }).catch((error) => {
            console.log(value.error);
        });

    }, [])


    const styles = StyleSheet.create({
        container: {
            paddingLeft: 20,
            paddingTop: 20
        },
        headers3: {
            fontSize: 30
        }
    });

    return (

        <>
            <SafeAreaView>
                <CustomAppbar navigation={navigation}></CustomAppbar>

                <Text style={styles.headers3}>Mes contrats</Text>
                <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 40 }}>
                    <ScrollView>
                        {
                            state.contractList.map((value) => {
                                return (<>
                                    <View>
                                        {

                                            NewInsurancePreviewFactory(value, navigation)

                                        }
                                    </View>
                                </>)
                            })
                        }
                    </ScrollView>
                </View>
            </SafeAreaView>
        </>
    )
}

NewInsurancePreviewFactory = (insurance, navigation) => {
    switch (insurance.type) {
        case "TRAVEL":
            return <TravelContractPreview insurance={insurance} navigation={navigation}></TravelContractPreview>
        case "TRANSPORT":
            return <TransportContractPreview insurance={insurance} navigation={navigation}></TransportContractPreview>
        default:
            return <Text>Assurance non connu</Text>
    }
}

const TravelContractPreview = ({ insurance, navigation }) => {
    return (<View style={{ flexDirection: "row", borderRadius: 10, backgroundColor: 'rgb(255,255,255)', height: 120, borderWidth: 0.5, padding: 10, marginBottom: 30, marginLeft: 20, marginRight: 20, justifyContent: 'space-around' }}>
        <View style={{ flexDirection: "column", justifyContent: "space-around" }}>
            <Text style={{ fontSize: 25, fontWeight: '' }}>{InsuranceUtils.getNameFromInsuranceType(insurance.type)}</Text>
            <Text style={{ fontSize: 19, color: 'rgb(115,115,115)' }}>{InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(insurance.type, insurance.travelInsurance.travelCoverType)}</Text>
            <Text style={{ fontSize: 19, color: 'rgb(115,115,115)', fontStyle: 'italic' }}>{insurance.country}</Text>


            <Text>{insurance.subscriptionDate.substring(0, 10)}</Text>
            <Text>{" au " + insurance.endOfSubscriptionDate.substring(0, 10)}</Text>
        </View>
        <View style={{ flexDirection: "column", justifyContent: "space-around" }}>
            <Button mode="contained" onPress={() => navigation.navigate('TravelContractDetails', { insurance: insurance })}>
                {I18n.t('details')}
            </Button>
        </View>
    </View>);
}

const TransportContractPreview = ({ insurance, navigation }) => {

    return (<View style={{ flexDirection: "row", borderRadius: 10, backgroundColor: 'rgb(255,255,255)', height: 120, borderWidth: 0.5, padding: 10, marginBottom: 30, marginLeft: 20, marginRight: 20, justifyContent: 'space-around' }}>
        <View style={{ flexDirection: "column", justifyContent: "space-around" }}>
            <Text style={{ fontSize: 25, fontWeight: '' }}>{InsuranceUtils.getNameFromInsuranceType(insurance.type)}</Text>
                <Text style={{ fontSize: 19, color: 'rgb(115,115,115)' }}>{InsuranceUtils.getSubtypeLabelFromInsuranceSubtype(insurance.type, insurance.transport.type)}</Text>
                <Text>{insurance.subscriptionDate.substring(0, 10)}</Text>
                <Text>{" au " + insurance.endOfSubscriptionDate.substring(0, 10)}</Text>
            </View>
        <View style={{ flexDirection: "column", justifyContent: "space-around" }}>
            <Button mode="contained" onPress={() => navigation.navigate('TransportContractDetails', { insurance: insurance })}>
                {I18n.t('details')}
            </Button>
        </View>
    </View>);
}