import I18n from 'i18n-js';


export class InsuranceUtils {

    static  getNameFromInsuranceType(type) {

        return I18n.t(['contractType',type,'trad'])

    }

    static  getSubtypeLabelFromInsuranceSubtype(type,subtype) {

        return I18n.t(['contractType',type,"subtype",subtype,'label'])

    }

    static  getStepOfContract(step) {

        return I18n.t(['contractStep',step,"label"])

    }
}

export default InsuranceUtils;

