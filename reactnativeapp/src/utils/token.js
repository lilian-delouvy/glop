import EncryptedStorage from 'react-native-encrypted-storage';
export const StoreToken=async (jwtToken)=>{
    try {
        console.log(jwtToken)
        await EncryptedStorage.setItem(
            "jwtToken",
            JSON.stringify({
                token : jwtToken,

            })
        );

        console.log("token stored")
    } catch (error) {
        console.log(error)
        console.log("token store error")
    }
}

export const RetrieveToken= async ()=>{
    try {
        const session = await EncryptedStorage.getItem("jwtToken");
        console.log(JSON.parse(session).token);
        return JSON.parse(session).token;
    } catch (error) {
        console.log("token retrieve error")
    }
}

export const DeleteToken= async ()=>{
    try {
        const session = await EncryptedStorage.removeItem("jwtToken");
        
        return JSON.parse(session).token;
    } catch (error) {
        console.log("token delete error")
    }
}

export default {StoreToken,RetrieveToken};