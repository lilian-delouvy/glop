export default {
    lastDocument: "Mes derniers documents",
    myContracts: "Mes contracts",
    details: 'Détails',
    choose: 'Choisir',
    contractType: {
        TRAVEL: {
            trad: "Voyage  ✈️",
            subtype: {
                SHORT: { label: "Courte durée", },
                LONG: { label: "Longue durée", },
                PREMIUM: { label: "Durée premium", }
            }
        },
        TRANSPORT: {
            trad: "Transport",
            subtype: {
                NEW_MOBILITY:{label:"Nouvelle mobilité 🛴"},
                THEFT: { label: "Vol", },
                INTEGRAL: { label: "Tout risque", },
                THIRD_PARTY: { label: "Tiers", },
                THIRD_PARTY_THEFT: { label: "Tiers vol", },
            }
        },
        VEHICLE: {
            trad: "Vehicule 🚙",
            subtype: {
                THEFT: { label: "Vol", },
                INTEGRAL: { label: "Tout risque", },
                THIRD_PARTY: { label: "Tiers", },
                THIRD_PARTY_THEFT: { label: "Tiers vol", },
            }
        },
        NEW_MOBILITY: {
            trad: "Nouvelle mobilité 🛴",
            subtype: {
                THEFT: { label: "Vol", },
                INTEGRAL: { label: "Tout risque", },
                THIRD_PARTY: { label: "Tiers", },
                THIRD_PARTY_THEFT: { label: "Tiers vol", },
            }
        }
    },
    contractStep: {
        PENDING: {
            label: "En attente de validation 🕦"
        },
        VALID: {
            label: "Validé ✅"
        },
        ENDED: {
            label: "Terminé ❗️"
        }
    },
    startDate: "Date de début d'assurance",
    endDate: "Date de fin  d'assurance",
    wantedCountry: "Pays choisis",
    rightHolder: "Ayant droit",
    date: "Date",
    status: "État",
    price: "Prix",
    wantedVehicleBrand:'Marque',
    wantedVehicleModel:'Modéle',
    powerDIN:'Puissance DIN',
    emailInput:'Email',
    passwordInput:'Mot de passe',
    loginBtn:'Connexion'
};