import { RetrieveToken } from "../utils/token";
import axios from "axios";
import { getSpringUrl } from 'reactapp/src/urls';


export class ConsumerAPI {

    static async verifyToken() {

        var token = await RetrieveToken()
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        }

        axios.get(getSpringUrl() + "/customer/contracts", config).then((value) => {
            return true;
        }).catch((error) => {
            return false;
        })

    }


    static async getContractFromConsumer() {

        var token = await RetrieveToken()
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        }

        return axios.get(getSpringUrl() + "/customer/contracts", config)

    }
}

export default ConsumerAPI;