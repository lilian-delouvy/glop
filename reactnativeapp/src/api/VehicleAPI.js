import { RetrieveToken } from "../utils/token";
import axios from "axios";
import { getSpringUrl } from 'reactapp/src/urls';

export class VehicleAPI {


    static async getAllVehicle() {

        var token = await RetrieveToken()
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        }

        return axios.get(getSpringUrl() + "/common/vehicle_model/all", config)

    }




}

export default VehicleAPI;