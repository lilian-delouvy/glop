import { RetrieveToken } from "../utils/token";
import axios from "axios";
import { getSpringUrl } from 'reactapp/src/urls';


export class InsuranceAPI {

    static async getAllInsuranceAvailable() {

        var token = await RetrieveToken()
        var config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + token
            }
        }
        return axios.get(getSpringUrl() + "/common/insurances/all", config);
    }

    static getAllCountry() {

        return [
            { label: 'France', value: 'FRANCE' },
            { label: 'Belgium', value: 'BELGIUM' },
            { label: 'Suisse', value: 'SWITZERLAND' },

        ];
    }


    static async addNewTravelContract(information) {
        var token = await RetrieveToken()
        var body = {
            ...information,
            type: "contractTravelRequest",
            option: []
        }

        console.log(body)


        return axios({
            method: 'POST',
            url: getSpringUrl() + "/customer/contract/new",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            data: body
        })

    }

    static async addNewVehicleContract(information) {
        var token = await RetrieveToken()
        var body = {
            type:"contractVehicleRequest",
            option:[],
            rightHoldersEmails:[],
            registrationDate:information.registrationDate,
            subscriptionDate:information.subscriptionDate,
            registrationNumber:information.registrationNumber,
            vehicleModelId:information.selectedVehicle.id,
            year:information.productionDate.substring(0,4),
            vehicleCoverType:information.vehicleCoverType,
            vehicleHistoric: {
                drivingLicenceDate:information.drivingLicenceDate,
                nbResponsibleSinister:information.nbResponsibleSinister,
                nbNonResponsibleSinister:information.nbNonResponsibleSinister,
                nbInsuredMonth:information.nbInsuredMonth,
                bonusMalus:information.bonusMalus
            }            
        }

        console.log(body);

        return axios({
            method: 'POST',
            url: getSpringUrl() + "/customer/contract/new",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },
            data: body
        })

    }
}

export default InsuranceAPI;