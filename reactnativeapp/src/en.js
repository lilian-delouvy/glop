export default {
    lastDocument: "My last documents",
    myContracts: "My contract",
    details: 'Details',
    choose: 'Choose',
    contractType:{
        TRAVEL:{
            imageUrl:"https://hubinstitute.com/sites/default/files/styles/1200x500_crop/public/2020-06/travel-voyage-post-covid%20%281%29.jpg?itok=BkvHH4ZV",
            trad:"Travel  ✈️",
            subtype:{
                SHORT:{label:"Short duration",},
                LONG:{label:"Long duration",},
                PREMIUM:{label:"Premium duration",}
            }
        },
        TRANSPORT: {
            imageUrl:"https://images.caradisiac.com/images/7/7/5/2/187752/S0-plus-belle-voiture-de-l-annee-2021-et-la-gagnante-est-659940.jpg",
            trad: "Transport 🚙",
            subtype: {
                VEHICLE:{label:"Véhicule 🚙"},
                NEW_MOBILITY:{label:"Nouvelle mobilité 🛴"},
                THEFT: { label: "Theft", },
                INTEGRAL: { label: "Integral", },
                THIRD_PARTY: { label: "Third party", },
                THIRD_PARTY_THEFT: { label: "Third party theft", },
            }
        },
        VEHICLE: {
            imageUrl:"https://images.caradisiac.com/images/7/7/5/2/187752/S0-plus-belle-voiture-de-l-annee-2021-et-la-gagnante-est-659940.jpg",
            trad: "Vehicule 🚙",
            subtype: {
                THEFT: { label: "Theft", },
                INTEGRAL: { label: "Integral", },
                THIRD_PARTY: { label: "Third party", },
                THIRD_PARTY_THEFT: { label: "Third party theft", },
            }
        },
        NEW_MOBILITY: {
            imageUrl:"https://images.caradisiac.com/images/7/7/5/2/187752/S0-plus-belle-voiture-de-l-annee-2021-et-la-gagnante-est-659940.jpg",
            trad: "Nouvelle mobilité 🛴",
            subtype: {
                THEFT: { label: "Theft", },
                INTEGRAL: { label: "Integral", },
                THIRD_PARTY: { label: "Third party", },
                THIRD_PARTY_THEFT: { label: "Third party theft", },
            }
        }
    },
    contractStep:{
        PENDING:{
            label:"Waiting for approval 🕦"
        },
        VALID:{
            label:"Validated ✅"
        },
        ENDED:{
            label:"Ended ❗️"
        }
    },
    startDate:"Insurance starting date",
    endDate:"Insurance ending date",
    wantedCountry:"Choosen date",
    rightHolder:"Right holders",
    date:"Date",
    status:"Status",
    price:"Price",
    wantedVehicleBrand:'Marque',
    wantedVehicleModel:'Model',
    powerDIN:'Power DIN',
    emailInput:'Email',
    passwordInput:'Password',
    loginBtn:'Login'



};