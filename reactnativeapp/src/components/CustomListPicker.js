
import React, { useState, useEffect } from 'react';
import { Button, TextInput } from 'react-native-paper';
import CustomTextInput from './CustomTextInput';
import I18n from '../i18n';
import { Text } from 'react-native-paper';
export default CustomListPicker = ({ label,onListChanged}) => {


    const [state, setState] = useState({
        value:"",
        listValue:[]
    })

    addNewValue=()=>{
        setState({...state,value:"",listValue:[...state.listValue,state.value]})
    }

    useEffect( ()=>{
        onListChanged(state.listValue);
    },[state.listValue])


    return (
        <>

            <CustomTextInput label={label} isConfidential={false} onChange={(value) => { setState({ ...state, value: value }) }}/> 
            <Button mode="contained" onPress={()=>{
                addNewValue();
            }} >Ajouter nouveaux ayants droits</Button>

 
            {
                state.listValue.map((value)=>{
                    return <Text >{value}</Text>
                })
            }

        </>
            );


}