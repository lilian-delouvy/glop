import React, { useState, useEffect } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { Text } from 'react-native-paper';

import {StyleSheet,View} from "react-native" ;
export default DropDownList = ({ label, items, onItemSelected,zindex=1 }) => {

    const [item, setItem] = useState((null));

    const [state, setState] = useState({
        open: false,
        items: items,
        value: null,
    })

    useEffect(() => {
        onItemSelected(item);
    }, [item])

    return (
    <View style={Platform.OS === 'ios' ? {zIndex: zindex} : {}}>
        <Text style={{ paddingBottom: 10, paddingTop: 10, fontWeight: 'bold' }}>{label}</Text>
        <DropDownPicker
            open={state.open}
            value={item}
            items={state.items}
            setOpen={() => { setState({ ...state, open: !state.open }); }}
            setValue={(value) => { setItem(value); }} />
    </View>);
}