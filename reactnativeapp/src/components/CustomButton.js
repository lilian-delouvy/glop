import React from 'react'
import { Button } from 'react-native-paper';
export default CustomButton = ({mode,icon,onPress,label}) =>{
    return (
    <Button icon={icon} mode={mode} onPress={onPress}>
    {label}
  </Button>
  );
}