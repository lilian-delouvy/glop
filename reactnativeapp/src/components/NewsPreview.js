import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default NewsPreview = ({ title, content }) => {


    const styles = StyleSheet.create({
        container: {


        },
    });

    return (<View style={styles.container}>
        <Text style={{ fontSize: 15, color: 'blue', fontWeight: 'bold' }}>8 weeks ago</Text>
        <Text style={{ fontSize: 20, color: 'black', fontWeight: 'bold' }}>{title}</Text>
        <Text style={{ fontSize: 15, color: 'black' }}>{content.substring(0,200)}</Text>
    </View>);
}