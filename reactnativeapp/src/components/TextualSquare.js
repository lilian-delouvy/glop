import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default TextualSquare = ({ text }) => {


    const styles = StyleSheet.create({
        container: {
            backgroundColor: '#0E387A',
            height: 135,
            width: 135,
            fontSize: 40,
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            borderRadius: 20,
            marginRight:10

        },
    });

    return (<View style={styles.container}>
        <Text style={{ fontSize: 17, color: 'white', fontWeight: 'bold' }}>{text}</Text>
    </View>);
}