import React, { useState, useEffect } from 'react';
import { Appbar, Drawer } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import { StackActions } from '@react-navigation/routers';
import { DeleteToken } from '../utils/token';
export default CustomAppBar = ({ navigation }) => {

  const [state, setState] = useState({ open: false });


  const styles = StyleSheet.create({

  });

  return (
    <Appbar >
             <Appbar.Content title="Mobisure"  />
      <Appbar.Action
        icon="arrow-left"
        onPress={() => {
          const popAction = StackActions.pop(1);
          navigation.dispatch(popAction);
        }
        }
      />
       <Appbar.Action
        icon="home"
        onPress={() => navigation.navigate('HomeScreen')
        }
      />
      <Appbar.Action
        icon="plus"
        onPress={() => navigation.navigate('NewInsuranceScreen')
        }
      />
      <Appbar.Action
        icon="format-list-bulleted"
        onPress={() => navigation.navigate('MyInsuranceScreen')
        }
      />
    </Appbar>);

}
