import React, { useState } from 'react'
import { Button } from 'react-native'
import DatePicker from 'react-native-date-picker'
import { Text, View } from 'react-native-paper'
export default CustomDatePicker = ({ label,onDateSelected }) => {
    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)

    return (
        <>
            <Text style={{paddingTop: 10, paddingBottom: 10 ,fontWeight:'bold'}}>{label}</Text>
            <Text onPress={() => setOpen(true)} style={{
                width: '100%', textAlign: 'center', backgroundColor: 'white'
                , paddingTop: 10, paddingBottom: 10, fontSize: 16, borderWidth: 1
            }}>{date.toLocaleDateString("en-US")}</Text>

            <DatePicker
                mode="date"
                minimumDate={new Date()}
                modal
                open={open}
                date={date}
                onConfirm={(date) => {
                    setOpen(false)
                    setDate(date)
                    onDateSelected(date)
                }}
                onCancel={() => {
                    setOpen(false)
                }}
            />
        </>
    )
}