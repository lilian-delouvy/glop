import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import TextualSquare from './TextualSquare';
import InsuranceUtils from '../models/InsuranceUtils';
export default  InsurancePreview = ({contract})=>{


    const styles = StyleSheet.create({
        container: {
            marginRight:20
        },
        headers3: {
            fontSize: 30
        }
    });

    if(contract.type==="TRAVEL")
    return(<TextualSquare  text={InsuranceUtils.getNameFromInsuranceType(contract.type) +" - "+contract.country}></TextualSquare>)
    else if (contract.type==="TRANSPORT")
    return(<TextualSquare  text={InsuranceUtils.getNameFromInsuranceType(contract.type) +" - "+contract.transport.model}></TextualSquare>)
    else 
         return(<TextualSquare text={contract.type}></TextualSquare>)

}