import React from 'react'
import { TextInput } from 'react-native-paper';
export default CustomTextInput = ({label,value,onChange,isConfidential}) =>{
    const [text, setText] = React.useState('');
    const onChangeText = (text)=>{
        setText(text);
        onChange(text);
    }

    return (
        <TextInput
          label={label}
          value={text}
          onChangeText={text => onChangeText(text)}
          autoCapitalize='none'
          secureTextEntry={isConfidential} 
        />
      );
}