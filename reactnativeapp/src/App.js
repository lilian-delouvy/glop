import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import LoginScreen from './screens/LoginScreen';
import { NavigationContainer } from '@react-navigation/native';import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen';
import SplashScreen from './screens/SplashScreen';
import MyInsuranceScreen from './screens/MyInsuranceScreen';
import NewInsuranceChoiceScreen from './screens/NewInsuranceScreen/NewInsuranceChoiceScreen';
import NewInsuranceScreen from './screens/NewInsuranceScreen/NewInsuranceScreen';
import TravelContractDetails from './screens/InsuranceDetailScreen/TravelContractDetails';
import TransportContractDetails from './screens/InsuranceDetailScreen/TransportContractDetails';
import { DefaultTheme,Provider as PaperProvider } from 'react-native-paper';
import { LogBox } from 'react-native';
import NewContractValidated from './screens/NewInsuranceScreen/NewContractValidated';

LogBox.ignoreLogs(['Warning: ...']);
const Stack = createNativeStackNavigator();

export default function App() {
    return (
        <PaperProvider
        
        theme={{ ...DefaultTheme,colors:{  ...DefaultTheme.colors,primary:'#0E387A',  accent:'#FFFFFF'}}}
        >

        <NavigationContainer>
            <Stack.Navigator initialRouteName="SplashScreen">
                <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }}  />
                <Stack.Screen name="HomeScreen" component={HomeScreen} options={{ headerShown: false }} />
                <Stack.Screen name="SplashScreen" component={SplashScreen}  options={{ headerShown: false }}/>
                <Stack.Screen name="NewInsuranceScreen" component={NewInsuranceScreen}  options={{ headerShown: false }}/>
                <Stack.Screen name="MyInsuranceScreen" component={MyInsuranceScreen} options={{ headerShown: false }} />
                <Stack.Screen name="NewInsuranceChoiceScreen" component={NewInsuranceChoiceScreen} options={{ headerShown: false }} />
                <Stack.Screen name="TravelContractDetails" component={TravelContractDetails} options={{ headerShown: false }} />
                <Stack.Screen name="TransportContractDetails" component={TransportContractDetails} options={{ headerShown: false }} />
                <Stack.Screen name="NewContractValidated" component={NewContractValidated} options={{ headerShown: false }} />

            </Stack.Navigator>
        </NavigationContainer>
    </PaperProvider>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
