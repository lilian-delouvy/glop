/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../ClientApp/App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const container = renderer.create(<App />).toJSON();
  expect(container).toMatchSnapshot();
});
