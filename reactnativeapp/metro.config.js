/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
 const path = require('path');const extraNodeModules = {
  'reactapp': path.resolve(__dirname + '/../reactapp'),
};
const watchFolders = [
  path.resolve(__dirname + '/../reactapp')
];module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  }, 
  resolver: {
    extraNodeModules
  },
  watchFolders,
};
