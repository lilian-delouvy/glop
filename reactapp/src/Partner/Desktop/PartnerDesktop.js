import {Avatar} from "@mui/material";
import Button from "@mui/material/Button";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import PartnerHomeDesktop from "./PartnerHomeDesktop";
import NavBar from "../../common/NavBar";
import AssistDesktop from "../../common/Desktop/Assistance/AssistDesktop";
import AssistanceRequestDetails from "../../common/Desktop/Assistance/AssistanceRequestDetails";
import AssistanceRequestDetailsValidation from "../../common/Desktop/Assistance/AssistanceRequestDetailsValidation";
import axios from "axios";
import urls from "../../urls";
import PendingRequestsContainer from "../../common/Desktop/Assistance/PendingRequestsContainer";
import AssistanceRequestClosingForm from "../../common/Desktop/Assistance/AssistanceRequestClosingForm";
import InvoiceContainer from "../../common/Invoice/InvoiceGenerator";
import {getNormalizedDate} from "../../common/date";
import {displayErrorMessage, displaySuccessMessage} from "../../common/toast";
import '../../common/Desktop/EmployeeDesktop/Desktop.scss';
import EmployeeContractsDesktop
    from "../../common/Desktop/EmployeeDesktop/EmployeeContractsDesktop/EmployeeContractsDesktop";

const PartnerDesktop = () => {

    const {t} = useTranslation();

    const [state, setState] = useState({
        navbarValue: 1,
        requests: [],
        isLoading: true,
        assistanceRequest: null
    });

    useEffect(() => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        }
        axios.get(urls.getSpringUrl() + "/intervention/related", config)
            .then(response => {
                if(response.data != null){
                    setState({...state, requests: response.data, isLoading: false});
                }
            })
    }, []);

    const onClosingSubmit = async (cost, description) => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        };
        const newDescription = state.assistanceRequest.description + "\n" + t('desktop_closing_request') + getNormalizedDate() + "\n" + description;
        const result = await axios.put(
            urls.getSpringUrl() + "/intervention/close",
            {interventionId: state.assistanceRequest.id, cost, description: newDescription},
            config
        );
        if(result.status !== 204){
            displayErrorMessage(t('assistance_request_closing_form_error'));
            return;
        }
        displaySuccessMessage(t('assistance_request_closing_form_success'));
        onRequestUpdate({...state.assistanceRequest, cost, status: 'ENDED', description: newDescription}, 5);
    };

    const changeTab = (value) => {
        setState({...state, navbarValue: value});
    };

    let navbarTitle;
    switch(state.navbarValue){
        case 2:
            navbarTitle = t('employee_desktop_pending');
            break;
        case 4:
            navbarTitle = t('employee_desktop_accepted');
            break;
        case 5:
            navbarTitle = t('employee_desktop_closed');
            break;
        case 8:
            navbarTitle = t('employee_desktop_contracts');
            break;
        default:
            navbarTitle = t('desktop_home');
            break;
    }
    
    const onSelectRequest = (request) => {
        // If the request is pending, it isn't in the requests array yet. We need to start tracking it, as it could be accepted by the user
        if(state.navbarValue === 2){
            const tempRequests = state.requests;
            tempRequests.push(request);
            setState({...state, navbarValue: 3, assistanceRequest: request, requests: tempRequests});
        }
        else
        {
            setState({...state, navbarValue: 3, assistanceRequest: request});
        }
    };

    const onRequestUpdate = (request, newTab) => {
        const newRequestsArray = state.requests.map(element => element.id === request.id ? {...request} : element);
        setState({...state, requests: newRequestsArray, navbarValue: newTab});
    };

    const onClosingRequest = () => {
        setState({...state, navbarValue: 6});
    };

    const onPDFOpening = () => {
        setState({...state, navbarValue: 7 });
    };

    let filteredRequests;
    switch(state.navbarValue){
        case 4:
            filteredRequests = state.requests.filter(element => element.status === "ACCEPTED");
            break;
        case 5:
            filteredRequests = state.requests.filter(element => element.status === "ENDED");
            break;
        default:
            filteredRequests = state.requests;
            break;
    }
    
    if(state.isLoading){
        return <div>Loading...</div>;
    }
    
    return(
        <div className={`desktop ${state.navbarValue === 7 ? 'desktop--scroll-disabled' : ''}`}>
            <div className="desktop__left-container">
                <div className="desktop-left">
                    <div className="desktop-left__avatar">
                        <Avatar alt={t('desktop_username')} src=""/>
                        <div className="desktop__username">{window.localStorage.getItem('useremail')}</div>
                        <hr/>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(1)}>{t('desktop_home')}</Button>
                        </div>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(2)}>{t('employee_desktop_pending')}</Button>
                        </div>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(4)}>{t('employee_desktop_accepted')}</Button>
                        </div>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(5)}>{t('employee_desktop_closed')}</Button>
                        </div>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(8)}>{t('employee_desktop_contracts')}</Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="desktop__right-container">
                <NavBar title={navbarTitle}/>
                {state.navbarValue === 1 &&
                    <PartnerHomeDesktop/>
                }
                {state.navbarValue === 2 &&
                    <PendingRequestsContainer onSelectRequest={onSelectRequest}/>
                }
                {state.navbarValue === 3 &&
                    <AssistanceRequestDetailsValidation request={state.assistanceRequest} onRequestUpdate={onRequestUpdate} onClosingRequest={onClosingRequest} onPdfOpening={onPDFOpening}>
                        <AssistanceRequestDetails request={state.assistanceRequest}/>
                    </AssistanceRequestDetailsValidation>
                }
                {(state.navbarValue === 4 || state.navbarValue === 5) &&
                    <AssistDesktop requests={filteredRequests} onSelectRequest={onSelectRequest}/>
                }
                {state.navbarValue === 6 &&
                    <AssistanceRequestClosingForm onSubmit={onClosingSubmit}/>
                }
                {state.navbarValue === 7 &&
                    <InvoiceContainer invoiceData={state.assistanceRequest}/>
                }
                {state.navbarValue === 8 &&
                    <EmployeeContractsDesktop/>
                }
            </div>
        </div>
    );
};

export default PartnerDesktop;