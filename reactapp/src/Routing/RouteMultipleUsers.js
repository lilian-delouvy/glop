import React, {FC} from "react";
import {Route} from "react-router-dom";
import {checkUserLoginStatus} from "../common/token";
import ForbiddenPage from "../common/ErrorPages/ForbiddenPage";

const RouteMultipleUsers: FC<React.ComponentProps<typeof Route>> = props => {
    let isUserLogged = false;
    for(let usertype of props.userTypes){
        if(checkUserLoginStatus(usertype)){
            isUserLogged = true;
        }
    }
    
    return(
        <Route {...props}>
            {isUserLogged ? props.children : <ForbiddenPage/>}
        </Route>
    );
};

export default RouteMultipleUsers;