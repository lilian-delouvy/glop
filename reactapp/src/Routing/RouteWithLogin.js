import React, {FC} from "react";
import {Route} from "react-router-dom";
import {checkUserLoginStatus} from "../common/token";
import ForbiddenPage from "../common/ErrorPages/ForbiddenPage";

const RouteWithLogin: FC<React.ComponentProps<typeof Route>> = props => {
    const isUserLogged = checkUserLoginStatus(props.userType);
    
    return(
        <Route {...props}>
            {isUserLogged ? props.children : <ForbiddenPage/>}
        </Route>
    );
};

export default RouteWithLogin;