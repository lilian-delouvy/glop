import {Switch, Route} from 'react-router';
import Home from "../Home";
import RouteWithLogin from "./RouteWithLogin";
import React from "react";
import IEDesktop from "../InsuranceEmployee/Desktop/IEDesktop";
import NotFound from "../common/ErrorPages/NotFound";
import EmployeeLoginContainer from "../common/EmployeesLogin/EmployeeLoginContainer";
import Discover from "../Customer/Discover/Discover";
import SignUpContainer from "../Customer/Authentication/SignUpContainer";
import DiscoverSignUpContainer from "../Customer/Discover/DiscoverSignUpContainer";
import InsuranceSubscription from "../Customer/Insurance/InsuranceSubscription";
import Desktop from "../Customer/Desktop/Desktop";
import PartnerDesktop from "../Partner/Desktop/PartnerDesktop";
import MEDesktop from "../MedicalEmployee/Desktop/MEDesktop";
import RouteMultipleUsers from "./RouteMultipleUsers";
import EmployeeContractDetailsContainer
    from "../common/Desktop/EmployeeDesktop/EmployeeContractsDesktop/EmployeeContractDetailsContainer";

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Home/>
            </Route>
            <Route exact path="/discover">
                <Discover/>
            </Route>
            <Route exact path="/signup">
                <SignUpContainer/>
            </Route>
            <Route exact path="/signup/:insuranceId">
                <DiscoverSignUpContainer/>
            </Route>
            <Route exact path="/insurance-employee/login">
                <EmployeeLoginContainer employeeType="insurance_employee"/>
            </Route>
            <Route exact path="/partner/login">
                <EmployeeLoginContainer employeeType="partner"/>
            </Route>
            <Route exact path="/medical-employee/login">
                <EmployeeLoginContainer employeeType="medical_employee"/>
            </Route>
            <RouteWithLogin exact path="/insurance-subscription/:insuranceId" userType="client">
                <InsuranceSubscription/>
            </RouteWithLogin>
            <RouteMultipleUsers exact path="/contract-details/:contractId" userTypes={["insurance_employee","partner","medical_employee"]}>
                <EmployeeContractDetailsContainer/>
            </RouteMultipleUsers>
            <RouteWithLogin exact path="/desktop" userType="client">
                <Desktop/>
            </RouteWithLogin>
            <RouteWithLogin exact path="/insurance-employee/desktop" userType="insurance_employee">
                <IEDesktop/>
            </RouteWithLogin>
            <RouteWithLogin exact path="/partner/desktop" userType="partner">
                <PartnerDesktop/>
            </RouteWithLogin>
            <RouteWithLogin exact path="/medical-employee/desktop" userType="medical_employee">
                <MEDesktop/>
            </RouteWithLogin>
            <Route>
                <NotFound/>
            </Route>
        </Switch>
    )
};

export default Routes;