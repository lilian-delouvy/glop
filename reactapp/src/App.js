import React from 'react'
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {HashRouter} from "react-router-dom";
import Routes from "./Routing/Routes";

function App() {
  return (
      <HashRouter basename={window.location.pathname}>
        <Routes/>
        <ToastContainer/>
      </HashRouter>
  );
}

export default App;
