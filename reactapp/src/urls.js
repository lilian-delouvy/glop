const json= {
    back :{
        dev: 'http://localhost:8080',
        prod: 'https://mobisure-spring.herokuapp.com'
    },
    front:{
        dev: 'http://localhost:3000',
        prod: 'https://mobisure-react.herokuapp.com'
    }
};

function getReactUrl() {
    if (process.env.NODE_ENV === 'production') {
        return json.front.prod;
      }
    else {
        return json.front.dev;
    }
}

function getSpringUrl() {
    if (process.env.NODE_ENV === 'production') {
        return json.back.prod;
      }
    else {
        return json.back.dev;
    }
}

module.exports= {getReactUrl,getSpringUrl};

