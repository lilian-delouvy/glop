import React, {useState} from "react";
import {ThemeProvider} from "@mui/material";
import Button from "@mui/material/Button";
import axios from "axios";
import {Redirect} from "react-router";
import './Home.scss';
import {useTranslation} from "react-i18next";
import LanguagesLoader from "./common/LanguagesLoader/LanguagesLoader";
import setTheme from "./common/ThemeColorProvider";
import AuthentContainer from "./Customer/Authentication/AuthentContainer";

const Home = () => {
    const [state,setState] = useState(
        {
            customers:[],
            isLoggedIn: false
        }
    );

    const theme = setTheme('#0E387A');

    const {t} = useTranslation();
    
    const isAlreadyLogged = localStorage.getItem('jwttoken') !== null;
    
    if(isAlreadyLogged){
        const userType = localStorage.getItem('usertype');
        let redirectUrl;
        switch (userType){
            case 'client':
                redirectUrl = "/desktop";
                break;
            case 'insurance_employee':
                redirectUrl = "/insurance-employee/desktop";
                break;
            case 'partner':
                redirectUrl = "/partner/desktop";
                break;
            case 'medical_employee':
                redirectUrl = "/medical-employee/desktop";
                break;
            default:
                redirectUrl = "/desktop";
                break;
        }
        return <Redirect to={redirectUrl}/>
    }

    return <>
        {state.isLoggedIn ? (
            <Redirect to="/desktop"/>
        ) : (
            <>
                <div className="app_container">
                    <div className="app_container__image-container">
                        <div className="app_container__background-image"></div>
                        <h1 className="app_container__image-title">
                            {t('home_slogan')}
                            <div className="app_container__image-button">
                                <ThemeProvider theme={theme}>
                                    <Button variant="contained" href="#discover"><div className="app_container__image-button-text">{t('home_discover')}</div></Button>
                                </ThemeProvider>
                            </div>
                        </h1>
                    </div>
                    <div className="app_container__authentication-block">
                        <div className="app_container__authentication-container">
                            <h2 className="app_container__title">{t('home_second_slogan')}</h2>
                            <AuthentContainer axios={axios} parentState={state} setParentState={setState}/>
                            <LanguagesLoader/>
                        </div>
                    </div>
                </div>
            </>
        )}
    </>;
}

export default Home;