import RowButton from "./RowButton";
import carIcon from '../../assets/images/InsurancesIcons/car.svg';
import travelIcon from '../../assets/images/InsurancesIcons/plane.svg';
import {useTranslation} from "react-i18next";

const RowButtonContainer = ({onClick, type, contract}) => {

    const {t} = useTranslation();
    
    let selectedIcon;
    let title;
    let info;
    switch (type) {
        case "TRAVEL":
            title = t('row_button_container_travel-title');
            selectedIcon = travelIcon;
            info = contract.country;
            break;
        case "TRANSPORT":
            title = t('row_button_container_transport-title');
            selectedIcon = carIcon;
            info = contract.transport.brand + " - " + contract.transport.model;
            break;
        default:
            break;
    }
    
    return(
        <RowButton
            onClick={onClick}
            title={title}
            icon={selectedIcon}
            info={info}
        />
    )
};

export default RowButtonContainer;