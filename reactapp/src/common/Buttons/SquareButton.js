import React from "react";
import './SquareButton.scss';
import Button from "@mui/material/Button";

const SquareButton = ({onClick, text}) => {
    return (
        <Button className="square-button__container">
            <div className="square-button__text" onClick={onClick}>{text}</div>
        </Button>
    );
};

export default SquareButton;
