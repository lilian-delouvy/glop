import RowButton from "./RowButton";
import {render} from "@testing-library/react";
import carIcon from '../../assets/images/InsurancesIcons/car.svg';
import RowButtonContainer from "./RowButtonContainer";
import SquareButton from "./SquareButton";

const testFunction = () => {};

describe("Check buttons behaviour", () => {
    test("RowButtonContainer should display RowButton accordingly to provided type", () => {
        const travelContract = {country: "FRANCE"};
        const transportContract = {transport: {brand: "Renault", model: "Clio 3"}};
        
        const {asFragment: asFragment1} = render(<RowButtonContainer onClick={testFunction} type="TRAVEL" contract={travelContract}/>);
        expect(asFragment1()).toMatchSnapshot();

        const {asFragment: asFragment2} = render(<RowButtonContainer onClick={testFunction} type="TRANSPORT" contract={transportContract}/>);
        
        expect(asFragment2()).toMatchSnapshot();
    });
    
    test("SquareButton should render correctly", () => {
        const {asFragment} = render(<SquareButton onClick={testFunction} text="Some text"/>);
        expect(asFragment()).toMatchSnapshot();
    })
})