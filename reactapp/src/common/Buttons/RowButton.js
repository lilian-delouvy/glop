import './RowButton.scss';

const RowButton = ({icon, onClick, title, info}) => {
    return(
        <div className="row-button__container">
            <button className="row-button__btn" onClick={onClick}>
                <img src={icon} alt="button icon"/>
                <p className="row-button__title">{title}</p>
                <p className="row-button__title">{info}</p>
            </button>
        </div>
    )
};

export default RowButton;