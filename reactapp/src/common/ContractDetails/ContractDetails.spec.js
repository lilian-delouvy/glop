import ContractDetails from "./ContractDetails";
import {render} from "@testing-library/react";
import i18n from "../../i18nextConf";
import {I18nextProvider} from "react-i18next";

const contract = {
    type: "TRAVEL",
    country: "FRANCE",
    subscriber: {
        id: 1,
        lastname: "DOE",
        firstname: "John",
        email: "john.doe@gmail.com"
    },
    subscriptionDate: "2022-02-07T09:46:31.155+00:00",
    endOfSubscriptionDate: "2023-02-07T09:46:31.155+00:00",
    dailyPrice: 1.5,
    rightHolders: [
        {
            id: 2,
            firstname: "firstname",
            lastname: "lastname",
            phoneNumber: "06 00 00 00 00"
        },
        {
            id: 3,
            firstname: "firstname_2",
            lastname: "lastname_2",
            phoneNumber: "06 00 00 00 01"
        }
    ]
};

describe("Check ContractDetails component behaviour", () => {
   test("Should render contract details correctly", () => {
       const {asFragment} = render(<I18nextProvider i18n={i18n}><ContractDetails contract={contract}/></I18nextProvider>);
       expect(asFragment()).toMatchSnapshot();
   });
});