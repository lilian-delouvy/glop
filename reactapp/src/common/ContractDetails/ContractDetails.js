import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import TableCell, {tableCellClasses} from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import {styled} from "@mui/material/styles";
import './ContractDetails.scss';
import {useTranslation} from "react-i18next";
import {translateType} from "../Desktop/translation";

const StyleHeaderTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#D3D3D3'
    }
}));

const ContractDetails = ({contract}) => {

    const {t} = useTranslation(["translation","contractRequestTypes"]);

    const requestTypes = [
        {key: 'TRAVEL', value: t('contractRequestTypes:contract_request_type_travel')},
        {key: 'TRANSPORT', value: t('contractRequestTypes:contract_request_type_transport')}
    ];
    
    return (
        <div className="contract-details-table">
            <div className="contract-details-table__container">
                <div className="contract-details__item contract-details__item--inline">
                    <b>{t('contract_details_type')}</b>{translateType(contract.type, requestTypes)}
                    {contract.type === 'TRAVEL' &&
                    <div className="contract-details__second-item"><b>{t('contract_details_country')}</b>{contract.country}</div>
                    }
                </div>
                <div className="contract-details__item contract-details__item--inline">
                    <div><b>{t('contract_details_lastname')}</b>{contract.subscriber.lastname}</div>
                    <div className="contract-details__second-item">
                        <b>{t('contract_details_firstname')}</b>{contract.subscriber.firstname}
                    </div>
                </div>
                <div className="contract-details__item">
                    <b>{t('contract_details_date')}</b>{contract.subscriptionDate.split('T')[0]}{t('contract_details_date_connector')}{contract.endOfSubscriptionDate.split('T')[0]}
                </div>
                <div className="contract-details__item"><b>{t('contract_details_client_id')}</b>{contract.subscriber.id}</div>
                <div className="contract-details__item"><b>{t('contract_details_price')}</b>{contract.dailyPrice} €</div>
                <div className="contract-details__item contract-details__item--last-item"><b>{t('contract_details_rightholders')}</b></div>
                <TableContainer component={Paper}>
                    <Table sx={{minWidth: 650}} aria-label="pending contracts table">
                        <TableHead>
                            <TableRow>
                                <StyleHeaderTableCell>{t('contract_details_tab_firstname')}</StyleHeaderTableCell>
                                <StyleHeaderTableCell align="right">{t('contract_details_tab_lastname')}</StyleHeaderTableCell>
                                <StyleHeaderTableCell align="right">{t('contract_details_tab_phone_number')}</StyleHeaderTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {contract.rightHolders.map(rightHolder => (
                                <TableRow
                                    key={rightHolder.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">{rightHolder.firstname}</TableCell>
                                    <TableCell align="right">{rightHolder.lastname}</TableCell>
                                    <TableCell align="right">{rightHolder.phoneNumber}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    )

};

export default ContractDetails;