import {createTheme} from "@mui/material";

const setTheme = (color) => {
    return createTheme({
    palette: {
        primary: {
            main: color
        }
    }
})};

export default setTheme;