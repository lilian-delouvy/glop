import React, {useState} from 'react';
import {ThemeProvider, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import './Login.scss';
import {useTranslation} from "react-i18next";
import setTheme from "../../ThemeColorProvider";

const Login = ({onSubmit}) => {

    const [state, setState] = useState({
        email: "",
        password: "",
    })

    const {t} = useTranslation();

    const theme = setTheme('#0E387A');
    
    const handleKeyPress = event => {
        if(event.key === "Enter"){
            onSubmit({email: state.email, password: state.password});
        }
    }

    return(
        <ThemeProvider theme={theme}>
            <TextField fullWidth label={t('authentication_email')} onChange={(e) => setState({...state, email: e.currentTarget.value})} variant="outlined" onKeyPress={handleKeyPress}/>
            <div className="login__password">
                <TextField fullWidth type="password" label={t('authentication_password')} onChange={(e) => setState({...state, password: e.currentTarget.value})} variant="outlined" onKeyPress={handleKeyPress}/>
            </div>
            <div className="login__centered-btn">
                <Button fullWidth variant="contained" onClick={() => onSubmit({email: state.email, password: state.password})}><div className="login__text-container">{t('global_validation')}</div></Button>
            </div>
        </ThemeProvider>
    )
};

export default Login;