import Login from "./Login";
import {render} from "@testing-library/react";

const onSubmit = () => {};

describe("Check Login component behaviour", () => {
    test("Should render correctly", () => {
        const {asFragment} = render(<Login onSubmit={onSubmit}/>);
        expect(asFragment()).toMatchSnapshot(); 
    });
});