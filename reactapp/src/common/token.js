export const checkUserLoginStatus = expectedUserType => {
    const loginToken = localStorage.getItem('jwttoken');
    const userType = localStorage.getItem('usertype');
    return loginToken !== null && userType === expectedUserType;
}