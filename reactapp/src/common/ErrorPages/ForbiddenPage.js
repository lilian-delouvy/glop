import React from "react";
import './ForbiddenPage.scss';
import {useTranslation} from "react-i18next";

const ForbiddenPage = () => {
    
    const {t} = useTranslation();
    
    return(
        <div className="forbidden__container">
            <h1 className="forbidden__title">403</h1>
            <div className="forbidden__text">{t('forbidden_page_text')}</div>
        </div>
    )
};

export default ForbiddenPage;