import React from "react";
import './NotFound.scss';
import {useTranslation} from "react-i18next";

const NotFound = () => {
    
    const {t} = useTranslation();
    
    return(
        <div className="not-found__container">
            <h1 className="not-found__title">404</h1>
            <div className="not-found__text">{t('not_found_text')}</div>
        </div>
    )
};

export default NotFound;