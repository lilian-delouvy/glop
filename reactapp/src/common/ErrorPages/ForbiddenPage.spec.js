import ForbiddenPage from "./ForbiddenPage";
import {render} from "@testing-library/react";
import i18n from "../../i18nextConf";
import {I18nextProvider} from "react-i18next";

describe("Should check ForbiddenPage behaviour", () => {
    test("Should render correctly", () => {
        const {asFragment} = render(<I18nextProvider i18n={i18n}><ForbiddenPage/></I18nextProvider>);
        expect(asFragment()).toMatchSnapshot();
    })
})