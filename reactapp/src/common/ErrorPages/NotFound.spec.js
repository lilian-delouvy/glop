import NotFound from "./NotFound";
import {render} from "@testing-library/react";
import i18n from "../../i18nextConf";
import {I18nextProvider} from "react-i18next";

describe("Check NotFound page behaviour", () => {
    test("Should render correctly", () => {
        const {asFragment} = render(<I18nextProvider i18n={i18n}><NotFound/></I18nextProvider>);
        expect(asFragment()).toMatchSnapshot();
    })
})