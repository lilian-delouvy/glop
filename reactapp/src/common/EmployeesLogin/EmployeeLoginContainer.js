import {useTranslation} from "react-i18next";
import axios from "axios";
import {useState} from "react";
import {Redirect} from "react-router";
import './EmployeeLoginContainer.scss';
import TitleBar from "../TitleBar/TitleBar";
import urls from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../toast";
import Login from "../Authentication/Login/Login";
import LanguagesLoader from "../LanguagesLoader/LanguagesLoader";

const replaceUnderscores = (employeeType) => {
    return employeeType.replace("_", "-");
}

const EmployeeLoginContainer = ({employeeType}) => {

    const [state, setState] = useState({
        isLoggedIn: false
    });
    
    const {t} = useTranslation();

    const confirmLogin = (response, userEmail) => {
        window.localStorage.setItem('jwttoken', JSON.stringify(response.data));
        window.localStorage.setItem('usertype', employeeType);
        window.localStorage.setItem('useremail', userEmail);
        displaySuccessMessage(t('authentication_signin_success'));
        setState({...state, isLoggedIn: true});
    }

    const onLoginSubmit = (e) => {
        axios.post(urls.getSpringUrl() + '/auth/' + employeeType + '/signin',e)
            .then(response => confirmLogin(response, e.email))
            .catch(() => displayErrorMessage(t('authentication_signin_error')));
    }
    
    let title;
    switch(employeeType){
        case "insurance_employee":
            title = t('employee_login_container_title_ie');
            break;
        case "medical_employee":
            title = t('employee_login_container_title_me')
            break;
        case "partner":
            title = t('employee_login_container_title_partner');
            break;
        default:
            break;
    }
    
    return(
        <>
            {state.isLoggedIn &&
                <Redirect to={'/' + replaceUnderscores(employeeType) + '/desktop'}/>
            }
            <TitleBar title={title}/>
            <div className="employee-login">
                <div className="employee-login__container">
                    <Login onSubmit={onLoginSubmit}/>
                    <div className="employee-login__languages-selector">
                        <LanguagesLoader/>
                    </div>
                </div>
            </div>
        </>
    )
    
};

export default EmployeeLoginContainer;