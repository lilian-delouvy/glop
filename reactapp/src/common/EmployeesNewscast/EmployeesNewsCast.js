import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";

const EmployeesNewsCast = () => {
    
    const [state, setState] = useState({
        news: []
    });
    
    const {t} = useTranslation();
    
    useEffect(() => {
        //TODO: load employees newscast
    }, []);
    
    return(
        <>
            <h3 className="desktop__title">{t('employees_newscast_title')}</h3>
            {state.news.length === 0 ? (
                <h4 className="desktop__error">{t('employees_newscast_no_news')}</h4>
            ) : (
                <div>
                    {state.news.map(current =>
                        <div>
                            {current.title}
                            {current.description}
                        </div>
                    )}
                </div>
            )}
        </>
    );
};

export default EmployeesNewsCast;