import './TitleBar.scss';
import logo from "../../assets/images/MobiSure_Insurance.PNG";

const TitleBar = ({title}) => {
    
    return(
        <div className="title-bar">
            <img className="title-bar__logo" src={logo} alt="mobisure_insurance_logo"/>
            <p className="title-bar__title">{title}</p>
        </div>
    )
    
};

export default TitleBar;