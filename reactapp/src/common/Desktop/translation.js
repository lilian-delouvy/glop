export const translateType = (type, allTypes) => {
    return allTypes.find(element => element.key === type).value;
};