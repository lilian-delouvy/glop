import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import {useEffect, useState} from "react";
import axios from "axios";
import {useTranslation} from "react-i18next";
import urls from "../../../../urls";
import {displayErrorMessage} from "../../../toast";
import ContractsList from "../../../../InsuranceEmployee/ContractsList/ContractsList";
import './EmployeeContractsDesktop.scss';

const EmployeeContractsDesktop = () => {

    const [state, setState] = useState({
        filter: 'Old to recent',
        searched: "",
        statusFilter: "PENDING",
        contracts: []
    });

    const {t} = useTranslation(["translation","employeeFilters"]);

    useEffect(() => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        }
        axios.get(urls.getSpringUrl() + "/employees/contracts", config)
            .then(response => {
                if(response.status === 200){
                    setState({...state, contracts: response.data});
                }
                else{
                    displayErrorMessage("An error occured while loading pending contracts");
                }
            })
    }, []);

    const filteredByStatusContracts = state.contracts.filter(contract => contract.status === state.statusFilter);
    let filteredContracts;
    switch(state.filter){
        case "Old to recent":
            filteredContracts = [...filteredByStatusContracts];
            filteredContracts = filteredContracts.sort((a,b) => (a.subscriptionDate - b.subscriptionDate) ? -1 : 1);
            break;
        case "Recent to old":
            filteredContracts = [...filteredByStatusContracts];
            filteredContracts = filteredContracts.sort((a,b) => (a.subscriptionDate - b.subscriptionDate) ? 1 : -1);
            break;
        case "Travel":
            filteredContracts = filteredByStatusContracts.filter(contract => contract.type === "TRAVEL");
            break;
        case "Transport":
            filteredContracts = filteredByStatusContracts.filter(contract => contract.type === "TRANSPORT");
            break;
        default:
            break;
    }

    let filteredAndSearched;
    if(state.searched !== ""){
        filteredAndSearched = filteredContracts.filter(contract =>
            `${contract.subscriber.firstname.toLowerCase()} ${contract.subscriber.lastname.toLowerCase()}`.includes(state.searched.toLowerCase()) ||
            `${contract.subscriber.lastname.toLowerCase()} ${contract.subscriber.firstname.toLowerCase()}`.includes(state.searched.toLowerCase())
        );
    }
    else{
        filteredAndSearched = filteredContracts;
    }

    const filterOptions = [
        {key: 'Old to recent', value: t('employeeFilters:employee_filter_old_to_recent')},
        {key: 'Recent to old', value: t('employeeFilters:employee_filter_recent_to_old')},
        {key: 'Travel', value: t('employeeFilters:employee_filter_travel')},
        {key: 'Transport', value: t('employeeFilters:employee_filter_transport')}
    ];
    
    const statusFilterOptions = [
        {key: 'PENDING', value: t('employeeFilters:employee_filter_pending')},
        {key: 'VALID', value: t('employeeFilters:employee_filter_valid')},
        {key: 'ENDED', value: t('employeeFilters:employee_filter_ended')}
    ]

    const openContractDetails = (id) => {
        const link = `#/contract-details/${id}`;
        window.open(link);
    };

    return (
        <>
            <div className="contracts-list-container__options">
                <FormControl>
                    <InputLabel id="countries-select-label">{t('employee_contracts_desktop_filters')}</InputLabel>
                    <Select
                        labelId="countries-select-label"
                        id="countries-select"
                        value={state.filter}
                        label="Filter"
                        onChange={(e) => setState({...state, filter: e.target.value})}
                    >
                        {filterOptions.map(filter => (
                            <MenuItem key={filter.key} value={filter.key}>{filter.value}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl>
                    <InputLabel id="status-select-label">{t('employee_contracts_desktop_status_filter')}</InputLabel>
                    <Select
                        labelId="status-select-label"
                        id="status-select"
                        value={state.statusFilter}
                        label="Status filter"
                        onChange={(e) => setState({...state, statusFilter: e.target.value})}
                    >
                        {statusFilterOptions.map(filter => (
                            <MenuItem key={filter.key} value={filter.key}>{filter.value}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <TextField
                    id="add-rightHolder"
                    label={t('employee_contracts_desktop_name')}
                    variant="outlined"
                    value={state.searched}
                    onChange={e => setState({...state, searched: e.target.value})}
                />
            </div>
            <ContractsList contracts={filteredAndSearched} onClick={openContractDetails}/>
        </>
    );

};

export default EmployeeContractsDesktop;