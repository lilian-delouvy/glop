import TitleBar from "../../../TitleBar/TitleBar";
import ContractDetails from "../../../ContractDetails/ContractDetails";
import {useEffect, useState} from "react";
import {useParams} from "react-router";
import axios from "axios";
import urls from "../../../../urls";
import {displayErrorMessage} from "../../../toast";
import {styled} from "@mui/material/styles";
import Button from "@mui/material/Button";
import {useTranslation} from "react-i18next";
import './EmployeeContractDetailsContainer.scss';

const RejectButton = styled(Button)(({ theme }) => ({
    color: 'white',
    backgroundColor: 'red',
    '&:hover': {
        backgroundColor: 'darkred',
    },
}));

const ValidateButton = styled(Button)(({ theme }) => ({
    color: 'white',
    backgroundColor: 'green',
    '&:hover': {
        backgroundColor: 'darkgreen',
    },
}));

const EmployeeContractDetailsContainer = () => {
    
    const {contractId} = useParams();
    
    const [state, setState] = useState({
        contract: null,
        isLoading: true
    });
    
    const {t} = useTranslation();

    useEffect(() => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        axios({
            method: 'get',
            url: urls.getSpringUrl() + "/contract-details/" + contractId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${tokenValue}`
            }
        }).then(response => {
            if(response.status === 200){
                setState({...state, contract: response.data, isLoading: false});
            }
        })
    },[]);

    const onAction = (isValidated) => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        if(isValidated){
            axios({
                method: 'post',
                url: urls.getSpringUrl() + "/insurance_employee/contract/validate",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${tokenValue}`
                },
                data: contractId
            }).then(response => {
                if(response.status === 200){
                    window.close();
                }
                else{
                    displayErrorMessage("An error occured while approving contract");
                }
            });
        }
        else{
            axios({
                method: 'post',
                url: urls.getSpringUrl() + "/insurance_employee/contract/invalidate",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${tokenValue}`
                },
                data: contractId
            }).then(response => {
                if(response.status === 200){
                    window.close();
                }
                else{
                    displayErrorMessage("An error occured while rejecting contract");
                }
            });
        }
    }
    
    if(state.isLoading){
        return <div>Loading...</div>;
    }

    const areButtonsShown = state.contract.status === "PENDING" && localStorage.getItem('usertype') === "insurance_employee";
    
    return(
        <>
            <TitleBar title={t('employee_contract_details_container_titlebar')}/>
            <div className="employee-contracts">
                <div className="employee-contracts__container">
                    <div className="employee-contracts__header">
                        <b>{t('employee_contract_details_container_title')}</b>
                        {areButtonsShown ? (
                            <div>
                                <RejectButton onClick={() => onAction(false)} variant="contained" sx={{backgroundColor: 'red'}}>{t('employee_contract_details_container_reject')}</RejectButton>
                                <ValidateButton onClick={() => onAction(true)} variant="contained" sx={{backgroundColor: 'green', marginLeft: '1em'}}>{t('employee_contract_details_container_accept')}</ValidateButton>
                            </div>
                            ) : (
                                <div>{state.contract.status}</div>
                            )
                        }
                    </div>
                    <ContractDetails contract={state.contract}/>
                </div>
            </div>
        </>
    )
};

export default EmployeeContractDetailsContainer;