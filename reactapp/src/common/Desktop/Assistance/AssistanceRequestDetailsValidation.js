import Button from '@mui/material/Button';
import CheckIcon from '@mui/icons-material/Check';
import './AssistanceRequestDetailsValidation.scss';
import axios from "axios";
import {getSpringUrl} from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../../toast";
import {useTranslation} from "react-i18next";
import setTheme from "../../ThemeColorProvider";
import {ThemeProvider} from "@mui/material";
import './AssistanceRequestClosingForm.scss';
import {useState} from "react";

const AssistanceRequestDetailsValidation = ({request, children, onRequestUpdate, onClosingRequest, onPdfOpening}) => {
    
    const [state, setState] = useState({
        isButtonClicked: false
    });
    
    const {t} = useTranslation();
    
    const theme = setTheme('#0E387A');
    
    const onClick = () => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        };
        if(request.status === "PENDING"){
            axios.post(getSpringUrl() + "/intervention/update", {id: request.id, status: "ACCEPTED"}, config)
                .then(() => {
                    displaySuccessMessage(t('assistance_request_details_acceptation_success'));
                    onRequestUpdate({...request, status: 'ACCEPTED'}, 4);
                    setState({...state, isButtonClicked: true});
                })
                .catch(() => displayErrorMessage(t('assistance_request_details_acceptation_error')));
        }
        if(request.status === "ACCEPTED"){
            onClosingRequest();
        }
        if(request.status === "ENDED"){
            onPdfOpening();
        }
    }
    
    const setButtonText = () => {
        let returnedText;
        switch(request.status){
            case "PENDING":
                returnedText = t('assistance_request_details_validation_accept_request');
                break;
            case "ACCEPTED":
                returnedText = t('assistance_request_details_validation_close_request');
                break;
            case "ENDED":
                returnedText = t('assistance_request_details_validation_generate_invoice');
                break;
            default:
                break;
        }
        return returnedText;
    }
    
    return(
        <>
            {!state.isButtonClicked &&
                <div className="validation-btn__container">
                    <ThemeProvider theme={theme}>
                        <Button variant="contained" startIcon={<CheckIcon/>} onClick={onClick}>
                            {setButtonText()}
                        </Button>
                    </ThemeProvider>
                </div>
            }
            <>{children}</>
        </>
    );
};

export default AssistanceRequestDetailsValidation;