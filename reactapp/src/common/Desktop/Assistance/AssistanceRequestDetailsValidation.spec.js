import {render} from "@testing-library/react";
import AssistanceRequestDetailsValidation from "./AssistanceRequestDetailsValidation";
import i18n from "../../../i18nextConf";
import {I18nextProvider} from "react-i18next";

const request = {
    id: 1,
    status: 'PENDING',
    interventionEmployee: {
        email: "someone@gmail.com"
    }
};

describe("Should check AssistanceRequestDetailsValidation behaviour", () => {
    test("Should render correctly", () => {
        const {asFragment} = render(<I18nextProvider i18n={i18n}>
            <AssistanceRequestDetailsValidation request={request} onClosingRequest={() => {}} onRequestUpdate={() => {}} onPdfOpening={() => {}}>
                <div>Test</div>
            </AssistanceRequestDetailsValidation>
        </I18nextProvider>);
        expect(asFragment()).toMatchSnapshot();
    })
})