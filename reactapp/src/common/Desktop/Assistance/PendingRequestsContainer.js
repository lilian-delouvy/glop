import {useEffect, useState} from "react";
import axios from "axios";
import urls from "../../../urls";
import AssistDesktop from "./AssistDesktop";

const PendingRequestsContainer = ({onSelectRequest}) => {
    const [state, setState] = useState({
        pendingRequests: [],
        isLoading: true
    });

    useEffect(() => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        }
        axios.get(urls.getSpringUrl() + "/intervention/PENDING", config)
            .then(response => {
                if(response.data != null){
                    setState({...state, pendingRequests: response.data, isLoading: false});
                }
            })
    }, []);
    
    if(state.isLoading){
        return <div>Loading...</div>;
    }
    
    return(
        <AssistDesktop requests={state.pendingRequests} onSelectRequest={onSelectRequest}/>
    )
};

export default PendingRequestsContainer;