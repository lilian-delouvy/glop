import {useState} from "react";
import {TextField} from "@mui/material";
import {useTranslation} from "react-i18next";
import Button from "@mui/material/Button";
import InputValidation from "../../Validation/InputValidation";
import {NUMBER, REQUIRED} from "../../Validation/rules";

const checkFormInputs = (cost, description) => {
    const regex = new RegExp( /^[0-9]\d*$/g);
    return !(!cost ||
        !regex.test(cost) ||
        !description);
}

const AssistanceRequestClosingForm = ({onSubmit}) => {
    
    const [state, setState] = useState({
        interventionCost: 0,
        description: "",
        isInputValid: true
    });
    
    const {t} = useTranslation();
    
    const onClosingSubmit = async () => {
        if(checkFormInputs(state.interventionCost, state.description)){
            return onSubmit(parseInt(state.interventionCost, 10), state.description);
        }
        else{
            setState({...state, isInputValid: false});
        }
    }
    
    return(
        <>
            <div className="assistance-request-closing-form__container">
                <div className="assistance-request-closing-form__item">
                    {t('assistance_request_closing_form_info')}
                </div>
                <div className="assistance-request-closing-form__item">
                    <InputValidation value={state.interventionCost} rules={[REQUIRED,NUMBER]}>
                        <TextField
                            fullWidth
                            type="number"
                            id="cost-id"
                            InputLabelProps={{ shrink: true }}
                            label={t('assistance_request_closing_form_cost') + " (€)"}
                            variant="outlined"
                            value={state.interventionCost}
                            onChange={e => setState({...state, interventionCost: e.target.value})}
                        />
                    </InputValidation>
                </div>
                <div className="assistance-request-closing-form__item">
                    <InputValidation value={state.description} rules={[REQUIRED]}>
                        <TextField
                            fullWidth
                            id="description-id"
                            label={t('assistance_request_closing_form_description')}
                            variant="outlined"
                            value={state.description}
                            onChange={e => setState({...state, description: e.target.value})}
                        />
                    </InputValidation>
                </div>
                {!state.isInputValid &&
                    <div className="assistance-request-closing-form__item assistance-request-closing-form__item--error">
                        {t('assistance_request_closing_form_inputs_error')}
                    </div>
                }
                <div className="assistance-request-closing-form__item assistance-request-closing-form__item--button">
                    <Button onClick={onClosingSubmit}>{t('assistance_request_closing_form_submit')}</Button>
                </div>
            </div>
        </>
    );
};

export default AssistanceRequestClosingForm;