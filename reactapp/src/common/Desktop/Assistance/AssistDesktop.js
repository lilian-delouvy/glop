import {styled} from "@mui/material/styles";
import TableCell, {tableCellClasses} from "@mui/material/TableCell";
import {useTranslation} from "react-i18next";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useState} from "react";
import './AssistDesktop.scss';

const StyleHeaderTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#D3D3D3'
    }
}));

const translateInterventionTypes = (types, translatedTypes) => {
    const result = [];
    for(let type of types){
        for(let translatedType of translatedTypes){
            if(type === translatedType.key){
                result.push(translatedType.value);
            }
        }
    }
    return result.join(", ");
}

const AssistDesktop = ({requests, onSelectRequest}) => {
    
    const {t} = useTranslation(["translation","interventionTypes","employeeFilters"]);
    
    const [state, setState] = useState({
        filter: 'Old to recent'
    });

    const interventionTypes = [
        {key: 'INJURY', value: t('interventionTypes:intervention_type_injured')},
        {key: 'MATERIAL', value: t('interventionTypes:intervention_type_material')},
        {key: 'UNDEFINED', value: t('interventionTypes:intervention_type_undefined')}
    ];

    const filterOptions = [
        {key: 'Old to recent', value: t('employeeFilters:employee_filter_old_to_recent')},
        {key: 'Recent to old', value: t('employeeFilters:employee_filter_recent_to_old')},
        {key: 'Last month', value: t('employeeFilters:employee_filter_last_month')}
    ];
    
    let filteredRequests = [...requests];
    switch (state.filter) {
        case 'Old to recent':
            filteredRequests = filteredRequests.sort((a,b) => new Date(a.date) - new Date(b.date));
            break;
        case 'Recent to old':
            filteredRequests = filteredRequests.sort((a,b) => new Date(b.date) - new Date(a.date));
            break;
        case 'Last month':
            const todayDate = new Date();
            const lastMonthDate = new Date(new Date().setDate(todayDate.getDate() -30));
            filteredRequests = filteredRequests.filter(request => new Date(request.date).getTime() >= new Date(lastMonthDate).getTime());
            break;
        default:
            break;
    }
    
    return(
        <>
            {requests.length === 0 ? (
                <p className="contracts-list__error">{t('pending_list_no_contracts')}</p>
            ) : (
                <>
                    <div className="contracts-list-container__options">
                        <FormControl>
                            <InputLabel id="countries-select-label">Filter</InputLabel>
                            <Select
                                labelId="countries-select-label"
                                id="countries-select"
                                value={state.filter}
                                label="Filter"
                                onChange={(e) => setState({...state, filter: e.target.value})}
                            >
                                {filterOptions.map(filter => (
                                    <MenuItem key={filter.key} value={filter.key}>{filter.value}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="pending contracts table">
                            <TableHead>
                                <TableRow>
                                    <StyleHeaderTableCell>{t('assistance_request_firstname')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('assistance_request_lastname')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('assistance_request_date')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('assistance_request_city')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('assistance_request_description')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('assistance_request_types')}</StyleHeaderTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredRequests.map(request => (
                                    <TableRow
                                        key={request.id}
                                        hover
                                        sx={{ '&:last-child td, &:last-child th': { border: 0}, 'cursor': 'pointer'}}
                                        onClick={() => onSelectRequest(request)}
                                    >
                                        <TableCell component="th" scope="row">{request.contract.subscriber.firstname}</TableCell>
                                        <TableCell align="right">{request.contract.subscriber.lastname}</TableCell>
                                        <TableCell align="right">{request.date.split('T')[0]}</TableCell>
                                        <TableCell align="right">{request.city}</TableCell>
                                        <TableCell align="right">{request.description.split('\n')[0]}</TableCell>
                                        <TableCell align="right">{translateInterventionTypes(request.types, interventionTypes)}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </>
            )}
        </>
    );
};

export default AssistDesktop;