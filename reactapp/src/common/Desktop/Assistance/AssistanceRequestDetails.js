import {useTranslation} from "react-i18next";
import {styled} from "@mui/material/styles";
import TableCell, {tableCellClasses} from "@mui/material/TableCell";
import './AssistanceRequestDetails.scss';
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';

const StyleHeaderTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#D3D3D3'
    }
}));

const getTranslatedInterventionTypes = (currentTypes, allInterventionTypes) => {
    const result = [];
    for(let type of currentTypes){
        let typeInArray = allInterventionTypes.find(element => element.key === type);
        result.push(typeInArray.value);
    }
    return result.join(", ");
}

const getTranslatedLocalization = (city, country, countriesArray) => {
    const countryInArray = countriesArray.find(element => element.key === country);
    return city + ", " + countryInArray.value;
};

const AssistanceRequestDetails = ({request}) => {
    
    const {t} = useTranslation(["translation", "interventionTypes", "countries"]);
    
    const interventionTypes = [
        {key: 'INJURY', value: t('interventionTypes:intervention_type_injured')},
        {key: 'MATERIAL', value: t('interventionTypes:intervention_type_material')},
        {key: 'UNDEFINED', value: t('interventionTypes:intervention_type_undefined')}
    ];

    const availableCountries = [
        {key: "FRANCE", value: t('countries:country_fr')},
        {key: "SWITZERLAND", value: t('countries:country_be')},
        {key: "BELGIUM", value: t('countries:country_ch')}
    ];
    
    return(
        <div className="assistance-request-table">
            <div className="assistance-request-table__container">
                <div className="assistance-request__item assistance-request__item--inline">
                    <b>{t('assistance_request_types')}</b>{getTranslatedInterventionTypes(request.types, interventionTypes)}
                </div>
                <div className="assistance-request__item">
                    <b>{t('assistance_request_localization')}</b>{getTranslatedLocalization(request.city, request.country, availableCountries)}
                </div>
                <div className="assistance-request__item assistance-request__item--inline">
                    <div><b>{t('assistance_request_lastname')}</b>{request.contract.subscriber.lastname}</div>
                    <div className="assistance-request__second-item">
                        <b>{t('assistance_request_firstname')}</b>{request.contract.subscriber.firstname}
                    </div>
                </div>
                <div className="assistance-request__item">
                    <b>{t('assistance_request_date')}</b>{request.date.split('T')[0]}
                </div>
                <div className="assistance-request__item">
                    <b>{t('assistance_request_description')}</b>
                    <div className="assistance-request__description">{request.description}</div>
                </div>
                <div className="assistance-request__item assistance-request__item--last-item"><b>{t('assistance_request_rightholders')}</b></div>
                <TableContainer component={Paper}>
                    <Table sx={{minWidth: 650}} aria-label="pending requests table">
                        <TableHead>
                            <TableRow>
                                <StyleHeaderTableCell>{t('assistance_request_tab_firstname')}</StyleHeaderTableCell>
                                <StyleHeaderTableCell align="right">{t('assistance_request_tab_lastname')}</StyleHeaderTableCell>
                                <StyleHeaderTableCell align="right">{t('assistance_request_tab_phone_number')}</StyleHeaderTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {request.contract.rightHolders.map(rightHolder => (
                                <TableRow
                                    key={rightHolder.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">{rightHolder.firstname}</TableCell>
                                    <TableCell align="right">{rightHolder.lastname}</TableCell>
                                    <TableCell align="right">{rightHolder.phoneNumber}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                {request.interventionEmployee &&
                    <div className="assistance-request__item">
                        <b>{t('assistance_request_intervention_employee')}</b>{request.interventionEmployee.email}
                    </div>
                }
            </div>
        </div>
    );
    
};

export default AssistanceRequestDetails;