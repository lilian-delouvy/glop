import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React from "react";
import {useTranslation} from "react-i18next";

const stylesInvoiceUserInfo = StyleSheet.create({
    headerContainer: {
        marginTop: 36
    },
    userInfo: {
        marginTop: 20,
        paddingBottom: 3,
        fontFamily: 'Helvetica-Oblique',
        fontSize: 12
    },
});


const InvoiceUserInfo = ({invoiceData}) => {
    const {t} = useTranslation(['invoice']);
    return(
        <View style={stylesInvoiceUserInfo.headerContainer}>
            <Text style={stylesInvoiceUserInfo.userInfo}>{t('invoice:invoice_client')}</Text>
            <Text>{invoiceData.contract.subscriber.lastname}</Text>
            <Text>{invoiceData.contract.subscriber.firstname}</Text>
            <Text>{invoiceData.contract.subscriber.address}</Text>
            <Text>{invoiceData.contract.subscriber.phoneNumber}</Text>
            <Text>{invoiceData.contract.subscriber.email}</Text>
        </View>
    )
};

export default InvoiceUserInfo;