import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React, {Fragment} from "react";
import {useTranslation} from "react-i18next";

const stylesInvoiceInfo = StyleSheet.create({
    invoiceRowContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    invoiceValue: {
        fontSize: 12,
        fontStyle: 'bold',
    }
});

const InvoiceEmployeeInfo = ({invoiceData}) => {
    const {t} = useTranslation(['invoice']);
    return(
        <Fragment>
            <View style={stylesInvoiceInfo.invoiceRowContainer}>
                <Text>{t('invoice:invoice_employee_email')}</Text>
                <Text style={stylesInvoiceInfo.invoiceValue}>{invoiceData.interventionEmployee.email}</Text>
            </View>
        </Fragment>
    );
};

export default InvoiceEmployeeInfo;