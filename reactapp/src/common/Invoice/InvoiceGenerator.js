import React  from 'react';
import { Page, Document, Image, StyleSheet } from '@react-pdf/renderer';
import { PDFViewer } from '@react-pdf/renderer';
import logo from "../../assets/images/MobiSure_Insurance.PNG";
import './InvoiceGenerator.scss';
import InvoiceTitle from "./InvoiceTitle";
import InvoiceRequestDescription from "./InvoiceRequestDescription";
import InvoiceFooter from "./InvoiceFooter";
import InvoiceUserInfo from "./InvoiceUserInfo";
import InvoiceInfo from "./InvoiceInfo";
import InvoiceEmployeeInfo from "./InvoiceEmployeeInfo";

const styles = StyleSheet.create({
    page: {
        fontFamily: 'Helvetica',
        fontSize: 11,
        paddingTop: 30,
        paddingLeft:60,
        paddingRight:60,
        lineHeight: 1.5,
        flexDirection: 'column',
    },
    logo: {
        width: 100,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto'
    }
});

const InvoiceGenerator = ({invoiceData}) => (
    <Document>
        <Page size="A4" style={styles.page}>
            <Image style={styles.logo} src={logo} />
            <InvoiceTitle/>
            <InvoiceInfo invoiceData={invoiceData}/>
            <InvoiceEmployeeInfo invoiceData={invoiceData}/>
            <InvoiceUserInfo invoiceData={invoiceData}/>
            <InvoiceRequestDescription invoiceData={invoiceData}/>
            <InvoiceFooter />
        </Page>
    </Document>
);

const InvoiceContainer = ({invoiceData}) => (
    <PDFViewer className="invoice-viewer">
        <InvoiceGenerator invoiceData={invoiceData}/>
    </PDFViewer>
);

export default InvoiceContainer;