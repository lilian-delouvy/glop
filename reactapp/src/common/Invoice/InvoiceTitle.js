import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React from "react";
import {useTranslation} from "react-i18next";

const stylesInvoiceTitle = StyleSheet.create({

    titleContainer:{
        flexDirection: 'row',
        marginTop: 24,
    },
    invoiceTitle:{
        color: '#61dafb',
        letterSpacing: 4,
        fontSize: 25,
        textAlign: 'center',
        textTransform: 'uppercase',
    }
});


const InvoiceTitle = () => {
    
    const {t} = useTranslation(["invoice"]);
    
    return(
        <View style={stylesInvoiceTitle.titleContainer}>
            <Text style={stylesInvoiceTitle.invoiceTitle}>{t('invoice:invoice_title')}</Text>
        </View>
    );
};

export default InvoiceTitle;