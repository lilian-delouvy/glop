import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React, {Fragment} from "react";
import {getNormalizedDate} from "../date";
import {useTranslation} from "react-i18next";

const stylesInvoiceInfo = StyleSheet.create({
    invoiceNumberContainer: {
        flexDirection: 'row',
        marginTop: 36,
        justifyContent: 'flex-end'
    },
    invoiceDateContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    invoiceValue: {
        fontSize: 12,
        fontStyle: 'bold',
    }

});


const InvoiceInfo = ({invoiceData}) => {
    const {t} = useTranslation(['invoice']);
    return(
        <Fragment>
            <View style={stylesInvoiceInfo.invoiceNumberContainer}>
                <Text>{t('invoice:invoice_number')}</Text>
                <Text style={stylesInvoiceInfo.invoiceValue}>{invoiceData.id}</Text>
            </View >
            <View style={stylesInvoiceInfo.invoiceDateContainer}>
                <Text>{t('invoice:invoice_date')}</Text>
                <Text style={stylesInvoiceInfo.invoiceValue}>{getNormalizedDate(invoiceData.date)}</Text>
            </View >
        </Fragment>
    );
};

export default InvoiceInfo;