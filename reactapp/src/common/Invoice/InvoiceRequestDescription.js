import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React from "react";
import {useTranslation} from "react-i18next";

const stylesInvoiceRequestDescription = StyleSheet.create({
    invoiceRequestInfoContainer: {
        marginTop: 36
    },
    invoiceRequestInfoTitle: {
        marginTop: 5,
        fontSize: 12,
        fontStyle: 'bold'
    },
    invoiceRequestInfo: {
        marginTop: 20,
        paddingBottom: 3,
        fontFamily: 'Helvetica-Oblique',
        fontSize: 12
    }
});

const InvoiceRequestDescription = ({invoiceData}) => {
    const {t} = useTranslation(['invoice'])
    return(
        <View style={stylesInvoiceRequestDescription.invoiceRequestInfoContainer}>
            <Text style={stylesInvoiceRequestDescription.invoiceRequestInfo}>{t('invoice:invoice_assistance_request_info')}</Text>
            <Text style={stylesInvoiceRequestDescription.invoiceRequestInfoTitle}>{t('invoice:invoice_assistance_request_description')}</Text>
            <Text>{invoiceData.description}</Text>
            <Text style={stylesInvoiceRequestDescription.invoiceRequestInfoTitle}>{t('invoice:invoice_assistance_request_cost')}</Text>
            <Text>{invoiceData.cost} €</Text>
        </View>
    );
};

export default InvoiceRequestDescription;