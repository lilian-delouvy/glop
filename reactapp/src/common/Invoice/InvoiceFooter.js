import {StyleSheet, Text, View} from "@react-pdf/renderer";
import React from "react";
import {useTranslation} from "react-i18next";

const stylesFooter = StyleSheet.create({
    invoiceFooterContainer:{
        flexDirection: 'row',
        position: 'absolute',
        bottom: 10,
        paddingLeft: 60
    },
    invoiceFooter:{
        fontSize: 12,
        textAlign: 'center'
    }
});


const InvoiceFooter = () => {
    const {t} = useTranslation(['invoice']);
    return(
        <View style={stylesFooter.invoiceFooterContainer}>
            <Text style={stylesFooter.invoiceFooter}>{t('invoice:invoice_thanks')}</Text>
        </View>
    );
};

export default InvoiceFooter;