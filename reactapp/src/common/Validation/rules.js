export const REQUIRED = {
    key: 'REQUIRED'
};

export const NUMBER = {
    key: 'NUMBER',
    regex: /^[0-9]*$/
};

export const FLOATNUMBER = {
    key: 'FLOATNUMBER',
    regex: /^[+-]?\d+(\.\d+|,\d+)?$/
}

export const REGEX = ({pattern, message}) => ({
    key: 'REGEX',
    pattern: pattern,
    message: message
});