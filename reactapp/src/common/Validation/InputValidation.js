import {useTranslation} from "react-i18next";
import './InputValidation.scss';
import {useEffect, useRef, useState} from "react";

function useDidUpdateEffect(fn, inputs) {
    const didMountRef = useRef(false);

    useEffect(() => {
        if (didMountRef.current) {
            return fn();
        }
        didMountRef.current = true;
    }, inputs);
}

const InputValidation = ({value, rules, children}) => {
    const {t} = useTranslation(["validationRules"]);
    
    const [state, setState] = useState({
        hasChanged: false
    });
    
    useDidUpdateEffect(() => {
        if(!state.hasChanged){
            setState({...state, hasChanged: true});
        }
    }, [value]);
    
    const applyValidationRules = () => {
        if(!state.hasChanged){
            return <></>;
        }
        for(let rule of rules){
            switch(rule.key){
                case 'REQUIRED':
                    if(!value){
                        return <div className="validation__error">{t('validationRules:validation_rules_required')}</div>;
                    }
                    break;
                case 'NUMBER':
                    if(!rule.regex.test(value)){
                        return <div className="validation__error">{t('validationRules:validation_rules_number_only')}</div>;
                    }
                    break;
                case 'FLOATNUMBER':
                    if(!rule.regex.test(value)){
                        return <div className="validation__error">{t('validationRules:validation_rules_float_number_only')}</div>
                    }
                    break;
                case 'REGEX':
                    if(!rule.pattern.test(value)){
                        return <div className="validation__error">{rule.message}</div>;
                    }
                    break;
                default:
                    return <></>;
            }
        }
    };
    
    return (
        <>
            <>{children}</>
            {applyValidationRules()}
        </>
    )
};

export default InputValidation;