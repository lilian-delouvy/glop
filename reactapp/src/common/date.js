export const getNormalizedDate = () => {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    const d = new Date()
    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')
};