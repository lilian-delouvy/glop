import {Suspense} from "react";
import LanguagesLoader from "./LanguagesLoader";

export default {
    title: 'Project/LanguagesLoader',
    component: LanguagesLoader
};

const Template = () => <Suspense fallback={<div>Loading...</div>}><LanguagesLoader/></Suspense>;

export const Default = Template.bind({});
Default.args = {};