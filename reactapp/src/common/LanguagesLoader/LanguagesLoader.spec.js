import LanguagesLoader from "./LanguagesLoader";
import {render, waitFor} from "@testing-library/react";
import i18n from '../../i18nextConf';
import {I18nextProvider} from 'react-i18next';

describe('Should check languages loader behaviour', () => {
    test("Should render languages loader properly", async () => {
        const {container, asFragment} = render(<I18nextProvider i18n={i18n}><LanguagesLoader/></I18nextProvider>);
        
        await waitFor(() => expect(container.querySelector('.btn__row')).not.toBeNull());
        expect(asFragment()).toMatchSnapshot();
    });
});
