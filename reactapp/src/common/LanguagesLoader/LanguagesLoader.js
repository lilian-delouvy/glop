import i18next from "i18next";
import languagesList from '../../assets/languages-list.json';
import './LanguagesLoader.scss';
import 'flag-icons/sass/flag-icons.scss';
import {IconButton} from "@mui/material";

const LanguagesLoader = () => {

    const setTranslatedLanguagesButtons = () => {
        const resultList = [];
        for(let element of languagesList){
            if(element.change_language_key !== i18next.language){
                resultList.push(<IconButton key={element.name} variant="outlined" onClick={() => i18next.changeLanguage(element.change_language_key)}><span className={`fi fi-${element.iso_country_code}`}></span></IconButton>);
            }
        }
        return resultList;
    }
    
    const buttonsList = setTranslatedLanguagesButtons();

    return(
        <div className="btn__row">
            {buttonsList}
        </div>
    );

};

export default LanguagesLoader;
