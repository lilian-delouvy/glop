var json= {
    back :{
        dev: 'http://localhost:8080',
        prod: 'https://mobisure-spring.herokuapp.com'
    }
}

export function getSpringUrl() { 
    console.log(process.env.NODE_ENV);
    if (process.env.NODE_ENV === 'production') {
        return json.back.prod;
      }
    else {
        return json.back.dev;

    }
};

