import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import translationEN from './assets/translations/en/translation.json';
import translationFR from './assets/translations/fr/translation.json';
import countriesEN from './assets/translations/en/countries.json';
import countriesFR from './assets/translations/fr/countries.json';
import interventionTypesEN from './assets/translations/en/interventionTypes.json';
import interventionTypesFR from './assets/translations/fr/interventionTypes.json';
import invoiceEN from './assets/translations/en/invoice.json';
import invoiceFR from './assets/translations/fr/invoice.json';
import validationRulesEN from './assets/translations/en/validationRules.json';
import validationRulesFR from './assets/translations/fr/validationRules.json';
import interventionStatusEN from './assets/translations/en/interventionStatus.json';
import interventionStatusFR from './assets/translations/fr/interventionStatus.json';
import employeeFiltersEN from './assets/translations/en/employeeFilters.json';
import employeeFiltersFR from './assets/translations/fr/employeeFilters.json';
import contractRequestTypesEN from './assets/translations/en/contractRequestTypes.json';
import contractRequestTypesFR from './assets/translations/fr/contractRequestTypes.json';
import insuranceTypesEN from './assets/translations/en/insuranceTypes.json';
import insuranceTypesFR from './assets/translations/fr/insuranceTypes.json';
import contractTypesEN from './assets/translations/en/contractTypes.json';
import contractTypesFR from './assets/translations/fr/contractTypes.json';

const resources = {
    en: {
        translation: translationEN,
        countries: countriesEN,
        interventionTypes: interventionTypesEN,
        invoice: invoiceEN,
        interventionStatus: interventionStatusEN,
        validationRules: validationRulesEN,
        employeeFilters: employeeFiltersEN,
        contractRequestTypes: contractRequestTypesEN,
        insuranceTypes: insuranceTypesEN,
        contractTypes: contractTypesEN
    },
    fr: {
        translation: translationFR,
        countries: countriesFR,
        interventionTypes: interventionTypesFR,
        invoice: invoiceFR,
        interventionStatus: interventionStatusFR,
        validationRules: validationRulesFR,
        employeeFilters: employeeFiltersFR,
        contractRequestTypes: contractRequestTypesFR,
        insuranceTypes: insuranceTypesFR,
        contractTypes: contractTypesFR
    }
};

const fallbackLng = ['en'];
const availableLanguages = ['en', 'fr'];

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources,
        fallbackLng,

        detection: {
            checkWhitelist: true,
        },

        debug: false,

        whitelist: availableLanguages,

        interpolation: {
            escapeValue: false,
        },
        react: { 
            useSuspense: false
          }
    });

export default i18n;