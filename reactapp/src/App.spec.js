import {render, waitFor} from "@testing-library/react";
import App from "./App";
import i18n from "./i18nextConf";
import {I18nextProvider} from "react-i18next";

describe("Should check App component status", () => {
    test("Should display App component properly", async () => {
        const {container, asFragment} = render(<I18nextProvider i18n={i18n}><App/></I18nextProvider>);
        
        await waitFor(() => expect(container.querySelector('.btn__row')).not.toBeNull());
        expect(asFragment()).toMatchSnapshot();
    });
});
