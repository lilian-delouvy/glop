import {useState} from "react";
import Button from "@mui/material/Button";
import {useTranslation} from "react-i18next";
import axios from "axios";
import {Redirect} from "react-router";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import Autocomplete from '@mui/material/Autocomplete';
import './AssistanceForm.scss';
import urls from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../../common/toast";
import InputValidation from "../../common/Validation/InputValidation";
import {REQUIRED} from "../../common/Validation/rules";

const getKeysArray = array => {
    return array.map(element => element.key);
};

const checkAssistanceRequestInputs = assistanceRequest => {
    console.log(assistanceRequest);
    return !(!assistanceRequest.city ||
        !assistanceRequest.country ||
        !assistanceRequest.description ||
        assistanceRequest.types.length === 0);
};

const AssistanceForm = ({contractId}) => {
    
    const [state, setState] = useState({
        city: "",
        country: {key: "", value: ""},
        interventionTypes: [],
        description: "",
        isRequestFinished: false,
        errorMessage: ""
    });
    
    const {t} = useTranslation(["translation", "countries", "interventionTypes"]);
    
    const onSubmit = () => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        const assistanceRequest = {
            contractId,
            date: new Date(),
            city: state.city,
            country: state.country.key,
            types: getKeysArray(state.interventionTypes),
            description: state.description
        };
        if(checkAssistanceRequestInputs(assistanceRequest)) {
            axios({
                method: 'post',
                url: urls.getSpringUrl() + "/customer/contract/intervention/new",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${tokenValue}`
                },
                data: assistanceRequest
            }).then(response => {
                if(response.status === 200){
                    setState({...state, isRequestFinished: true});
                    displaySuccessMessage(t('assistance_form_success'));
                }
                else{
                    displayErrorMessage(t('assistance_form_error'));
                }
            });
        }
        else{
            setState({...state, errorMessage: t('assistance_form_inputs_error')})
        }
    };
    
    const availableCountries = [
        {key: "FRANCE", value: t('countries:country_fr')},
        {key: "SWITZERLAND", value: t('countries:country_be')},
        {key: "BELGIUM", value: t('countries:country_ch')}
    ];

    const interventionTypes = [
        {key: 'INJURY', value: t('interventionTypes:intervention_type_injured')},
        {key: 'MATERIAL', value: t('interventionTypes:intervention_type_material')},
        {key: 'UNDEFINED', value: t('interventionTypes:intervention_type_undefined')}
    ];
    
    const getSelectElementByValue = (selectArray, value) => {
        for(const element of selectArray){
            if(element.value === value)
                return element;
        }
        return null;
    };
    
    return(
        <>
            {state.isRequestFinished &&
                <Redirect to="/desktop"/>
            }
            <div className="assistance-form__container">
                <div className="assistance-form__item">
                    {t('assistance_form_info')}
                </div>
                <div className="assistance-form__item">
                    <InputValidation value={state.city} rules={[REQUIRED]}>
                        <TextField
                            fullWidth
                            id="city-id"
                            label={t('assistance_form_city')}
                            variant="outlined"
                            value={state.city}
                            onChange={e => setState({...state, city: e.target.value})}
                        />
                    </InputValidation>
                </div>
                <div className="assistance-form__item">
                    <FormControl fullWidth>
                        <InputLabel id="countries-select-label">{t('assistance_form_country')}</InputLabel>
                        <InputValidation value={state.country.value} rules={[REQUIRED]}>
                            <Select
                                labelId="countries-select-label"
                                id="countries-select"
                                value={state.country.value}
                                label={t('assistance_form_country')}
                                onChange={(e) => {
                                    const element = getSelectElementByValue(availableCountries, e.target.value);
                                    if(element !== null){
                                        setState({...state, country: element});
                                    }
                                }}
                            >
                                {availableCountries.map(country => (
                                    <MenuItem key={country.key} value={country.value}>{country.value}</MenuItem>
                                ))}
                            </Select>
                        </InputValidation>
                    </FormControl>
                </div>
                <div className="assistance-form__item">
                    <Autocomplete
                        multiple
                        id="multiselect-intervention-types"
                        options={interventionTypes}
                        getOptionLabel={(option) => option.value}
                        onChange={(event, value) => setState({...state, interventionTypes: value})}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                variant="standard"
                                label={t('assistance_form_intervention_type')}
                            />
                        )}
                    />
                </div>
                <div className="assistance-form__item">
                    <InputValidation value={state.description} rules={[REQUIRED]}>
                        <TextField
                            fullWidth
                            id="description-id"
                            label={t('assistance_form_description')}
                            variant="outlined"
                            value={state.description}
                            onChange={e => setState({...state, description: e.target.value})}
                        />
                    </InputValidation>
                </div>
                {state.errorMessage &&
                    <div className="assistance-form__error">{state.errorMessage}</div>
                }
                <div className="assistance-form__item assistance-form__item--button">
                    <Button onClick={onSubmit} variant="contained">{t('assistance_form_submit')}</Button>
                </div>
            </div>
        </>
    );
};

export default AssistanceForm;