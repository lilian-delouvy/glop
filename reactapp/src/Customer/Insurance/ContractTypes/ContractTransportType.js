import React, {useEffect, useState} from "react";
import axios from "axios";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import {useTranslation} from "react-i18next";
import {DatePicker, LocalizationProvider} from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import Checkbox from '@mui/material/Checkbox';
import '../InsuranceSubscription.scss';
import InputValidation from "../../../common/Validation/InputValidation";
import {NUMBER, REQUIRED} from "../../../common/Validation/rules";
import urls from "../../../urls";

const VehicleType = ({parentState, setParentState}) => {

    const [state, setState] = useState({
        vehicleModelList: [],
        vehicleModel: null
    });

    const {t} = useTranslation();

    useEffect(() => {
        axios.get(urls.getSpringUrl() + "/common/vehicle_model/all")
            .then(response => setState({...parentState, vehicleModelList: response.data}));
    }, []);

    return(
        <>
            <div className="insurance-sub__item insurance-sub__item--select">
                <FormControl fullWidth>
                    <InputLabel id="transport-brand-select">{t('insurance_subscription_contract_transport_brand')}</InputLabel>
                    <InputValidation value={parentState.transportBrand} rules={[REQUIRED]}>
                        <Select
                            id="transport-brand-select"
                            labelId="transport-brand-select"
                            value={parentState.transportBrand}
                            label={t('insurance_subscription_contract_transport_brand')}
                            onChange={e => setParentState({...parentState, transportBrand: e.target.value})}
                        >
                            {[...new Set(state.vehicleModelList.map(vehicleModel => vehicleModel.brand))].map(brand => (
                                    <MenuItem value={brand}>{brand}</MenuItem>
                                ))}
                        </Select>
                    </InputValidation>
                </FormControl>
            </div>
            <div className="insurance-sub__item">
                <FormControl fullWidth>
                    <InputLabel id="transport-model-select">{t('insurance_subscription_contract_transport_model')}</InputLabel>
                    <InputValidation value={parentState.transportModel} rules={[REQUIRED]}>
                        <Select
                            id="transport-model-select"
                            labelId="transport-model-select"
                            value={parentState.transportModel}
                            label={t('insurance_subscription_contract_transport_model')}
                            onChange={e =>setParentState({...parentState, transportModel: e.target.value})}
                        >
                            {[...new Set(parentState.transportBrand === null ?  null : state.vehicleModelList
                                .filter(vehicleModel => vehicleModel.brand === parentState.transportBrand)
                                .map(vehicleModel => vehicleModel.model))]
                                .map(model => (
                                    <MenuItem value={model}>{model}</MenuItem>
                                ))}
                        </Select>
                    </InputValidation>
                </FormControl>
            </div>
            <div className="insurance-sub__item insurance-sub__item--select">
                <FormControl fullWidth>
                    <InputLabel id="vehicle-energy-select">{t('insurance_subscription_contract_transport_vehicle_energy')}</InputLabel>
                    <InputValidation value={parentState.transportVehicleEnergy} rules={[REQUIRED]}>
                        <Select
                            labelId="vehicle-energy-select"
                            id="vehicle-energy-select"
                            value={parentState.transportVehicleEnergy}
                            label={t('insurance_subscription_contract_transport_vehicle_energy')}
                            onChange={(e) => setParentState({...parentState, transportVehicleEnergy: e.target.value})}
                        >
                            {[...new Set(parentState.transportBrand === null && parentState.transportModel === null ?  null : state.vehicleModelList
                                .filter(vehicleModel => vehicleModel.brand === parentState.transportBrand)
                                .map(vehicleModel => vehicleModel.vehicle_energy))]
                                .map((type) => (
                                    <MenuItem key={type} value={type}>{type}</MenuItem>
                            ))}
                        </Select>
                    </InputValidation>
                </FormControl>
            </div>
            <div className="insurance-sub__item">
                <FormControl fullWidth>
                    <InputLabel id="transport-vehicle-model-select">{t('insurance_subscription_contract_transport_vehicle_model')}</InputLabel>
                    <InputValidation value={parentState.transportVehicleModel} rules={[REQUIRED]}>
                        <Select
                            id="transport-vehicle-model-select"
                            labelId="transport-vehicle-model-select"
                            value={parentState.transportVehicleModel}
                            label={t('insurance_subscription_contract_transport_model')}
                            onChange={e =>setParentState({...parentState, transportVehicleModelId: e.target.value})}
                        >
                            {[...new Set(parentState.transportBrand === null && parentState.transportModel === null && parentState.vehicle_energy=== null ?  null : state.vehicleModelList
                                .filter(vehicleModel =>
                                    vehicleModel.brand === parentState.transportBrand &&
                                    vehicleModel.model === parentState.transportModel &&
                                    vehicleModel.vehicle_energy === parentState.transportVehicleEnergy
                                )
                                .map(vehicleModel => vehicleModel))]
                                .map((vehicleModel) => (
                                    <MenuItem key={vehicleModel.id} value={vehicleModel.id}>{vehicleModel.powerDIN+"cv ["+vehicleModel.start_year+"-"+vehicleModel.end_year+"]"}</MenuItem>
                                ))}
                        </Select>
                    </InputValidation>
                </FormControl>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={parentState.transportYear} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_contract_transport_year')}
                            views={['year']}
                            value={parentState.transportYear}
                            onChange={(e) => setParentState({...parentState, transportYear: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={parentState.transportRegistrationDate} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_contract_transport_registration_date')}
                            value={parentState.transportRegistrationDate}
                            onChange={(e) => setParentState({...parentState, transportRegistrationDate: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
            </div>
            <div className="insurance-sub__item">
                <InputValidation value={parentState.transportRegistrationNumber} rules={[REQUIRED]}>
                    <TextField
                        id="registration-number"
                        label={t('insurance_subscription_contract_transport_registration_number')}
                        variant="outlined"
                        value={parentState.transportRegistrationNumber}
                        onChange={e => setParentState({...parentState, transportRegistrationNumber: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={parentState.transportDrivingLicenceDate} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_contract_transport_driving_licence_date')}
                            value={parentState.transportDrivingLicenceDate}
                            onChange={(e) => setParentState({...parentState, transportDrivingLicenceDate: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
            </div>
            <div className="insurance-sub__item">{t('insurance_subscription_contract_transport_nb_accidents')}</div>
            <div className="insurance-sub__item">
                <div>{t('insurance_subscription_contract_transport_nb_responsible_sinister')}</div>
                <InputValidation value={parentState.transportNbResponsibleSinister} rules={[REQUIRED,NUMBER]}>
                    <TextField
                        fullWidth
                        type="number"
                        id="nb_responsible_sinister"
                        variant="outlined"
                        value={parentState.transportNbResponsibleSinister}
                        onChange={e => setParentState({...parentState, transportNbResponsibleSinister: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <div>{t('insurance_subscription_contract_transport_nb_non_responsible_sinister')}</div>
                <InputValidation value={parentState.transportNbNonResponsibleSinister} rules={[REQUIRED,NUMBER]}>
                    <TextField
                        fullWidth
                        type="number"
                        id="nb_non_responsible_sinister"
                        variant="outlined"
                        value={parentState.transportNbNonResponsibleSinister}
                        onChange={e => setParentState({...parentState, transportNbNonResponsibleSinister: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <div>{t('insurance_subscription_contract_transport_nb_insured_month')}</div>
                <InputValidation value={parentState.transportNbInsuredMonths} rules={[REQUIRED,NUMBER]}>
                    <TextField
                        fullWidth
                        type="number"
                        id="nb_insured_month"
                        variant="outlined"
                        value={parentState.transportNbInsuredMonths}
                        onChange={e => setParentState({...parentState, transportNbInsuredMonths: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <InputValidation value={parentState.transportBonusMalus} rules={[REQUIRED]}>
                    <TextField
                        fullWidth
                        type="number"
                        id="bonus_malus"
                        InputLabelProps={{ shrink: true }}
                        label={t('insurance_subscription_contract_transport_bonus_malus')}
                        variant="outlined"
                        value={parentState.transportBonusMalus}
                        onChange={e => setParentState({...parentState, transportBonusMalus: e.target.value})}
                    />
                </InputValidation>
            </div>
        </>
    );
};

const NewMobilityType = ({state, setState}) => {

    const {t} = useTranslation();

    const selectNewMobilityType = [{value: "SCOOTER", label: "Scooter"}, {value: "BIKE", label: "Bike"}];

    return(
        <>
            <div className="insurance-sub__item">
                <InputValidation value={state.transportBrand} rules={[REQUIRED]}>
                    <TextField
                        id="transport-brand"
                        label={t('insurance_subscription_contract_transport_brand')}
                        variant="outlined"
                        value={state.transportBrand}
                        onChange={e => setState({...state, transportBrand: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <InputValidation value={state.transportModel} rules={[REQUIRED]}>
                    <TextField
                        id="transport-model"
                        label={t('insurance_subscription_contract_transport_model')}
                        variant="outlined"
                        value={state.transportModel}
                        onChange={e => setState({...state, transportModel: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={state.transportYear} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_contract_transport_year')}
                            views={['year']}
                            value={state.transportYear}
                            onChange={(e) => setState({...state, transportYear: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
            </div>
            <div className="insurance-sub__item">
                <InputValidation value={state.transportValue} rules={[REQUIRED,NUMBER]}>
                    <TextField
                        id="transport-value"
                        label={t('insurance_subscription_contract_transport_value')}
                        variant="outlined"
                        value={state.transportValue}
                        inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                        helperText={t('insurance_subscription_contract_transport_value_helper')}
                        onChange={e => setState({...state, transportValue: e.target.value})}
                    />
                </InputValidation>
            </div>
            <div className="insurance-sub__item insurance-sub__item--select">
                <FormControl fullWidth>
                    <InputLabel id="new-mobility-type">{t('insurance_subscription_contract_transport_new_mobility_type')}</InputLabel>
                    <InputValidation value={state.transportNewMobilityType} rules={[REQUIRED]}>
                        <Select
                            labelId="new-mobility-type"
                            id="new-mobility-type"
                            value={state.transportNewMobilityType}
                            label={t('insurance_subscription_contract_transport_new_mobility_type')}
                            onChange={(e) => setState({...state, transportNewMobilityType: e.target.value})}
                        >
                            {selectNewMobilityType.map((type) => (
                                <MenuItem key={type.value} value={type.value}>{type.label}</MenuItem>
                            ))}
                        </Select>
                    </InputValidation>
                </FormControl>
            </div>
            <div className="insurance-sub__item">
                <p>{t('insurance_subscription_contract_transport_is_electric')}</p>
                <Checkbox onChange={(e) => setState({...state, transportIsElectric: e.target.checked})}/>
            </div>
        </>
    );
};

const ContractTransportType = ({state, setState}) => {

    return(
        <>
            {state.transportType === "VEHICLE" ? (
                <VehicleType parentState={state} setParentState={setState}/>
            ) : (
                <NewMobilityType state={state} setState={setState}/>
            )}
        </>

    );

};

export default ContractTransportType;