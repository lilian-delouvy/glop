import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useTranslation} from "react-i18next";
import '../InsuranceSubscription.scss';
import InputValidation from "../../../common/Validation/InputValidation";
import {REQUIRED} from "../../../common/Validation/rules";

const selectCountries = ["FRANCE", "BELGIUM"];

const ContractTravelType = ({state, setState}) => {

    const {t} = useTranslation();

    return(
        <div className="insurance-sub__item insurance-sub__item--select">
            <FormControl fullWidth>
                <InputLabel id="countries-select-label">{t('insurance_subscription_country')}</InputLabel>
                <InputValidation value={state.country} rules={[REQUIRED]}>
                    <Select
                        labelId="countries-select-label"
                        id="countries-select"
                        value={state.country}
                        label={t('insurance_subscription_country')}
                        onChange={(e) => setState({...state, country: e.target.value})}
                    >
                        {selectCountries.map(country => (
                            <MenuItem key={country} value={country}>{country}</MenuItem>
                        ))}
                    </Select>
                </InputValidation>
            </FormControl>
        </div>
    );

};

export default ContractTravelType;