import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import {
    Checkbox,
    Chip,
    FormControl,
    InputLabel,
    ListItemText,
    MenuItem,
    Select,
    TextField
} from "@mui/material";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { DatePicker, LocalizationProvider } from '@mui/lab';
import Button from "@mui/material/Button";
import {Redirect} from "react-router";
import {useTranslation} from "react-i18next";
import './InsuranceSubscription.scss';
import ContractTravelType from "./ContractTypes/ContractTravelType";
import ContractTransportType from "./ContractTypes/ContractTransportType";
import urls from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../../common/toast";
import InputValidation from "../../common/Validation/InputValidation";
import {REQUIRED} from "../../common/Validation/rules";

const capitalizeFirstLetter = ([firstLetter,...other], locale = navigator.language) => {
    return firstLetter.toLocaleUpperCase(locale) + other.join('').toLocaleLowerCase(locale);
};

const adaptContractType = (insuranceType) => {
    let returnedValue;
    const tempType = insuranceType.toUpperCase();
    switch (tempType){
        case "TRAVEL":
            returnedValue = capitalizeFirstLetter(insuranceType);
            break;
        case "VEHICLE":
            returnedValue = capitalizeFirstLetter(insuranceType);
            break;
        case "NEW_MOBILITY":
            returnedValue = capitalizeFirstLetter(insuranceType.substring(0, 3)) + capitalizeFirstLetter(insuranceType.substring(4));
            break;
        default:
            break;
    }
    return returnedValue;
}

const adaptDate = (date) => {
    const offset = date.getTimezoneOffset();
    date = new Date(date.getTime() - (offset*60*1000));
    return date.toISOString().split('T')[0];
}

export const areDatesValid = (start, end, insuranceData) => {
    // if it's not a travel contract
    if(!insuranceData.hasOwnProperty('minDaysLength')){
        return end >= start;
    }
    const tempDate = new Date(start);
    if(insuranceData.minDaysLength > -1){
        tempDate.setDate(tempDate.getDate() + insuranceData.minDaysLength);
    }
    if(tempDate <= end){
        if(insuranceData.maxDaysLength > -1){
            const maxedDate = new Date(start);
            maxedDate.setDate(maxedDate.getDate() + insuranceData.maxDaysLength);
            return maxedDate >= end;
        }
        else{
            return true;
        }
    }
    return false;
}

const InsuranceSubscription = () => {

    const {insuranceId} = useParams();

    const {t} = useTranslation();

    const [state, setState] = useState({
        insuranceData: {},
        options: [],
        rightHoldersEmails: [],
        subscriptionDate: new Date(),
        endOfSubscriptionDate: new Date(),
        country: "",
        isLoading: true,
        errorMessage: "",
        tempRightHolder: "",
        isSubscriptionFinished: false,
        transportType: "",
        transportVehicleEnergy: "DIESEL",
        transportRegistrationDate: new Date(),
        transportRegistrationNumber: "",
        transportBrand: "",
        transportValue: 0,
        transportModel: "",
        transportYear: new Date(),
        transport: {},
        transportNewMobilityType: "",
        transportIsElectric: false
    });

    useEffect(() => {
        const userEmail = localStorage.getItem('useremail');
        const tempRightHolders = [userEmail];
        axios.get(urls.getSpringUrl() + "/common/insurances/" + insuranceId)
            .then(response => setState({...state, insuranceData: response.data, isLoading: false, transportType: response.data.type, rightHoldersEmails: tempRightHolders}));
    }, []);
    
    const checkContractInputs = type => {
        if(
            (state.rightHoldersEmails.length === 0) ||
            !state.subscriptionDate ||
            !state.endOfSubscriptionDate
        ){
            return false;
        }
        if(type === 'TRAVEL'){
            if(!state.country){
                return false;
            }
        }
        else{
            if(
                !state.transportYear ||
                !state.transportType ||
                !state.transportBrand ||
                !state.transportModel ||
                !state.transportValue
            ){
                return false;
            }
            else{
                if(state.transportType === 'VEHICLE'){
                    if(
                        !state.transportPowerDin ||
                        !state.transportRegistrationDate ||
                        !state.transportRegistrationNumber ||
                        !state.transportVehicleEnergy
                    ){
                        return false;
                    }
                }else{
                    if(!state.transportNewMobilityType){
                        return false;
                    }
                }
            }
        }
        return true;
    };

    const onSubmit = () => {
        if(!checkContractInputs(state.insuranceData.type)){
            setState({...state, errorMessage: t('insurance_subscription_data_required')});
            return;
        }
        if(!areDatesValid(state.subscriptionDate, state.endOfSubscriptionDate, state.insuranceData)){
            setState({...state, errorMessage: `${t('insurance_subscription_date_error_1')}${state.insuranceData.minDaysLength}${t('insurance_subscription_date_error_2')}`});
            return;
        }
        const coverTypeArray = [state.insuranceData.newMobilityCoverType, state.insuranceData.travelCoverType, state.insuranceData.vehicleCoverType];
        const coverTypeElement = coverTypeArray.find(el => el !== undefined);
        const index = coverTypeArray.indexOf(coverTypeElement);
        let coverType;
        switch(index){
            case 0:
                coverType = {newMobilityCoverType: coverTypeElement};
                break;
            case 1:
                coverType = {travelCoverType: coverTypeElement};
                break;
            case 2:
                coverType = {vehicleCoverType: coverTypeElement};
                break;
            default:
                break;
        }
        const rightHoldersSet = [...new Set(state.rightHoldersEmails)];
        const newContract = {
            ...coverType,
            type: `contract${adaptContractType(state.insuranceData.type)}Request`,
            option: state.options,
            rightHoldersEmails: rightHoldersSet,
            subscriptionDate: adaptDate(state.subscriptionDate),
            endOfSubscriptionDate: adaptDate(state.endOfSubscriptionDate)
        };
        if(state.insuranceData.type === "TRAVEL"){
            newContract.country = state.country;
        }
        else{
            if(state.transportYear.getFullYear() > new Date().getFullYear()){
                return ;
            }
            newContract.transport = {
                type: state.transportType,
                brand: state.transportBrand,
                model: state.transportModel,
                year: state.transportYear.getFullYear(),
                value: state.transportValue
            };
            if(state.transportType === "VEHICLE"){
                newContract.transport.powerDIN = state.transportPowerDin;
                newContract.transport.registrationDate = adaptDate(state.transportRegistrationDate);
                newContract.transport.registrationNumber = state.transportRegistrationNumber;
                newContract.transport.vehicle_energy = state.transportVehicleEnergy;
            }else{
                newContract.transport.newMobilityType = state.transportNewMobilityType;
                newContract.transport.isElectric = state.transportIsElectric;
            }
        }
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        axios({
            method: 'post',
            url: urls.getSpringUrl() + "/customer/contract/new",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${tokenValue}`
            },
            data: newContract
        }).then(response => {
                if(response.status === 200){
                    setState({...state, isSubscriptionFinished: true});
                    displaySuccessMessage(t('insurance_subscription_success'));
                }
                else{
                    displayErrorMessage(t('insurance_subscription_error'));
                }
            }
        );
    };

    const displayWarningDateMessage = () => {
        if(state.insuranceData.hasOwnProperty('minDaysLength')){
            if(state.insuranceData.minDaysLength > -1  || state.insuranceData.maxDaysLength > -1){
                return <div className="insurance-sub__constraints-message">
                    <small>{t('insurance_subscription_warning_date_end')}</small>
                    {state.insuranceData.minDaysLength > -1 &&
                        <>
                            <br/>
                            <small>{t('insurance_subscription_warning_date_end_minimum')}{state.insuranceData.minDaysLength}</small>
                        </>
                    }
                    {state.insuranceData.maxDaysLength > -1 &&
                        <>
                            <br/>
                            <small>{t('insurance_subscription_warning_date_end_maximum')}{state.insuranceData.maxDaysLength}</small>
                        </>
                    }
                </div>;
            }
        }
    }

    const addRightHolder = () => {
        if(!state.tempRightHolder) return;
        const temp = state.tempRightHolder;
        const tempArray = state.rightHoldersEmails;
        tempArray.push(temp);
        setState({...state, rightHoldersEmails: tempArray, tempRightHolder: ""});
    };
    
    const removeRightHolder = name => {
        const index = state.rightHoldersEmails.indexOf(name);
        const tempArray = state.rightHoldersEmails;
        if(index > -1){
            tempArray.splice(index, 1);
        }
        setState({...state, rightHoldersEmails: tempArray});
    }

    if(state.isLoading){
        return <div>{t('insurance_subscription_loading')}</div>;
    }

    return (
        <div className="insurance-sub">
            {state.isSubscriptionFinished &&
                <Redirect to="/desktop"/>
            }
            <h2 className="insurance-sub__title">{t('insurance_subscription_title')}</h2>
            <h4>{t('insurance_subscription_contract_info')}</h4>
            <h4>{state.insuranceData.type} - {state.insuranceData.baseDailyPrice} €</h4>
            {state.insuranceData.type === "TRAVEL" ? (
                <ContractTravelType state={state} setState={setState}/>
            ) : (
                <ContractTransportType state={state} setState={setState}/>
            )}
            <div className="insurance-sub__item insurance-sub__item--select">
                <FormControl fullWidth>
                    <InputLabel id="options-select-label">{t('insurance_subscription_options')}</InputLabel>
                    <Select
                        labelId="options-select-label"
                        id="options-select"
                        value={state.options}
                        label={t('insurance_subscription_options')}
                        onChange={(e) => console.log("WIP")}
                    >
                        {state.insuranceData.availableOptions.map(option => (
                            <MenuItem key={option} value={option}>
                                <Checkbox checked={state.options.indexOf(option) > -1}/>
                                <ListItemText primary={option}/>
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={state.subscriptionDate} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_date_start')}
                            value={state.subscriptionDate}
                            onChange={(e) => setState({...state, subscriptionDate: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
            </div>
            <div className="insurance-sub__item">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <InputValidation value={state.endOfSubscriptionDate} rules={[REQUIRED]}>
                        <DatePicker
                            label={t('insurance_subscription_date_end')}
                            value={state.endOfSubscriptionDate}
                            onChange={(e) => setState({...state, endOfSubscriptionDate: e})}
                            renderInput={(params) => <TextField {...params}/>}
                        />
                    </InputValidation>
                </LocalizationProvider>
                {displayWarningDateMessage()}
            </div>
            <div className="insurance-sub__item">
                <TextField
                    id="add-rightHolder"
                    label={t('insurance_subscription_rightholder')}
                    variant="outlined"
                    value={state.tempRightHolder}
                    onChange={e => setState({...state, tempRightHolder: e.target.value})}
                    InputProps={{endAdornment: <Button onClick={() => addRightHolder()}>{t('insurance_subscription_add')}</Button>}}
                />
                {state.rightHoldersEmails.length !== 0 &&
                    <div className="insurance-sub__rightHolders-title">{t('insurance_subscription_rightholders_list')}</div>
                }
                {state.rightHoldersEmails.map((rightHolder, index) => (
                    <div key={index} className="insurance-sub__rightHolders-item">
                        <Chip label={rightHolder} onDelete={() => removeRightHolder(rightHolder)}/>
                    </div>
                ))}
            </div>
            {state.errorMessage &&
                <div>{state.errorMessage}</div>
            }
            <div className="insurance-sub__item">
                <Button onClick={onSubmit} variant="contained">{t('insurance_subscription_submit')}</Button>
            </div>
        </div>
    );
};

export default InsuranceSubscription;