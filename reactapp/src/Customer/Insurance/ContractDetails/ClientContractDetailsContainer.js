import './ClientContractDetailsContainer.scss';
import {useEffect, useState} from "react";
import axios from "axios";
import {useTranslation} from "react-i18next";
import ContractDetails from "../../../common/ContractDetails/ContractDetails";
import urls from "../../../urls";
import {translateType} from "../../../common/Desktop/translation";

const ClientContractDetailsContainer = ({contractId}) => {
    
    const [state, setState] = useState({
        contract: {},
        isLoading: true
    });
    
    const {t} = useTranslation(["translation","contractStatus"]);

    const contractStatus = [
        {key: 'PENDING', value:t('contractStatus:contract_status_pending')},
        {key: 'VALID', value:t('contractStatus:contract_status_valid')},
        {key: 'ENDED', value:t('contractStatus:contract_status_ended')},
    ];
    
    useEffect(() => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        axios({
            method: 'get',
            url: urls.getSpringUrl() + "/contract-details/" + contractId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${tokenValue}`
            }
        }).then(response => {
            if(response.status === 200){
                setState({...state, contract: response.data, isLoading: false});
            }
        })
    },[]);
    
    if(state.isLoading){
        return <div>Loading...</div>;
    }
    
    return(
        <>
            <div className="client-contract-details">
                <div className="client-contract-details__container">
                    <div className="client-contract-details__header">
                        <b>{t('client_contract_details_container_title')}</b>
                        <div>
                            <p>{t('client_contract_details_container_status')}{translateType(state.contract.status, contractStatus)}</p>
                        </div>
                    </div>
                    <ContractDetails contract={state.contract}/>
                </div>
            </div>
        </>
    )
    
};

export default ClientContractDetailsContainer;