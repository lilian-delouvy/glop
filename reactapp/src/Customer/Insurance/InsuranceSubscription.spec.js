import {areDatesValid} from "./InsuranceSubscription";

describe("Should check InsuranceSubscription component behaviour", () => {
    test("Should check dates correctly for a travel contract and respect minDaysLength constraint", () => {
        const start = new Date();
        let end = new Date(start);
        end.setMonth(end.getMonth() + 2);
        const insuranceData = {
            minDaysLength: 30,
            maxDaysLength: -1
        };
        expect(areDatesValid(start, end, insuranceData)).toBeTruthy();
        end = new Date(start);
        end.setMonth(end.getMonth() - 2);
        expect(areDatesValid(start, end, insuranceData)).not.toBeTruthy();
    });
    
    test("Should check dates correctly for a travel contract and respect maxDaysLength constraint", () => {
        const start = new Date();
        let end = new Date(start);
        end.setMonth(end.getMonth() + 2);
        const insuranceData = {
            maxDaysLength: 30,
            minDaysLength: 1
        };
        expect(areDatesValid(start, end, insuranceData)).not.toBeTruthy();
        end = new Date(start);
        end.setDate(end.getDate() + 15);
        expect(areDatesValid(start, end, insuranceData)).toBeTruthy();
    });
    
    test("Should check dates correctly when it's not a travel contract",() => {
        const start = new Date();
        let end = new Date(start);
        end.setMonth(end.getMonth() + 2);
        const insuranceData = {};
        expect(areDatesValid(start, end, insuranceData)).toBeTruthy();
        end = new Date(start);
        end.setDate(end.getDate() - 1);
        expect(areDatesValid(start, end, insuranceData)).not.toBeTruthy();
    })
})