import React from "react";
import {useTranslation} from "react-i18next";
import './Desktop.scss';

const DocumentsDesktop = () => {

    const {t} = useTranslation();
    
    return(
        <>
            <h3 className="desktop__title">{t('documents_desktop_title')}</h3>
            <div className="desktop__error">{t('documents_desktop_no_documents')}</div>
        </>
    )
    
};

export default DocumentsDesktop;