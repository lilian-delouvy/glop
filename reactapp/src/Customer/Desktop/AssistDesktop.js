import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import axios from "axios";
import urls from "../../urls";
import RowButtonContainer from "../../common/Buttons/RowButtonContainer";
import './Desktop.scss';
import alertIcon from "../../assets/images/InsurancesIcons/alert.svg";
import RowButton from "../../common/Buttons/RowButton";

const getContracts = async (config) => {
    return await axios.get(urls.getSpringUrl() + "/customer/contracts/VALID", config);
};

const getRequests = async (config) => {
    return await axios.get(urls.getSpringUrl() + "/customer/interventions", config);
}

const AssistDesktop = ({state, setState}) => {

    const {t} = useTranslation(["translation","interventionStatus"]);
    
    const [assistState, assistSetState] = useState({
        contracts: [],
        requests: []
    });
    
    const translateStatus = status => {
        let translatedStatus;
        switch (status) {
            case 'PENDING':
                translatedStatus = t('interventionStatus:intervention_status_pending');
                break;
            case 'ACCEPTED':
                translatedStatus = t('interventionStatus:intervention_status_accepted');
                break;
            default:
                break;
        }
        return translatedStatus;
    }
    
    const openAssistanceForm = contract => {
        setState({...state, navbarTitle: 7, contractId: contract.id});
    };
    
    const openRequest = request => {
        setState({...state, navbarTitle: 8, assistanceRequest: request});
    }
    
    useEffect(() => {
        const setContractsAndRequests = async() => {
            const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
            let config = {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + tokenValue
                }
            }
            const responseContracts = await getContracts(config);
            const responseRequests = await getRequests(config);
            console.log(responseContracts);
            assistSetState({...state, contracts: responseContracts.data, requests: responseRequests.data});
        }
        setContractsAndRequests();
    }, []);
    
    const filteredNonClosedRequests = assistState.requests.filter(request => request.status === "ACCEPTED" || request.status === "PENDING");
    
    return(
        <>
            <h3 className="desktop__title">{t('assist_desktop_title')}</h3>
            {assistState.contracts.length === 0 &&
                <div className="desktop__error">{t('assist_desktop_no_valid_contracts')}</div>
            }
            {assistState.contracts.map((contract, index) =>
                <div key={index}>
                    <RowButtonContainer
                        type={contract.type}
                        onClick={() => openAssistanceForm(contract)}
                        contract={contract}
                    />
                </div>
            )}
            <h3 className="desktop__title">{t('assist_desktop_pending_requests')}</h3>
            {filteredNonClosedRequests.length === 0 &&
                <div className="desktop__error">{t('assist_desktop_no_pending_requests')}</div>
            }
            {filteredNonClosedRequests.map((request, index) =>
                <div key={index}>
                    <RowButton
                        onClick={() => openRequest(request)}
                        title={translateStatus(request.status)}
                        icon={alertIcon}
                        info={request.description}
                    />
                </div>
            )}
        </>
    )
    
};

export default AssistDesktop;