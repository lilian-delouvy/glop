import React from "react";
import {useTranslation} from "react-i18next";
import RowButtonContainer from "../../common/Buttons/RowButtonContainer";
import './Desktop.scss';

const HomeDesktop = ({state, setState}) => {

    const {t} = useTranslation();
    
    const threeContracts = state.contracts.slice(0, 3);

    const openInsurance = (id) => {
        setState({...state, contractId: id, navbarTitle: 6});
    }
    
    return(
        <>
            <h3 className="desktop__title">{t('home_desktop_title')}</h3>
            {state.contracts.length === 0 ? (
                <h4 className="desktop__error">{t('home_desktop_no_contracts')}</h4>
            ) : (
                <>
                    {threeContracts.map(function(contract, index){
                        return <div key={`contract${index}`}>
                            <RowButtonContainer
                                onClick={() => openInsurance(contract.id)}
                                type={contract.type}
                                contract={contract}
                            />
                        </div>
                    })}
                </>
            )}
            <h3 className="desktop__title">{t('documents_desktop_title')}</h3>
            {state.documents.length === 0 ? (
                <h4 className="desktop__error">{t('home_desktop_no_documents')}</h4>
            ) : (
                <div className="document">
                    {state.documents.map(function (document, index){
                        return <div key={`document${index}`}>
                            <p className="document__timeline">{document.date}</p>
                            <h4 className="document__title">{document.title}</h4>
                            <p className="document__description">{document.description}</p>
                        </div>
                    })}
                </div>
            )}
            <h3 className="desktop__title">{t('home_desktop_news_title')}</h3>
            {state.news.length === 0 ? (
                <h4 className="desktop__error">{t('home_desktop_no_news')}</h4>
            ) : (
                <div>
                    {state.news.map(function (current, index){
                        return <div key={`news${index}`}>
                            {current.title}
                            {current.description}
                        </div>
                    })}
                </div>
            )}
        </>
    )
    
};

export default HomeDesktop;