import React from "react";
import BookmarkIcon from '@mui/icons-material/Bookmark';
import './MenuItemWithIcon.scss';

const MenuItemWithIcon = ({onClick, text}) => {
    return(
        <div className="menuitem" onClick={onClick}>
            <BookmarkIcon sx={{color: 'lightgrey'}}/>
            <p className="menuitem__title">{text}</p>
        </div>
    );
};

export default MenuItemWithIcon;