import './ContractsDesktop.scss';
import {useTranslation} from "react-i18next";
import {ThemeProvider} from "@mui/material";
import Button from "@mui/material/Button";
import React from "react";
import RowButtonContainer from "../../common/Buttons/RowButtonContainer";
import setTheme from "../../common/ThemeColorProvider";

const ContractsDesktop = ({state, setState, contracts, onClick}) => {

    const {t} = useTranslation();

    const theme = setTheme('#0E387A');
    
    const openInsurance = (id) => {
        setState({...state, contractId: id, navbarTitle: 6});
    }
    
    return(
        <>
            <h1 className="contracts-list__title">{t('contracts_list_title')}</h1>
            <div className="contracts-list">
                {contracts.map((contract, index) =>
                    <div key={index}>
                        <RowButtonContainer
                            type={contract.type}
                            onClick={() => openInsurance(contract.id)}
                            contract={contract}
                        />
                    </div>
                )}
                <div className="contracts-list__image-button">
                    <ThemeProvider theme={theme}>
                        <Button variant="contained" onClick={onClick}><div className="contracts-list__image-button-text">{t('contracts_desktop_add')}</div></Button>
                    </ThemeProvider>
                </div>
            </div>
        </>
    )
};

export default ContractsDesktop;