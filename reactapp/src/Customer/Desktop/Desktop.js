import React, {useEffect, useState} from "react";
import './Desktop.scss';
import {Avatar} from "@mui/material";
import BookmarkIcon from '@mui/icons-material/Bookmark';
import Button from "@mui/material/Button";
import HomeDesktop from "./HomeDesktop";
import AssistDesktop from "./AssistDesktop";
import EstimateDesktop from "./EstimateDesktop";
import DocumentsDesktop from "./DocumentsDesktop";
import {useTranslation} from "react-i18next";
import axios from "axios";
import ContractsDesktop from "./ContractsDesktop";
import urls from "../../urls";
import NavBar from "../../common/NavBar";
import ClientContractDetailsContainer from "../Insurance/ContractDetails/ClientContractDetailsContainer";
import AssistanceForm from "../Assistance/AssistanceForm";
import AssistanceRequestDetails from "../../common/Desktop/Assistance/AssistanceRequestDetails";

const Desktop = () => {
    
    const [state, setState] = useState({
        contracts: [],
        username: "",
        //MOCKED DATA
        documents: [{
            date: "8 weeks ago",
            title: "Appel à l'aide !",
            description: "Description du document"
        }],
        news: [],
        navbarTitle: 1,
        contractId: 0,
        assistanceRequest: {}
    });

    const {t} = useTranslation();

    const getContracts = () => {
        const tokenValue = JSON.parse(localStorage.getItem('jwttoken')).token;
        let config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + tokenValue
            }
        }
        axios.get(urls.getSpringUrl() + "/customer/infos", config)
            .then(response => {
                if(response.data != null){
                    setState({...state, contracts: response.data.contractList, username: response.data.firstname + " " + response.data.lastname});
                }
            });
    }
    
    useEffect(() => {
        getContracts();
    }, []);
    
    const changeTab = (value) => {
        setState({...state, navbarTitle: value});
    }
    
    let navbarTitle;
    switch(state.navbarTitle){
        case 2:
            navbarTitle = t('desktop_assist');
            break;
        case 3:
            navbarTitle = t('desktop_estimation_title');
            break;
        case 4:
            navbarTitle = t('desktop_documents_title');
            break;
        case 5:
            navbarTitle = t('desktop_contracts');
            break;
        case 6:
            navbarTitle = t('desktop_account');
            break;
        default:
            navbarTitle = t('desktop_home');
            break;
    }
    
    return(
        <div className="desktop">
            <div className="desktop__left-container">
                <div className="desktop-left">
                    <div className="desktop-left__avatar">
                        <Avatar alt={state.username} src=""/>
                    </div>
                    <div className="desktop__username">{state.username}</div>
                    <hr/>
                    <div>
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(1)}>{t('desktop_home')}</Button>
                    </div>
                    <div>
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(2)}>{t('desktop_help')}</Button>
                    </div>
                    <div>
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(3)}>{t('desktop_estimation')}</Button>
                    </div>
                    <div>
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(4)}>{t('desktop_documents')}</Button>
                    </div>
                    <div>
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(5)}>{t('desktop_contracts')}</Button>
                    </div>
                    <hr className="desktop__hidden"/>
                    <div className="desktop__hidden">
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(9)}>{t('desktop_account')}</Button>
                    </div>
                    <div className="desktop__hidden">
                        <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}}>{t('desktop_parameters')}</Button>
                    </div>
                </div>
            </div>
            <div className={`desktop__right-container${(state.navbarTitle === 3 || state.navbarTitle === 2) ? " desktop__right-container--scrollable" : ""}`}>
                <NavBar title={navbarTitle}/>
                {state.navbarTitle === 1 &&
                    <HomeDesktop state={state} setState={setState}/>
                }
                {state.navbarTitle === 2 &&
                    <AssistDesktop state={state} setState={setState}/>
                }
                {state.navbarTitle === 3 &&
                    <EstimateDesktop/>
                }
                {state.navbarTitle === 4 &&
                    <DocumentsDesktop/>
                }
                {state.navbarTitle === 5 &&
                    <ContractsDesktop state={state} setState={setState} contracts={state.contracts} onClick={() => setState({...state, navbarTitle: 3})}/>
                }
                {state.navbarTitle === 6 &&
                    <ClientContractDetailsContainer contractId={state.contractId}/>
                }
                {state.navbarTitle === 7 &&
                    <AssistanceForm contractId={state.contractId}/>
                }
                {state.navbarTitle === 8 &&
                    <AssistanceRequestDetails request={state.assistanceRequest}/>
                }
            </div>
        </div>
    )
};

export default Desktop;