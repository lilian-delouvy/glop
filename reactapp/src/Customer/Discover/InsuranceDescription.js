import './InsuranceDescription.scss';
import beachImage from '../../assets/images/plage-fete.jpg';
import vehicleImage from '../../assets/images/ContractImages/vehicle.png';
import travelImage from '../../assets/images/ContractImages/plane.png';
import newMobilityImage from '../../assets/images/ContractImages/new_mobility.png';
import {useTranslation} from "react-i18next";
import {Button} from "@mui/material";
import {checkUserLoginStatus} from "../../common/token";
import {translateType} from "../../common/Desktop/translation";

const InsuranceDescription = ({insurance}) => {

    const {t} = useTranslation(["translation","insuranceTypes","contractTypes"]);
    
    const isUserLogged = checkUserLoginStatus("client");
    
    const insuranceTypes = [
        {key: 'TRAVEL', value: t('insuranceTypes:insurance_type_travel')},
        {key: 'VEHICLE', value: t('insuranceTypes:insurance_type_vehicle')},
        {key: 'NEW_MOBILITY', value: t('insuranceTypes:insurance_type_new_mobility')}
    ];
    
    const contractTypes = [
        {key: 'LONG', value: t('contractTypes:contract_type_long')},
        {key: 'SHORT', value: t('contractTypes:contract_type_short')},
        {key: 'PREMIUM', value: t('contractTypes:contract_type_premium')},
        {key: 'INTEGRAL', value: t('contractTypes:contract_type_integral')},
        {key: 'THIRD_PARTY', value: t('contractTypes:contract_type_third_party')},
        {key: 'THIRD_PARTY_THEFT', value: t('contractTypes:contract_type_third_party_theft')},
        {key: 'THEFT', value: t('contractTypes:contract_type_theft')}
    ];
    
    let ref = isUserLogged ? "#insurance-subscription/" + insurance.id : "#signup/" + insurance.id;

    const coverTypeArray = [insurance.newMobilityCoverType, insurance.travelCoverType, insurance.vehicleCoverType];
    const coverType = coverTypeArray.find(el => el !== undefined);

    let image;
    switch (insurance.type) {
        case 'TRAVEL':
            image = travelImage;
            break;
        case 'VEHICLE':
            image = vehicleImage;
            break;
        case 'NEW_MOBILITY':
            image = newMobilityImage;
            break;
        default:
            image = beachImage;
            break;
    }
    
    return(
        <div key={insurance.id} className="description">
            <div className="description__header">{t('insurance_description_title')} {translateType(insurance.type, insuranceTypes)}</div>
            <div className="description__container">
                <div className="imageDisplay__container">
                    <img src={image} alt="insurance" className="imageDisplay__image"/>
                </div>
                <div className="description__content-container">
                    <div className="description__content">
                        <p>{t('insuranceDescription_contract_type')}{translateType(coverType, contractTypes)}</p>
                        <p>{t('insuranceDescription_daily_price')}{insurance.baseDailyPrice} €</p>
                        <Button href={ref} variant="outlined">{t('insurance_description_submit')}</Button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default InsuranceDescription;