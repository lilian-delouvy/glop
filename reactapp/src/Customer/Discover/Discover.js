import React, {useEffect, useState} from "react";
import axios from "axios";
import './Discover.scss';
import InsuranceDescription from "./InsuranceDescription";
import {useTranslation} from "react-i18next";
import urls from "../../urls";

const Discover = () => {
    
    const [state, setState] = useState({
        contractsList: []
    });

    const {t} = useTranslation();
    
    useEffect(() => {
        axios.get(urls.getSpringUrl() + "/common/insurances/all")
            .then(response => setState({...state, contractsList: response.data}));
    }, []);
    
    return (
        <div>
            <h3 className="discover__title">{t('discover_title')}</h3>
            {state.contractsList.map((contract, index) => (
                <div key={index}>
                    <InsuranceDescription
                        insurance={contract}
                    />
                </div>
            ))}
        </div>
    )
};

export default Discover;