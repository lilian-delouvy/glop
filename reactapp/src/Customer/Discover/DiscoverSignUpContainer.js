import SignUp from "../Authentication/SignUp";
import {Avatar, Chip} from "@mui/material";
import logo from '../../assets/images/plage-fete.jpg';
import React, {useState} from "react";
import {Redirect} from "react-router";
import { useParams } from "react-router-dom";
import './DiscoverSignUp.scss';
import {useTranslation} from "react-i18next";

const DiscoverSignUpContainer = () => {
    
    const {insuranceId} = useParams();
    
    const {t} = useTranslation();
    
    const [state, setState] = useState({
        isLoggedIn: false
    })
    
    const onParentSubmit = () => {
        setState({...state, isLoggedIn: true});
    };
    
    const insuranceURL = "/insurance-subscription/" + insuranceId;
    
    return(
        <>
            {state.isLoggedIn ? (
                <Redirect to={insuranceURL}/>
            ) : (
                <>
                    <Avatar alt="MobiSure Logo" src={logo}/>
                    <div className="discover-signup">
                        <p>{t('discover_signup_title')}</p>
                        <Chip label={t('discover_signup_label')} />
                        <SignUp onParentSubmit={onParentSubmit}/>
                    </div>
                </>
                
            )}
        </>
    );
    
};

export default DiscoverSignUpContainer;