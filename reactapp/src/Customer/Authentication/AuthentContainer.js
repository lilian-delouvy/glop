import React from "react";
import './AuthentContainer.scss';
import {Link} from "@mui/material";
import {useTranslation} from "react-i18next";
import urls from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../../common/toast";
import Login from "../../common/Authentication/Login/Login";

const AuthentContainer = ({axios, parentState, setParentState}) => {

    const {t} = useTranslation();

    const confirmLogin = (response) => {
        window.localStorage.setItem('jwttoken', JSON.stringify(response.data));
        window.localStorage.setItem('usertype', 'client');
        displaySuccessMessage(t('authentication_signin_success'));
        setParentState({...parentState, isLoggedIn: true});
    }

    const onLoginSubmit = (e) => {
        axios.post(urls.getSpringUrl() + '/auth/customer/signin',e)
            .then(response => confirmLogin(response))
            .catch(() => displayErrorMessage(t('authentication_signin_error')));
    }

    return <div>
        <Login onSubmit={onLoginSubmit}/>
        <div className="authent-container__link">
            <Link href="#signup">{t('authentication_signup')}</Link>
        </div>
    </div>
};

export default AuthentContainer;