import AuthentContainer from "./AuthentContainer";
import i18n from '../../i18nextConf';
import {I18nextProvider} from 'react-i18next';
import {fireEvent, render, waitFor} from "@testing-library/react";

const mockedAxios = {
    post: function(){}
};

describe("Check authentification behaviour", () => {
    test("Should render component properly and simulate user click on subscription", async () => {
        
        const parentState = {};
        const setParentState = () => {};
        
        const {container, asFragment, getByText} = render(<I18nextProvider i18n={i18n}><AuthentContainer parentState={parentState} setParentState={setParentState} axios={mockedAxios}/></I18nextProvider>);
        
        await waitFor(() => expect(container.querySelector('.authent-container__link')).not.toBeNull());
        expect(asFragment()).toMatchSnapshot();
        
        const subscriptionButton = getByText("Sign up");
        fireEvent.click(subscriptionButton);
        expect(asFragment()).toMatchSnapshot();
    })
})