import {Suspense} from "react";
import AuthentContainer from "./AuthentContainer";


const mockedAxios = {
    post: function(){}
}

export default {
    title: 'Project/Authentication',
    component: AuthentContainer
};

const Template = () => <Suspense fallback={<div>Loading...</div>}><AuthentContainer axios={mockedAxios}/></Suspense>;

export const Default = Template.bind({});
Default.args = {};