import {ThemeProvider, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import React, {useState} from "react";
import './SignUp.scss';
import {useTranslation} from "react-i18next";
import axios from "axios";
import urls from "../../urls";
import {displayErrorMessage, displaySuccessMessage} from "../../common/toast";
import setTheme from "../../common/ThemeColorProvider";
import {REGEX, REQUIRED} from "../../common/Validation/rules";
import InputValidation from "../../common/Validation/InputValidation";

const SignUpFirstPage = ({state, setState}) => {

    const {t} = useTranslation();
    
    const checkPageContent = () => {
        if(state.email && state.password){
            setState({...state, page: 1, unfilledPageMessage: ""});
        }
        else{
            setState({...state, unfilledPageMessage: t('signup_error_informations')});
        }
    }
    
    return(
        <div className="signup__container">
            <h3>{t('signup_account_creation')}</h3>
            <h4>{t('signup_ask_info')}</h4>
            <div>
                <InputValidation value={state.email} rules={[REQUIRED]}>
                    <TextField fullWidth label={t('authentication_email')} onChange={(e) => setState({...state, email: e.currentTarget.value})} variant="outlined"/>
                </InputValidation>
            </div>
            <div className="signup__item">
                <InputValidation value={state.password} rules={[REQUIRED]}>
                    <TextField fullWidth type="password" label={t('authentication_password')} onChange={(e) => setState({...state, password: e.currentTarget.value})} variant="outlined"/>
                </InputValidation>
            </div>
            <div className="signup__item">
                <TextField fullWidth type="password" label={t('signup_password_validation')} onChange={(e) => setState({...state, verifPassword: e.currentTarget.value})} variant="outlined"/>
            </div>
            {(state.password !== state.verifPassword && state.password !== "" && state.verifPassword !== "") &&
                <p className="signup__error-message">{t('signup_password_error')}</p>
            }
            <div className="signup__centered-btn">
                <Button fullWidth variant="contained" onClick={() => checkPageContent()}><div className="signup__text-container">{t('signup_next')}</div></Button>
            </div>
        </div>
    );
    
};

const SignUpSecondPage = ({state, setState, onParentSubmit}) => {

    const {t} = useTranslation();
    
    const onSubmit = (e) => {
        axios.post(urls.getSpringUrl() + '/auth/customer/signup',e)
            .then(response => onSignIn(e))
            .catch(() => displayErrorMessage(t('authentication_signup_error')));
    };
    
    const onSignIn = (e) => {
        axios.post(urls.getSpringUrl() + "/auth/customer/signin", e)
            .then(response => confirmRegistration(response))
            .catch(() => displayErrorMessage(t('authentication_signup_error')));
    }

    const confirmRegistration = (response) => {
        window.localStorage.setItem('jwttoken', JSON.stringify(response.data));
        window.localStorage.setItem('usertype', 'client');
        window.localStorage.setItem('useremail', state.email);
        displaySuccessMessage(t('authentication_signup_success'));
        onParentSubmit();
    };

    const checkPageContent = () => {
        if(state.lastname && state.firstname && state.birthdate && state.address && state.phone){
            onSubmit({email: state.email, password: state.password, firstname: state.firstname, lastname: state.lastname, birthdate: state.birthdate, address: state.address, phoneNumber: state.phone});
        }
        else{
            setState({...state, unfilledPageMessage: t('signup_error_informations')});
        }
    }

    return(
        <div className="signup__container signup__container--second-page">
            <h3>{t('signup_next_message')}</h3>
            <InputValidation value={state.lastname} rules={[REQUIRED,REGEX({message:t('signup_error_only_letters'), pattern:/^[a-z '-]+$/i})]}>
                <TextField fullWidth label={t('signup_second_name')} onChange={(e) => setState({...state, lastname: e.currentTarget.value})} variant="outlined"/>
            </InputValidation>
            <InputValidation value={state.firstname} rules={[REQUIRED,REGEX({message:t('signup_error_only_letters'), pattern:/^[a-z '-]+$/i})]}>
                <TextField fullWidth label={t('signup_second_firstname')} onChange={(e) => setState({...state, firstname: e.currentTarget.value})} variant="outlined"/>
            </InputValidation>
            <p>{t('signup_second_birthdate')}</p>
            <InputValidation value={state.birthdate} rules={[REQUIRED]}>
                <TextField fullWidth type="date" onChange={(e) => setState({...state, birthdate: e.currentTarget.value})} variant="outlined"/>
            </InputValidation>
            <InputValidation value={state.address} rules={[REQUIRED]}>
                <TextField fullWidth label={t('signup_second_address')} onChange={(e) => setState({...state, address: e.currentTarget.value})} variant="outlined"/>
            </InputValidation>
            <InputValidation value={state.phone} rules={[REQUIRED,REGEX({message:t('signup_error_only_numbers'), pattern:/^\d+$/})]}>
                <TextField fullWidth type="tel" label={t('signup_second_phone')} onChange={(e) => setState({...state, phone: e.currentTarget.value})} variant="outlined"/>
            </InputValidation>
            <div className="signup__centered-btn">
                <Button fullWidth variant="contained" onClick={() => checkPageContent()}><div className="signup__text-container">{t('signup_validation')}</div></Button>
            </div>
        </div>
    )
    
}

const SignUp = ({onParentSubmit}) => {

    const [state, setState] = useState({
        email: "",
        password: "",
        verifPassword: "",
        firstname: "",
        lastname: "",
        birthdate: "",
        address: "",
        phone: "",
        page: 0,
        unfilledPageMessage: ""
    });
    
    const theme = setTheme('#0E387A');
    
    return(
        <>
            <ThemeProvider theme={theme}>
                {state.page === 0 ?
                    <SignUpFirstPage state={state} setState={setState}/>
                    :
                    <SignUpSecondPage state={state} setState={setState} onParentSubmit={onParentSubmit}/>
                }
                {state.unfilledPageMessage &&
                    <div className="signup__error-message">{state.unfilledPageMessage}</div>
                }
            </ThemeProvider>
        </>
    )
}

export default SignUp;