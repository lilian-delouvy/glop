import SignUp from "./SignUp";
import './SignUp.scss';
import React, {useState} from "react";
import {Redirect} from "react-router";

const SignUpContainer = () => {
    
    const [state, setState] = useState({
        isLoggedIn: false
    })
    
    const onParentSubmit = () => {
        setState({isLoggedIn: true});
    };
    
    return (<div className="signup-container">
        {state.isLoggedIn ? (
            <Redirect to="/desktop"/>
        ) : (
            <SignUp onParentSubmit={onParentSubmit}/>
        )}
    </div>);
}

export default SignUpContainer;