import './IEDesktop.scss';
import {Avatar} from "@mui/material";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import Button from "@mui/material/Button";
import BookmarkIcon from '@mui/icons-material/Bookmark';
import IEHomeDesktop from "./IEHomeDesktop";
import NavBar from "../../common/NavBar";
import EmployeeContractsDesktop
    from "../../common/Desktop/EmployeeDesktop/EmployeeContractsDesktop/EmployeeContractsDesktop";

const IEDesktop = () => {
    
    const {t} = useTranslation();

    const [state, setState] = useState({
        navbarTitle: 1
    });
    
    const changeTab = (value) => {
        setState({...state, navbarTitle: value});
    }
    
    let navbarTitle;
    switch(state.navbarTitle){
        case 2:
            navbarTitle = t('employee_desktop_contracts');
            break;
        default:
            navbarTitle = t('desktop_home');
            break;
    }
    
    return(
        <div className="desktop">
            <div className="desktop__left-container">
                <div className="desktop-left">
                    <div className="desktop-left__avatar">
                        <Avatar alt={t('desktop_username')} src=""/>
                        <div className="desktop__username">{window.localStorage.getItem('useremail')}</div>
                        <hr/>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(1)}>{t('desktop_home')}</Button>
                        </div>
                        <div>
                            <Button hover className="Button" variant="text" startIcon={<BookmarkIcon/>} sx={{color: "white"}} onClick={() => changeTab(2)}>{t('employee_desktop_contracts')}</Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="desktop__right-container">
                <NavBar title={navbarTitle}/>
                {state.navbarTitle === 1 &&
                    <IEHomeDesktop state={state}/>
                }
                {state.navbarTitle === 2 &&
                    <EmployeeContractsDesktop/>
                }
            </div>
        </div>
    );
    
};

export default IEDesktop;