import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import './ContractsList.scss';
import {useTranslation} from "react-i18next";
import {translateType} from "../../common/Desktop/translation";

const StyleHeaderTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#D3D3D3'
    }
}));

const ContractsList = ({contracts, onClick}) => {
    
    const {t} = useTranslation(["translation","contractRequestTypes"]);

    const requestTypes = [
        {key: 'TRAVEL', value: t('contractRequestTypes:contract_request_type_travel')},
        {key: 'TRANSPORT', value: t('contractRequestTypes:contract_request_type_transport')}
    ];
    
    return(
        <>
            {contracts.length === 0 ? (
                <p className="pending-list__error">{t('pending_list_no_contracts')}</p>
            ) : (
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="pending contracts table">
                            <TableHead>
                                <TableRow>
                                    <StyleHeaderTableCell>{t('pending_list_firstname')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('pending_list_lastname')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('pending_list_start_date')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('pending_list_end_date')}</StyleHeaderTableCell>
                                    <StyleHeaderTableCell align="right">{t('pending_list_type')}</StyleHeaderTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {contracts.map(contract => (
                                    <TableRow
                                        key={contract.id}
                                        hover
                                        sx={{ '&:last-child td, &:last-child th': { border: 0}, 'cursor': 'pointer'}}
                                        onClick={() => onClick(contract.id)}
                                    >
                                        <TableCell component="th" scope="row">{contract.subscriber.firstname}</TableCell>
                                        <TableCell align="right">{contract.subscriber.lastname}</TableCell>
                                        <TableCell align="right">{contract.subscriptionDate.split('T')[0]}</TableCell>
                                        <TableCell align="right">{contract.endOfSubscriptionDate.split('T')[0]}</TableCell>
                                        <TableCell align="right">{translateType(contract.type, requestTypes)}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                )
            }
        </>
    );
};

export default ContractsList;