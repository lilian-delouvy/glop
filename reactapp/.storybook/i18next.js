﻿import {initReactI18next} from 'react-i18next';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

const supportedLngs = ['en', 'fr'];
const ns = ['common'];

i18n.use(LanguageDetector)
    .use(initReactI18next)
    .init({
        //debug: true,
        lng: 'en',
        fallbackLng: 'en',
        interpolation: {
            escapeValue: false,
        },
        defaultNS: 'common',
        ns,
        supportedLngs,
    });

supportedLngs.forEach((lang) => {
    i18n.addResources(
        lang,
        "common",
        require(`../src/assets/translations/${lang}/translation.json`),
    );
});

export {i18n};